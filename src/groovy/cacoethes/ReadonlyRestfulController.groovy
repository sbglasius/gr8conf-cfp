package cacoethes
import grails.rest.RestfulController

import static javax.servlet.http.HttpServletResponse.SC_METHOD_NOT_ALLOWED

class ReadonlyRestfulController<T> extends RestfulController<T> {
    ReadonlyRestfulController(Class<T> resource) {
        super(resource)
    }

    def create() {
        response.sendError(SC_METHOD_NOT_ALLOWED, 'This resource is read-only')
    }
    def save() {
        response.sendError(SC_METHOD_NOT_ALLOWED, 'This resource is read-only')
    }
    def edit() {
        response.sendError(SC_METHOD_NOT_ALLOWED, 'This resource is read-only')
    }
    def update() {
        response.sendError(SC_METHOD_NOT_ALLOWED, 'This resource is read-only')
    }
    def delete() {
        response.sendError(SC_METHOD_NOT_ALLOWED, 'This resource is read-only')
    }
}

package cacoethes

import grails.plugin.i18nEnums.annotations.I18nEnum

@I18nEnum
enum Level {
    BEGINNER,
    INTERMEDIATE,
    ADVANCED
}
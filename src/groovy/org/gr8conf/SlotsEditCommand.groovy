package org.gr8conf

import cacoethes.Conference
import grails.validation.Validateable
import org.grails.databinding.BindingFormat
import org.joda.time.LocalDate

@Validateable
class SlotsEditCommand extends SlotsCRUDCommand {
    List<Slot> slots = []
}

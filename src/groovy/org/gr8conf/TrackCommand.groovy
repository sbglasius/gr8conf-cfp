package org.gr8conf

import grails.validation.Validateable

@Validateable
class TrackCommand {
    List<SlotCommand> slot
}

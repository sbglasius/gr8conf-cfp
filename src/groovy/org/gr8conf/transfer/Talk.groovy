package org.gr8conf.transfer

class Talk extends AgendaItem {
    
    Long id
    String summary
    Speaker speaker
    List<String> tags
}

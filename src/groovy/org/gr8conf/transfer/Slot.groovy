package org.gr8conf.transfer

import org.joda.time.LocalDate
import org.joda.time.LocalTime
class Slot {
    LocalDate date
    LocalTime startTime
    LocalTime endTime
    Boolean allColumns = false
    AgendaItem item
}

package org.gr8conf.transfer

import org.joda.time.LocalDate
import org.joda.time.LocalTime
class AgendaDay {
    List<Slot> slots
    List<Track> tracks
    
    LocalDate date
    LocalTime startTime
    LocalTime endTime
}

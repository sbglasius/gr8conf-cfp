package org.gr8conf.transfer
class Track {
    String name
    Boolean allColumns
    String color
    List<AgendaItem> agendaItems
}

package org.gr8conf.transfer
class Speaker {
    Long id
    String name
    String employer
    String twitter
    String bio
    String shortBio
    String image

    List<Talk> talks
}

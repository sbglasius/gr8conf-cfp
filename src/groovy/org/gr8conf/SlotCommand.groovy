package org.gr8conf

import cacoethes.Conference
import cacoethes.Submission
import grails.validation.Validateable

@Validateable
class SlotCommand {
    ScheduledItem scheduledItem
    Conference conference
    Slot slot
    Track track
    String name
    String icon
    Submission submission

    String toString() {
        this.dump()
    }

    boolean isEmpty() {
        !name && !icon && !submission
    }

    boolean isChanged() {
        scheduledItem?.name != name || scheduledItem?.icon != icon || scheduledItem.submission != submission
    }
}

package org.gr8conf

import org.grails.databinding.BindingHelper
import org.grails.databinding.DataBindingSource
import org.joda.time.LocalTime
import org.joda.time.format.DateTimeFormat

class LocalTimeBindingHelper implements BindingHelper<LocalTime> {

    /**
     * The value returned from this method will be bound to
     * the property specified by propertyName.
     *
     * @param obj The object that data binding is being applied to
     * @param propertyName The name of the property data binding is being applied to
     * @param source The Map containing all of the values being bound to this object
     * @return The value which should be bound to propertyName
     */
    @Override
    LocalTime getPropertyValue(Object obj, String propertyName, DataBindingSource source) {
        String value = source.getPropertyValue(propertyName)
        return LocalTime.parse(value, DateTimeFormat.forPattern('HH:mm'))
    }
}

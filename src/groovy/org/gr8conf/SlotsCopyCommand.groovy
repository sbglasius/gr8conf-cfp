package org.gr8conf

import cacoethes.Conference
import org.grails.databinding.BindingFormat
import org.joda.time.LocalDate

class SlotsCopyCommand extends SlotsCRUDCommand {
    List<Slot> slots = []
}

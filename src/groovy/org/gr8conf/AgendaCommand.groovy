package org.gr8conf

import grails.validation.Validateable

@Validateable
class AgendaCommand {
    List<TrackCommand> track
}

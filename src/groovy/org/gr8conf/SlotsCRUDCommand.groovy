package org.gr8conf

import cacoethes.Conference
import org.grails.databinding.BindingFormat
import org.joda.time.LocalDate

abstract class SlotsCRUDCommand {
    Conference conference
    @BindingFormat('yyyy-MM-dd')
    LocalDate date
}

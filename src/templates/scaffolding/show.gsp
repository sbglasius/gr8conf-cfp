<% import grails.persistence.Event %>
<%=packageName%>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>

		<div id="show-${domainClass.propertyName}" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="\${flash.message}">
			<div class="message" role="status">\${flash.message}</div>
			</g:if>
			<%  excludedProps = Event.allEvents.toList() << 'id' << 'version'
				allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
				props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) }
				Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
				props.each { p -> %>
				<g:if test="\${${propertyName}?.${p.name}}">
                    <f:display bean="\${${propertyName}}" property="${p.name}">
					<%  if (p.isEnum()) { %>
						<g:fieldValue bean="\${${propertyName}}" field="${p.name}"/>
					<%  } else if (p.oneToMany || p.manyToMany) { %>
						<g:each in="\${${propertyName}.${p.name}}" var="${p.name[0]}">
						<g:link controller="${p.referencedDomainClass?.propertyName}" action="show" id="\${${p.name[0]}.id}">\${${p.name[0]}?.encodeAsHTML()}</g:link>
						</g:each>
					<%  } else if (p.manyToOne || p.oneToOne) { %>
						<g:link controller="${p.referencedDomainClass?.propertyName}" action="show" id="\${${propertyName}?.${p.name}?.id}">\${${propertyName}?.${p.name}?.encodeAsHTML()}</g:link>
					<%  } else if (p.type == Boolean || p.type == boolean) { %>
						<g:formatBoolean boolean="\${${propertyName}?.${p.name}}" />
					<%  } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
						<g:formatDate date="\${${propertyName}?.${p.name}}" />
					<%  } else if (!p.type.isArray()) { %>
						<g:fieldValue bean="\${${propertyName}}" field="${p.name}"/>
					<%  } %>
                    </f:display>
				</g:if>
			<%  } %>
			</ol>
			<g:form>
				<class class="row">
                    <div class="col-lg-push-2 col-lg-10">
					<g:hiddenField name="id" value="\${${propertyName}?.id}" />
					<g:link class="edit btn btn-default" action="edit" id="\${${propertyName}?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete btn btn-warning" action="delete" value="\${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('\${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                    </div>
				</class>
			</g:form>
		</div>
	</body>
</html>

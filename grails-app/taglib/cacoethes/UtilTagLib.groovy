package cacoethes

import groovy.xml.MarkupBuilder
import org.joda.time.DateTime

class UtilTagLib {
    static defaultEncodeAs = 'raw'

    def countdown = { attrs ->
        if (!attrs.id) throwTagError "Tag [countdown] requires the [id] attribute"
        if (!attrs.date) throwTagError "Tag [countdown] requires the [date] attribute"
        if (!attrs.timeZone) throwTagError "Tag [countdown] requires the [timeZone] attribute"

        if (new DateTime(attrs.timeZone) < attrs.date) {
            r.require modules: "countdown"
            out << "<span id=\"${attrs.id}\"></span> to go"
            r.script {
                """(function() {
                    \$("#${attrs.id}").countdown({
                        until: new Date(${attrs.date.millis}),
                        format: "dhM",
                        layout: "{dn} {dl} {hn} {hl} {mn} {ml}" });
                })();"""
            }
        } else {
            out << "Deadline has passed!"
        }
    }

    def profile = { attrs ->
        if (!attrs.value) return
        if (!attrs.value.instanceOf(Profile)) throwTagError "Tag [profile] wrong [value] attribute type"
        def value = attrs.value as Profile
        def html = new MarkupBuilder(out)
        r.require(module: 'font-awesome')
        html.span {
            a(href: createLink(controller: 'profile', action: 'show', id: value.id), "${value.name}")
            if (value.needTravel) {
                mkp.yield " - "
                i(class: "icon-plane icon-2", '')
            }
            if (value.needAccommodation) {
                mkp.yield " - "
                i(class: "icon-suitcase icon-2", '')

            }
        }
    }

    def company = { attrs ->
        if (!attrs.value) return
        if (!attrs.value.instanceOf(Profile)) throwTagError "Tag [company] wrong [value] attribute type"
        def value = attrs.value as Profile
        def html = new MarkupBuilder(out)
        r.require(module: 'font-awesome')
        html.span {
            mkp.yield "${value.employer}"
            if (value.employerSponsor) {
                mkp.yield " - "
                i(class: "text-warning icon-usd icon-2", '')
            }

        }

    }

    def filterLinks = { attrs ->
        if(!attrs.containsKey('collection')) throwTagError "Tag [filterLinks] missing [collection] attribute"
        if(!attrs.name) throwTagError "Tag [filterLinks] missing [name] attribute"
        def collection = attrs.collection ?: [] as List
        def name = attrs.name as String
        def selected = attrs.selected
        def parameters = attrs.params ?: [:]
        def button = attrs.button
        def links = collection.collect { item ->
            def buttonFace = "btn btn-xs btn-${selected == item ? 'primary':'info'}"
            if(!button) {
                buttonFace = ''
            }
            def id = "${name}.id".toString()
            if(selected == item) {
                parameters.remove(id)
            } else {
                parameters[id] = item.id
            }
            link(action: 'index', params: parameters, class: buttonFace, item.toString())
        }
        out << links.join(button ? ' ' : ', ')
    }

    def activeTab = { attrs ->
        if (!attrs.containsKey('collection')) throwTagError "Tag [activeTab] missing [collection] attribute"
        if (!attrs.match) throwTagError "Tag [activeTab] missing [match] attribute"
        if (!attrs.var) throwTagError "Tag [activeTab] missing [var] attribute"
        Collection collection = attrs.collection as List
        Closure match = attrs.match
        String var = attrs.var
        pageScope[var] = 0
        for(int i=0;i<collection.size();i++) {
            if(match.call(collection[i])) {
                pageScope[var] = i
                break
            }
        }
    }
}


package org.gr8conf.cfp

import grails.converters.JSON

class JQPlotTagLib {
    static namespace = "jqplot"

    def piechart = { attrs ->

        r.require(module: 'jqplot-piechart')

        def id = attrs.id ?: "pie${System.nanoTime()}"
        def series = attrs.series ?: []
        def serie = attrs.serie
        if(!series && !serie) {
            throwTagError("Missing series or serie")
        }
        if(serie) {
            series << serie
        }
        def width = attrs.width ?: '500px'
        def height = attrs.height ?: '200px'

                out << """<div id="${id}" style="height:$height;width:$width; "></div>"""
        out << r.script([:]) {
            """
            \$(document).ready(function(){
                    var ${id} = \$.jqplot('${id}', ${series as JSON}, {
                    grid: {
                        drawBorder: false,
                        drawGridlines: false,
                        background: '#ffffff',
                        shadow:false
                    },
                    axesDefaults: {

                    },
                    seriesDefaults:{
                        renderer:\$.jqplot.PieRenderer, trendline:{ show: true },
                        rendererOptions: {showDataLabels: true}
                     },
                    legend:{ show:
                        true
                    }
                });
            });

            """
        }
    }
}

package org.gr8conf

import grails.plugin.springsecurity.annotation.Secured
import org.springframework.dao.DataIntegrityViolationException

@Secured(['ROLE_ADMIN'])
class SlotController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]


    static defaultAction = "list"

    def list(Integer max) {
        params.max = Math.min(max ?: 500, 500)
        def instances = Slot.createCriteria().list(params) {
            and {
                order('conference', 'asc')
                order('date', 'asc')
                order('startTime', 'asc')
            }
        }
        def slotsByConference = instances.groupBy { it.conference }.collect {
            def byDay = it.value.groupBy { it.date }
            [
                    conference: it.key,
                    days      : byDay.collect {
                        [
                                date : it.key,
                                slots: it.value
                        ]
                    }
            ]
        }.sort { -it.conference.id }
        [slotsByConference: slotsByConference]
    }

    def create() {
        [slotInstance: new Slot(params)]
    }

    def save() {
        def slotInstance = new Slot(params)
        if (!slotInstance.save(flush: true)) {
            render(view: "create", model: [slotInstance: slotInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'slot.label', default: 'Slot'), slotInstance.id])
        redirect(action: "show", id: slotInstance.id)
    }

    def edit(Long id) {
        def slotInstance = Slot.get(id)
        if (!slotInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'slot.label', default: 'Slot'), id])
            redirect(action: "list")
            return
        }

        [slotInstance: slotInstance]
    }

    def update(Long id, Long version) {
        def slotInstance = Slot.get(id)
        if (!slotInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'slot.label', default: 'Slot'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (slotInstance.version > version) {
                slotInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'slot.label', default: 'Slot')] as Object[],
                        "Another user has updated this Slot while you were editing")
                render(view: "edit", model: [slotInstance: slotInstance])
                return
            }
        }

        slotInstance.properties = params

        if (!slotInstance.save(flush: true)) {
            render(view: "edit", model: [slotInstance: slotInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'slot.label', default: 'Slot'), slotInstance.id])
        redirect(action: "show", id: slotInstance.id)
    }

    def delete(Long id) {
        def slotInstance = Slot.get(id)
        if (!slotInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'slot.label', default: 'Slot'), id])
            redirect(action: "list")
            return
        }

        try {
            long conference = slotInstance.conference.id
            slotInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'slot.label', default: 'Slot'), id])
            redirect(action: "list", params: [conference: conference])
        }
        catch (DataIntegrityViolationException ignored) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'slot.label', default: 'Slot'), id])
            redirect(action: "show", id: id)
        }
    }

    def deleteSlots(SlotsDeleteCommand slotsDeleteCommand) {
        def slots = Slot.findAllByConferenceAndDate(slotsDeleteCommand.conference, slotsDeleteCommand.date)

        def hasScheduledItem = { Slot slot -> ScheduledItem.findBySlot(slot) }
        if (slots.any(hasScheduledItem)) {
            flash.message = "Slots for ${slotsDeleteCommand.date.format('d. MMMM - yyyy')} cannot be deleted as they are referenced in an agenda"
        } else {
            slots*.delete()
            flash.message = "Slots for ${slotsDeleteCommand.date.format('d. MMMM - yyyy')} was deleted"
        }
        redirect(action: 'list')
    }

    def editSlots(SlotsEditCommand slotsEditCommand) {
        if (request.method == 'GET') {
            slotsEditCommand.slots = Slot.findAllByConferenceAndDate(slotsEditCommand.conference, slotsEditCommand.date, [sort: 'startTime'])
            [slotsEditCommand: slotsEditCommand]
        } else {
            slotsEditCommand.slots.each { Slot slot ->
                if(!slot.startTime || !slot.endTime) {
                    // Skip slot because startTime or endTime is null
                    return
                }
                if (!slot.id) {
                    slot.conference = slotsEditCommand.conference
                    slot.date = slotsEditCommand.date
                }
                slot.save(failOnError: true)
            }
            flash.message = "Slots for ${slotsEditCommand.date.format('d. MMMM - yyyy')} was saved"
            redirect(action: 'list', params: [conference: slotsEditCommand.conference.id, date: slotsEditCommand.date.format('yyyy-MM-dd')])
        }
    }


    def copySlots(SlotsCopyCommand slotsCopyCommand) {
        if (request.method == 'GET') {
            def slots = Slot.findAllByConferenceAndDate(slotsCopyCommand.conference, slotsCopyCommand.date)

            slotsCopyCommand.slots = slots
            [slotsCopyCommand: slotsCopyCommand]
        } else {
            def slotsForConference = Slot.findAllByConference(slotsCopyCommand.conference)
            if (slotsForConference.any { it.date == slotsCopyCommand.date }) {
                flash.message = "You cannot add more slots to a date already having slots."
                [slotsCopyCommand: slotsCopyCommand]
            } else {
                slotsCopyCommand.slots.each {
                    it.conference = slotsCopyCommand.conference
                    it.date = slotsCopyCommand.date
                    it.save(failOnError: true)
                }

                redirect(action: 'list', params: [conference: slotsCopyCommand.conference.id, date: slotsCopyCommand.date.format('yyyy-MM-dd')])
            }
        }
    }
}



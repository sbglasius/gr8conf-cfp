package org.gr8conf.api

import cacoethes.Conference
import cacoethes.ReadonlyRestfulController
import cacoethes.Submission

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import org.gr8conf.ScheduledItem
import org.gr8conf.Slot
import org.gr8conf.Track
import org.gr8conf.AgendaTranslatorService
import org.gr8conf.transfer.Agenda

@Secured(['permitAll'])
class AgendaApiController extends ReadonlyRestfulController<Submission>{
    static responseFormats = ['json']
    static allowedMethods = ['GET']

    def submissionService
    AgendaTranslatorService agendaTranslatorService
    AgendaApiController() {
        super(Submission)
    }

    def index(Long conferenceId) {
        Conference conference = Conference.get(conferenceId)
        List<Slot> slots = Slot.findAllByConference(conference)
        List<Track> tracks = Track.findAllByConference(conference)
        List<ScheduledItem> scheduledItems = ScheduledItem.findAllByConference(conference)
        Agenda agenda = agendaTranslatorService.toAgenda(tracks, slots, scheduledItems)
        
        response.addHeader("Access-Control-Allow-Origin", "*");
        render agenda as JSON
    }
}

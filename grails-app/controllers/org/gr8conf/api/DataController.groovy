package org.gr8conf.api

import cacoethes.Conference
import cacoethes.Submission
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.Validateable
import groovy.transform.ToString
import org.gr8conf.client.ClientProfile

import javax.servlet.http.HttpServletResponse

@Secured(["permitAll"])
class DataController {
    def favoriteService
    def ratingService
    def agendaDataService

    def status(Conference conference, String uuid) {
        if (!conference) {
            response.sendError HttpServletResponse.SC_NOT_FOUND
            return
        }
        render(agendaDataService.getStatus(conference, uuid) as JSON)
    }


    def speakers(Conference conference, Boolean featured) {
        if (!conference) {
            response.sendError HttpServletResponse.SC_NOT_FOUND
            return
        }
        if(featured == null) {
            featured = false
        }
        def speakers = agendaDataService.getSpeakers(conference,featured)
        render speakers as JSON
    }

    def talks(Conference conference) {
        if (!conference) {
            response.sendError HttpServletResponse.SC_NOT_FOUND
            return
        }
        List talks = agendaDataService.getTalks(conference)

        render talks as JSON
    }


    def agenda(Conference conference) {
        if (!conference) {
            response.sendError HttpServletResponse.SC_NOT_FOUND
            return
        }
        def data = agendaDataService.buildAgenda(conference)

        render data as JSON
    }


    def tags(Conference conference) {
        if (!conference) {
            response.sendError HttpServletResponse.SC_NOT_FOUND
            return
        }
        def tags = agendaDataService.getTags(conference)
        render(tags as JSON)

    }

    def loadFavorites(ClientDataCommand command) {
        if (command.hasErrors()) {
            log.warn("LoadFavorites command errors: ${command.errors}")
            response.sendError HttpServletResponse.SC_NOT_FOUND
            return
        }
        def favorites = favoriteService.getCurrentFavorites(command)

        render(favorites as JSON)
    }


    def saveFavorite(SaveFavoriteCommand command) {
        if (command.hasErrors()) {
            log.warn("SaveFavorites command errors: ${command.errors}")
            response.sendError HttpServletResponse.SC_NOT_FOUND
            return
        }

        def favorites = favoriteService.saveFavorite(command)
        render(favorites as JSON)
    }

    def loadRatings(ClientDataCommand command) {
        log.warn("LoadRatings command errors: ${command.errors}")
        if (command.hasErrors()) {
            response.sendError HttpServletResponse.SC_NOT_FOUND
            return
        }
        def ratings = ratingService.getCurrentRatings(command)

        render(ratings as JSON)

    }
    def saveRating(SaveRatingCommand command) {
        println request.JSON
        if (command.hasErrors()) {
            log.warn("SaveRatings command errors: ${command.errors}")
            response.sendError HttpServletResponse.SC_NOT_FOUND
            return
        }

        def ratings = ratingService.saveRating(command)
        render(ratings as JSON)
    }
    def loadConferenceRating(ClientDataCommand command) {
        log.warn("LoadConferenceRatings command errors: ${command.errors}")
        if (command.hasErrors()) {
            response.sendError HttpServletResponse.SC_NOT_FOUND
            return
        }
        def ratings = ratingService.getConferenceRating(command)
        if(ratings) {
            render(ratings as JSON)
        } else {
            response.sendError HttpServletResponse.SC_NO_CONTENT
        }

    }
    def saveConferenceRating(SaveConferenceRatingCommand command) {
        println params
        if (command.hasErrors()) {
            log.warn("SaveConferenceRating command errors: ${command.errors}")
            response.sendError HttpServletResponse.SC_NOT_FOUND
            return
        }

        def rating = ratingService.saveConferenceRating(command)
        render(rating as JSON)
    }

    def conferences() {
        def conferences = agendaDataService.conferences

        render(conferences as JSON)
    }
    def conferencesGrouped() {
        def conferences = agendaDataService.conferences.groupBy {
            it.start.year
        }

        render(conferences as JSON)
    }


}

@ToString(includeNames = true)
@Validateable
class ClientDataCommand {
    Conference conference
    String uuid

    ClientProfile getClientProfile() {
        ClientProfile.findByUuid(uuid)
    }

    static constraints = {
        conference nullable: false
        uuid nullable: false, validator: { value, object ->
            if(!ClientProfile.findByUuid(value)) return ['not found']
        }
    }
}

@ToString(includeNames = true, includeSuper = true)
@Validateable
class SaveFavoriteCommand extends ClientDataCommand {
    Submission talk
    boolean status
    // Hide inner structure from API. Useful for dataBinding
    Submission getSubmission() {
        return talk
    }
    static constraints = {
        talk nullable: false
    }
}


@ToString(includeNames = true, includeSuper = true)
@Validateable
class SaveRatingCommand extends ClientDataCommand {
    Submission talk
    Integer talkRelevance
    Integer talkQuality
    Integer talkLevel
    String talkComment
    Integer speakerQuality
    String speakerComment

    // Hide inner structure from API. Useful for dataBinding
    Submission getSubmission() {
        return talk
    }
    static constraints = {
        talk nullable: false
        talkRelevance nullable: true, range: 1..5
        talkQuality nullable: true, range: 1..5
        talkLevel nullable: true, range: 1..5
        talkComment nullable: true, maxSize: 1000
        speakerQuality nullable: true, range: 1..5
        speakerComment nullable: true, maxSize: 1000
    }
}

@ToString(includeNames = true, includeSuper = true)
class SaveConferenceRatingCommand extends ClientDataCommand {
    Integer overall
    String overallComments

    Integer preInfo
    Integer preWebsite
    Integer preRegistration
    String preComments

    Integer venueImpression
    Integer venueCatering
    Integer venueWorkshopRooms
    Integer venueAuditoriums
    String venueComments

    Integer confTopicRelevance
    Integer confPresentations
    String confComments

    Integer beBackSelf
    Integer beBackBoss

    static constraints = {
        overall nullable: true, range: 1..5
        overallComments nullable: true

        preComments nullable: true
        preInfo nullable: true, range: 1..5
        preWebsite nullable: true, range: 1..5
        preRegistration nullable: true, range: 1..5

        venueImpression nullable: true, range: 1..5
        venueCatering nullable: true, range: 1..5
        venueWorkshopRooms nullable: true, range: 1..5
        venueAuditoriums nullable: true, range: 1..5
        venueComments nullable: true

        confTopicRelevance nullable: true, range: 1..5
        confPresentations nullable: true, range: 1..5
        confComments nullable: true

        beBackSelf nullable: true, range: 1..5
        beBackBoss nullable: true, range: 1..5
    }

}


package org.gr8conf.api

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

@Secured(["permitAll"])
class TwitterSearchController {
    def twitterSearchService

    def latest() {
        render (twitterSearchService.latestSearchResult as JSON)
    }

    def trigger() {
        twitterSearchService.refreshTweets()
        render twitterSearchService.latestSearchResult as JSON
    }
}

package org.gr8conf.api
import cacoethes.Conference
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import groovy.transform.ToString
import org.gr8conf.ClientUserService

@Secured(['permitAll'])
class ClientUserController {
    ClientUserService clientUserService

    def createCode() {
        render([code: clientUserService.stateToken] as JSON)
    }

    def getUser(ClientCode token) {
        def result = clientUserService.userLogin(token)
        log.debug("Get user: $result from $token")
        render(result as JSON)
    }

    def index() {}

}

@ToString(includeNames = true)
class ClientCode {
    Conference conference
    String provider
    String state
    String code
    String name
    String email

    String getTokenKey() {
        "$code-$state"
    }
}

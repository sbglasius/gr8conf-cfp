package org.gr8conf.api
import cacoethes.ReadonlyRestfulController
import cacoethes.Submission
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import org.gr8conf.TalkTranslaterService
import org.gr8conf.transfer.Speaker

@Secured(['permitAll'])
class SpeakerController extends ReadonlyRestfulController<Submission>{
    static responseFormats = ['json']
    static allowedMethods = ['GET']

    def submissionService
    TalkTranslaterService talkTranslaterService
    SpeakerController() {
        super(Submission)
    }

    def index(Long conferenceId) {
        List<Submission> submissions = submissionService.findSubmissionsForConference(conferenceId).sort { it.title }
        List<Speaker> speakers = talkTranslaterService.translateToSpeakerList(submissions, conferenceId).sort { it.name }
        response.addHeader("Access-Control-Allow-Origin", "*");
        
        render speakers as JSON
    }
}

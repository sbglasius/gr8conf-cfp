package org.gr8conf.api

import cacoethes.ReadonlyRestfulController
import cacoethes.Submission

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import org.gr8conf.TalkTranslaterService
import org.gr8conf.transfer.Talk

@Secured(['permitAll'])
class TalkController extends ReadonlyRestfulController<Submission>{
    static responseFormats = ['json']
    static allowedMethods = ['GET']

    def submissionService
    TalkTranslaterService talkTranslaterService
    TalkController() {
        super(Submission)
    }

    def index(Long conferenceId) {
        List<Submission> submissions = submissionService.findSubmissionsForConference(conferenceId)
        List<Talk> talks = talkTranslaterService.translateToTalkList(submissions, conferenceId)
        response.addHeader("Access-Control-Allow-Origin", "*");
        render talks as JSON
    }
}

package org.gr8conf

import cacoethes.Conference
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class AgendaController {
    def submissionService
    def agendaService

    def edit(Conference conference, int activeTab) {
        def scheduledItems = ScheduledItem.findAllByConference(conference)
        def breakTracks = Track.where { conference == conference && breaks == true }.list(order: 'name')
        def allColumnsTracks = Track.where { conference == conference && allColumns == true }.list(order: 'name')
        def normalTracks = Track.where { conference == conference && breaks == false && allColumns == false }.list(order: 'name')
        def tracks = breakTracks + allColumnsTracks + normalTracks
        def groupedSlots = Slot.findAllByConference(conference).sort { a, b -> a.date <=> b.date ?: a.startTime <=> b.startTime }.groupBy {
            it.date
        }
        def acceptedTalks = submissionService.findSubmissionsForConference(conference.id).sort { it.title }
        log.debug("Edit active tab: $activeTab")

        [scheduledItems: scheduledItems,
         tracks        : tracks,
         groupedSlots  : groupedSlots,
         acceptedTalks : acceptedTalks,
         conference    : conference,
         activeTab     : activeTab ?: 0
        ]
    }

    def save(Conference conference, int activeTab, AgendaCommand agenda) {
        log.debug("Save active tab: $activeTab")
        agendaService.saveAgenda(agenda)
        redirect(action: 'edit', id: conference.id, params: [activeTab: activeTab ?: 0])
    }
}





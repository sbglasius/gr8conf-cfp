package org.gr8conf

import cacoethes.Conference
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.dao.DataIntegrityViolationException

@Secured(['ROLE_ADMIN'])
class ScheduledItemController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Long conferenceId) {
        def query = ScheduledItem.where {
            submission == null
        }
        if (conferenceId) {
            query = query.where {
                conference.id == conferenceId
            }
        }
        [scheduledItemInstanceList: query.findAll(), scheduledItemInstanceTotal: ScheduledItem.count()]
    }

    def create(Long conferenceId) {
        Conference conference = Conference.get(conferenceId)
        [scheduledItemInstance: new ScheduledItem(params), 
         conferenceId: conferenceId,
         tracks: Track.findAllByConference(conference),
         slots: Slot.findAllByConference(conference)
        ]
    }

    def save(Long conferenceId) {
        def scheduledItemInstance = new ScheduledItem(params)
        def existing = ScheduledItem.findByTrackAndSlot(scheduledItemInstance.track, scheduledItemInstance.slot)
        if (!scheduledItemInstance.save(flush:true)) {
            render(view: "create", model: [scheduledItemInstance: scheduledItemInstance])
            return
        }
        existing?.delete(flush: true)


        flash.message = message(code: 'default.created.message', args: [message(code: 'scheduledItem.label', default: 'ScheduledItem'), scheduledItemInstance.id])
        redirect(action: "show", id: scheduledItemInstance.id)
    }

    def show(Long id) {
        def scheduledItemInstance = ScheduledItem.get(id)
        if (!scheduledItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'scheduledItem.label', default: 'ScheduledItem'), id])
            redirect(action: "list")
            return
        }

        [scheduledItemInstance: scheduledItemInstance]
    }

    def edit(Long id) {
        def scheduledItemInstance = ScheduledItem.get(id)
        if (!scheduledItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'scheduledItem.label', default: 'ScheduledItem'), id])
            redirect(action: "list")
            return
        }
        [scheduledItemInstance: scheduledItemInstance, 
         conferenceId: scheduledItemInstance.conference.id,
         tracks: Track.findAllByConference(scheduledItemInstance.conference),
         slots: Slot.findAllByConference(scheduledItemInstance.conference)
        ]
    }

    def update(Long id, Long version) {
        def scheduledItemInstance = ScheduledItem.get(id)
        if (!scheduledItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'scheduledItem.label', default: 'ScheduledItem'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (scheduledItemInstance.version > version) {
                scheduledItemInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'scheduledItem.label', default: 'ScheduledItem')] as Object[],
                          "Another user has updated this ScheduledItem while you were editing")
                render(view: "edit", model: [scheduledItemInstance: scheduledItemInstance])
                return
            }
        }

        scheduledItemInstance.properties = params

        if (!scheduledItemInstance.save(flush: true)) {
            render(view: "edit", model: [scheduledItemInstance: scheduledItemInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'scheduledItem.label', default: 'ScheduledItem'), scheduledItemInstance.id])
        redirect(action: "show", id: scheduledItemInstance.id)
    }

    def delete(Long id) {
        def scheduledItemInstance = ScheduledItem.get(id)
        if (!scheduledItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'scheduledItem.label', default: 'ScheduledItem'), id])
            redirect(action: "list")
            return
        }

        try {
            scheduledItemInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'scheduledItem.label', default: 'ScheduledItem'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException ignored) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'scheduledItem.label', default: 'ScheduledItem'), id])
            redirect(action: "show", id: id)
        }
    }
}

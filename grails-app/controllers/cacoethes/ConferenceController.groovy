package cacoethes

import grails.plugin.springsecurity.annotation.Secured
import org.joda.time.DateTimeZone
import org.joda.time.LocalDate

@Secured(['ROLE_ADMIN'])
class ConferenceController {
    static scaffold = true
    def ratingService
    def show(Long id) {
        def conferenceInstance = Conference.get(id)
        if (!conferenceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'conference.label', default: 'Conference'), id])
            redirect(action: "list")
            return
        }
        def ratings = ratingService.getRatingsForConference(conferenceInstance)
        [conferenceInstance: conferenceInstance, ratings: ratings]
    }


    def save() {
        def conferenceInstance = new Conference()
        updateFromParams(conferenceInstance)
        if (!conferenceInstance.save(flush: true)) {
            render(view: "create", model: [conferenceInstance: conferenceInstance])
            return
        }
        flash.message = message(code: 'default.created.message', args: [message(code: 'conference.label', default: 'Conference'), conferenceInstance.id])
        redirect uri: "/"
    }

    def update(Long id, Long version) {
        def conferenceInstance = Conference.get(id)
        if (!conferenceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'conference.label', default: 'Conference'), id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            if (conferenceInstance.version > version) {
                conferenceInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'conference.label', default: 'Conference')] as Object[],
                          "Another user has updated this Conference while you were editing")
                render(view: "edit", model: [conferenceInstance: conferenceInstance])
                return
            }
        }

        updateFromParams(conferenceInstance)

        if (!conferenceInstance.save(flush: true)) {
            render(view: "edit", model: [conferenceInstance: conferenceInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'conference.label', default: 'Conference'), conferenceInstance.id])
        redirect(action: "show", id: conferenceInstance.id)
    }

    private void updateFromParams(Conference conference) {
        conference.name = params.name
        conference.location = params.location
        conference.latitude = params.double('latitude')
        conference.longitude = params.double('longitude')
        conference.start = LocalDate.parse(params.start)
        conference.end = LocalDate.parse(params.end)
        conference.cfpDeadline = LocalDate.parse(params.cfpDeadline)
        conference.timeZone = DateTimeZone.forID(params.timeZone)
    }
}

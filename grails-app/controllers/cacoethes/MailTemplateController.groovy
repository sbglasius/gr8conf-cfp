package cacoethes

import grails.plugin.springsecurity.annotation.Secured

@Secured("ROLE_ADMIN")
class MailTemplateController {
    static scaffold = true
}

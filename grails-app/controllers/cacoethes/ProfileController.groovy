package cacoethes

import grails.plugin.springsecurity.annotation.Secured
import org.springframework.dao.DataIntegrityViolationException

import javax.imageio.ImageIO
import javax.servlet.http.HttpServletResponse
import java.awt.*
import java.awt.image.BufferedImage

@Secured("ROLE_USER")
class ProfileController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def securityService

    static defaultAction = "list"

    @Secured("ROLE_ADMIN")
    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [profileInstanceList: Profile.list(params), profileInstanceTotal: Profile.count()]
    }

/*
    @Secured("ROLE_ADMIN")
    def acceptedEmails(Integer forYear) {
        def acceptedTalks = Submission.where {
            accepted == true
            if (forYear) {
                year == forYear
            }
        }.list()

        def profiles = acceptedTalks.collect { it.user.profile }.unique()
        render profiles*.email.join(", ")
    }
*/

/*
    @Secured("ROLE_ADMIN")
    def submittedEmails(Integer forYear) {
        def submittedTalks = Submission.where {
            if (forYear) {
                year == forYear
            }
        }.property("user").list()

        def profiles = submittedTalks*.profile.unique()
        render profiles*.email.join(", ")
    }
*/

/*
    @Secured("ROLE_ADMIN")
    def acceptedExpenses(Integer forYear) {
        def acceptedTalks = Submission.where {
            accepted == true
            if (forYear) {
                year == forYear
            }
        }.list()

        def profiles = acceptedTalks.collect { it.user.profile }.unique()
        profiles = profiles.findAll { it.needTravel || it.needAccommodation }

        [profiles: profiles]
    }
*/

    def create() {
        // If a profile already exists, go to the edit view.
        def user = securityService.currentUser

        if (!securityService.isAdmin() && user.profile) {
            redirect action: "edit", id: user.profile.id
            return
        }
        params.twitter = session['twitterName']
        [profileInstance: new Profile(params)]
    }

    def save() {
        def profileInstance = new Profile(params)
        if (securityService.currentUser.username != "admin") profileInstance.user = securityService.currentUser
        if (!profileInstance.save(flush: true)) {
            render(view: "create", model: [profileInstance: profileInstance])
            return
        }
        flash.message = message(code: 'profile.created.message')
        redirect uri: "/"
    }

    def show(Long id) {
        def profileInstance = Profile.get(id)
        if (!profileInstance || !checkProfileAccess(profileInstance)) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'profile.label', default: 'Profile'), id])
            redirect(action: "list")
            return
        }

        [profileInstance: profileInstance]
    }

    def edit(Long id) {
        def profileInstance = Profile.get(id)
        if (!profileInstance || !checkProfileAccess(profileInstance)) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'profile.label', default: 'Profile'), id])
            redirect(action: "list")
            return
        }

        [profileInstance: profileInstance]
    }

    def update(Long id, Long version) {
        def profileInstance = Profile.get(id)
        if (!profileInstance || !checkProfileAccess(profileInstance)) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'profile.label', default: 'Profile'), id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            if (profileInstance.version > version) {
                profileInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'profile.label', default: 'Profile')] as Object[],
                        "Another user has updated this Profile while you were editing")
                render(view: "edit", model: [profileInstance: profileInstance])
                return
            }
        }
        profileInstance.featured.clear() // Remove featured before update
        profileInstance.properties = params

        if (!profileInstance.save(flush: true)) {
            render(view: "edit", model: [profileInstance: profileInstance])
            return
        }

        flash.message = message(code: 'profile.updated.message')
        redirect(action: "show", id: profileInstance.id)
    }

    @Secured("ROLE_ADMIN")
    def delete(Long id) {
        def profileInstance = Profile.get(id)
        if (!profileInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'profile.label', default: 'Profile'), id])
            redirect(action: "list")
            return
        }

        try {
            profileInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'profile.label', default: 'Profile'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException ignored) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'profile.label', default: 'Profile'), id])
            redirect(action: "show", id: params.id)
        }
    }

    def loggedIn() {
        [currentUser: securityService.currentUser]
    }

    def setImage(Long id) {
        def profileInstance = Profile.get(id)
        if (!profileInstance || !checkProfileAccess(profileInstance)) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'profile.label', default: 'Profile'), params.id])
            redirect(action: "list")
            return
        }

        [profileInstance: profileInstance]
    }

    def uploadImage(UploadProfileImageCommand command) {
        def profile = command.profile
        if(!profile || !checkProfileAccess(profile)) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'profile.label', default: 'Profile'), params.id])
            redirect(action: "list")
            return
        }
        def profileImage = profile.profileImage ?: new ProfileImage(profile: profile)
        profileImage.imageData = command.croppedImage

        profileImage.save()
        redirect(action: 'show', id: profile.id)
    }

    def removeImage(Long id) {
        def profileInstance = Profile.get(id)
        if (!profileInstance || !checkProfileAccess(profileInstance)) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'profile.label', default: 'Profile'), params.id])
            redirect(action: "list")
            return
        }
        profileInstance.profileImage.delete()
        profileInstance.profileImage = null
        profileInstance.save()
        redirect(action: 'show', id: profileInstance.id)
    }

    @Secured("IS_AUTHENTICATED_ANONYMOUSLY")
    def img(Long id) {
        cache shared:true, validFor: 3600  // 1hr on content
        def profileInstance = Profile.get(id)
        if(!profileInstance?.profileImage) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND)
            return
        }
        log.debug("Serving image for $profileInstance.name")
        response.contentType = "image/png"
        response.outputStream << profileInstance.profileImage.imageData
    }

    /**
     * Checks whether the current user has permission to access the given profile.
     */
    private checkProfileAccess(profile) {
        // Check that the user has permission to view this one.
        return securityService.canAccessUserData(profile.user)
    }
}

class UploadProfileImageCommand {
    Profile profile
    int x1
    int x2
    int y1
    int y2
    int width
    int height
    byte[] image
    String type

    public byte[] getCroppedImage() {
        def img = ImageIO.read(new ByteArrayInputStream(image))
        def cropped = img.getSubimage(x1, y1, width, height)
        def scaled = cropped.getScaledInstance(145, 145, Image.SCALE_SMOOTH);
        def finalImage = new BufferedImage(scaled.getWidth(null), scaled.getHeight(null), BufferedImage.TYPE_INT_RGB);
        finalImage.getGraphics().drawImage(scaled, 0, 0, null);
        def out = new ByteArrayOutputStream()
        ImageIO.write(finalImage, "png", out)
        return out.toByteArray()
    }
}

package cacoethes

import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import groovy.text.SimpleTemplateEngine
import org.joda.time.LocalDate
import org.springframework.dao.DataIntegrityViolationException

@Secured("ROLE_USER")
class SubmissionController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", sendStatusEmail: "POST"]

    def grailsApplication
    def sendGridService
    def securityService
    def ratingService


    def index(SearchCommand command) {
        // Can't view submissions until a profile is created.
        if (!securityService.isAdmin() && !securityService.currentUser.profile) {
            redirect controller: "profile", action: "create"
            return
        }



//        params.max = Math.min(params.max ? params.int('max') : 200, 200)

        List<Submission> submissions
        def submissionCount
        def year = new LocalDate(2000, 1, 1)

        if (securityService.isAdmin()) {
            if (command.hidePastConferences) {
                def now = LocalDate.now()
                year = year.withYear(now.year)
            }
            def closure = {
                if (command.title) {
                    ilike('title',"%$command.title%")
                }
                if (command.tag) {
                    tags {
                        eq('id', command.tag.id)
                    }
                }
                if (command.duration) {
                    durations {
                        eq('id', command.duration.id)
                    }
                }
                submittedTo {
                    if (command.conference) {
                        eq('id', command.conference.id)
                    }
                    gt('start', year)
                }
                if (command.acceptedFor) {
                    acceptedFor {
                        eq('id', command.acceptedFor.id)
                    }
                }
                if (command.rejectedFor) {
                    rejectedFor {
                        eq('id', command.rejectedFor.id)
                    }
                }
            }

            submissions = Submission.createCriteria().listDistinct(closure)
            submissions = submissions.sort { it.profile.name }
            submissionCount = submissions.size()
        } else {
            submissions = Submission.findAllByUser(securityService.currentUser, params)
            submissionCount = Submission.countByUser(securityService.currentUser)
        }

        def upcomingConferences = Conference.activeConferences.list()

        [
                submissionInstanceList : submissions,
                submissionInstanceTotal: submissionCount,
                conferences            : upcomingConferences,
                allTags                : activeTags,
                allDurations           : Duration.list().sort { it.minutes },
                year                   : year,
                command                : command
        ]
    }

    private List<Tag> getActiveTags() {
        Submission.list().tags.flatten().unique().sort { it.name.toLowerCase() }
    }

    def create() {
        [submissionInstance: new Submission(params), conferences: Conference.list()]
    }

    def save() {
        def submissionInstance = new Submission()
        def tags = resolveTags(params.remove('tags') as String)
        if (SpringSecurityUtils.ifAnyGranted('ROLE_ADMIN')) {
            bindData submissionInstance, params
        } else {
            bindData submissionInstance, params, ["user", "additionalSpeakers"]
            submissionInstance.user = securityService.currentUser
        }
        submissionInstance.tags = tags

        if (!submissionInstance.save(flush: true)) {
            render view: "create", model: [submissionInstance: submissionInstance]
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'submission.label', default: 'Submission'), submissionInstance.id])
        redirect action: 'index'
    }

    private resolveTags(String tagsString) {
        tagsString?.split(',')?.collect { tag ->
            tag = tag.trim().toLowerCase().capitalize()
            Tag.findByName(tag) ?: new Tag(name: tag).save(flush: true)
        } ?: []

    }

    def show() {
        def submissionInstance = Submission.get(params.id)

        if (!submissionInstance || !checkSubmissionAccess(submissionInstance)) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'submission.label', default: 'Submission'), params.id])
            redirect(action: 'index')
            return
        }
        def ratings = ratingService.getRatingsForSubmission(submissionInstance)
        [submissionInstance: submissionInstance, ratings: ratings]
    }

    def edit() {
        def submissionInstance = Submission.get(params.id)

        if (!submissionInstance || !checkSubmissionAccess(submissionInstance)) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'submission.label', default: 'Submission'), params.id])
            redirect(action: 'index')
            return
        }

        [submissionInstance: submissionInstance,
        ]
    }

    def update() {
        def submissionInstance = Submission.get(params.id)

        if (!submissionInstance || !checkSubmissionAccess(submissionInstance)) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'submission.label', default: 'Submission'), params.id])
            redirect(action: 'index')
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (submissionInstance.version > version) {
                submissionInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'submission.label', default: 'Submission')] as Object[],
                        "Another user has updated this Submission while you were editing")
                render(view: "edit", model: [submissionInstance: submissionInstance])
                return
            }
        }
        def tags = resolveTags(params.remove('tags') as String)

        if (SpringSecurityUtils.ifAnyGranted('ROLE_ADMIN')) {
            submissionInstance.additionalSpeakers.clear()
            bindData submissionInstance, params
        } else {
            bindData submissionInstance, params, ["user", "additionalSpeakers"]
        }
        submissionInstance.tags = tags

        if (!submissionInstance.save(flush: true)) {
            render view: "edit", model: [
                    submissionInstance: submissionInstance
            ]
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'submission.label', default: 'Submission'), submissionInstance.id])
        redirect(action: "show", id: submissionInstance.id)
    }

    @Secured("ROLE_ADMIN")
    def changeStatus(Long id, Long conferenceId, String status) {
        Submission submission = Submission.get(id)
        Conference conference = Conference.get(conferenceId)
        if (submission.validate()) {
            switch (status) {
                case 'accept':
                    submission.acceptFor(conference)
                    break
                case 'reject':
                    submission.rejectFor(conference)
                    break
                default:
                    submission.makeUndecided(conference)
            }
            submission.save(failOnError: true)
        }

        render template: 'acceptStatus', model: [submission: submission, conference: conference]
    }

    @Secured("ROLE_ADMIN")
    def delete() {
        def submissionInstance = Submission.get(params.id)
        if (!submissionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'submission.label', default: 'Submission'), params.id])
            redirect(action: 'index')
            return
        }

        try {
            submissionInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'submission.label', default: 'Submission'), params.id])
            redirect(action: 'index')
        }
        catch (DataIntegrityViolationException ignored) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'submission.label', default: 'Submission'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

//    def schedule(Integer forYear) {
//        if (!forYear) forYear = currentYear
//        def talks = TalkAssignment.where { talk.year == forYear }.list()
//
//        def schedule = createSchedule(talks)
//        [schedule: schedule, year: forYear, trackNames: Track.list(sort: "id")*.name]
//    }

//    @Secured("ROLE_ADMIN")
//    def conflicts() {
//        def talks = TalkAssignment.where { talk.year == currentYear }.list()
//        def conflicts = talks.groupBy { it.track?.name + "_" + it.slot?.toString() }.findAll { key, value -> value?.size() > 1 }
//
//        render {
//            ul {
//                for (conflict in conflicts) {
//                    li confict.key + ": " + conflict.value*.talk*.title.join(', ')
//                }
//            }
//        }
//    }

    @Secured("ROLE_ADMIN")
    def sendStatusEmail(Long id) {
        def submissionInstance = Submission.get(id)
        if (!submissionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'submission.label', default: 'Submission'), params.id])
            redirect action: "show", params: [id: id]
            return
        }

        // Can't send an email for a submission that is still pending.
        if (submissionInstance.accepted == null) {
            // Status is still pending, so cannot send an email.
            flash.message = message(code: 'message.submission.pending')
            redirect action: "show", params: [id: id]
            return
        }

        // Pick the appropriate email template based on the status.
        def emailTemplate = MailTemplate.where { key == (submissionInstance.accepted ? "accepted" : "rejected") }.get()
        def binding = createTemplateBinding(submissionInstance)
        def engine = new SimpleTemplateEngine()

        def config = grailsApplication.config
        sendGridService.sendMail {
            from config.grails.mail.default.from
            to engine.createTemplate(emailTemplate.to).make(binding).toString()

            if (emailTemplate.cc) {
                cc engine.createTemplate(emailTemplate.cc).make(binding).toString()
            }

            if (emailTemplate.bcc) {
                bcc engine.createTemplate(emailTemplate.bcc).make(binding).toString()
            }

            subject engine.createTemplate(emailTemplate.subject).make(binding).toString()
            body engine.createTemplate(emailTemplate.body).make(binding).toString()
        }

        flash.message = message(code: 'message.submission.emailSent')
        redirect action: "show", params: [id: id]
    }

    def listREST() {

        respond Submission.list(params)
    }

    def listAsJson() {
        render Submission.all as JSON
    }

    private createTemplateBinding(submission) {
        def profile = submission.user.profile
        return [email: profile.email, name: profile.name, title: submission.title]
    }

    /**
     * Checks whether the current user has permission to access the given submission.
     */
    private checkSubmissionAccess(Submission submissionInstance) {
        // Check that the user has permission to view this one.
        return securityService.canAccessUserData(submissionInstance.user)
    }
}

class SearchCommand {
    boolean hidePastConferences = true
    Conference conference
    Conference acceptedFor
    Conference rejectedFor
    Tag tag
    Duration duration
    String title

    Map getParams() {
        def map = ["conference", "tag", "duration", "acceptedFor", "rejectedFor"].findAll { this."$it" }.collectEntries { ["${it}.id".toString(), this."${it}".id.toString()] }
        map.hidePastConferences = !hidePastConferences
        if (title) {
            map.title = title
        }
        return map
    }

    boolean getHasParams() {
        !hidePastConferences ? params.size() > 0 : params.size() > 1
    }

    String toString() {
        "params: $params, hasParams: $hasParams"
    }
}


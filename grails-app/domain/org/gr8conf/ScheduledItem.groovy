package org.gr8conf

import cacoethes.Conference
import cacoethes.Submission
class ScheduledItem {

    String name
    String icon
    Conference conference
    Track track
    Slot slot

    Submission submission
    Date dateCreated
    Date lastUpdated

    static nameOrSubmissionValidtor = { val, obj ->
        if (!obj.submission && !obj.name) {
            return ['nullNameAndSubmission']
        }
        if (obj.submission && obj.name) {
            return['populatedNameAndSubmission']
        }
    } 
    static constraints = {
        icon nullable: true
        submission nullable: true, validator: nameOrSubmissionValidtor
        name nullable: true, validator: nameOrSubmissionValidtor
        dateCreated nullable: true
        lastUpdated nullable: true
    }
}

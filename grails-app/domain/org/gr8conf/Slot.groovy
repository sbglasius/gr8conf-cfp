package org.gr8conf

import cacoethes.Conference
import org.grails.databinding.BindUsing
import org.jadira.usertype.dateandtime.joda.PersistentLocalDate
import org.jadira.usertype.dateandtime.joda.PersistentLocalTime
import org.joda.time.LocalDate
import org.joda.time.LocalTime
import org.joda.time.Period

@BindUsing(LocalTimeBindingHelper)
class Slot {
    Conference conference
    LocalDate date
    LocalTime startTime
    LocalTime endTime

    int getDurationInMinutes() {
        Period p = new Period(startTime, endTime)
        p.hours * 60 + p.minutes
    }
    static constraints = {
        date nullable: true
        startTime nullable: true
        endTime nullable: true

    }

    static mapping = {
        date type: PersistentLocalDate
        startTime type: PersistentLocalTime
        endTime type: PersistentLocalTime
    }

    String toString() {
        "${date?.toString('M/dd')}: ${startTime?.toString('hh:mm')} - ${endTime?.toString('hh:mm')}"
    }
}

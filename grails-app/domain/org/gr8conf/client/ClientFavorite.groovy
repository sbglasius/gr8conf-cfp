package org.gr8conf.client

import cacoethes.Conference
import cacoethes.Submission

class ClientFavorite {
    ClientProfile clientProfile
    Conference conference
    Submission submission
    Date dateCreated
    Date lastUpdated


    static constraints = {
        dateCreated nullable: true
        lastUpdated nullable: true
    }
}

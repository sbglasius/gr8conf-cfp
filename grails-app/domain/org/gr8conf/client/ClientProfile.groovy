package org.gr8conf.client

class ClientProfile {
    String uuid
    String screenName
    String name
    String email
    boolean verified = false

    static hasOne = [twitterProfile: ClientTwitterProfile, googleProfile: ClientGoogleProfile, githubProfile: ClientGithubProfile]

    static constraints = {
        screenName blank: false
        name blank: false
        email blank: false, email: true
        twitterProfile nullable: true
        googleProfile nullable: true
        githubProfile nullable: true
    }

    def beforeValidate() {
        if (!uuid) {
            uuid = UUID.randomUUID().toString()
        }
    }
}

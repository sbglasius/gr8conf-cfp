package org.gr8conf.client

class ClientTwitterProfile {
    String screenName
    String url
    String profileImageUrl
    String oauthToken
    String oauthTokenSecret


    static belongsTo = [clientProfile: ClientProfile]

    static constraints = {
        screenName nullable: false
        url nullable: true, url: true
        profileImageUrl nullable: true, url: true
    }
}

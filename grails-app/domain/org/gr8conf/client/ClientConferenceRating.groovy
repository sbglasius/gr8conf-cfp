package org.gr8conf.client

import cacoethes.Conference

class ClientConferenceRating {
    ClientProfile clientProfile
    Conference conference

    Integer overall
    String overallComments

    Integer preInfo
    Integer preWebsite
    Integer preRegistration
    String preComments

    Integer venueImpression
    Integer venueCatering
    Integer venueWorkshopRooms
    Integer venueAuditoriums
    String venueComments

    Integer confTopicRelevance
    Integer confPresentations
    String confComments

    Integer beBackSelf
    Integer beBackBoss

    Date dateCreated
    Date lastUpdated

    static constraints = {
        dateCreated nullable: true
        lastUpdated nullable: true
        overall nullable: true, range: 1..5
        overallComments nullable: true

        preComments nullable: true
        preInfo nullable: true, range: 1..5
        preWebsite nullable: true, range: 1..5
        preRegistration nullable: true, range: 1..5

        venueImpression nullable: true, range: 1..5
        venueCatering nullable: true, range: 1..5
        venueWorkshopRooms nullable: true, range: 1..5
        venueAuditoriums nullable: true, range: 1..5
        venueComments nullable: true

        confTopicRelevance nullable: true, range: 1..5
        confPresentations nullable: true, range: 1..5
        confComments nullable: true

        beBackSelf nullable: true, range: 1..5
        beBackBoss nullable: true, range: 1..5
    }

    static mapping = {
        overallComments type: 'text'
        preComments type: 'text'
        venueComments type: 'text'
        confComments type: 'text'
    }
}

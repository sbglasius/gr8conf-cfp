package org.gr8conf.client

class ClientGithubProfile {
    String githubId
    String oauthToken
    String screenName
    String profileImageUrl

    static belongsTo = [clientProfile: ClientProfile]

    static constraints = {
        screenName nullable: true
        profileImageUrl nullable: true, url: true
    }
}

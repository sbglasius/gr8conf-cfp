package org.gr8conf.client

import cacoethes.Conference
import cacoethes.Submission

class ClientRating {
    ClientProfile clientProfile
    Conference conference
    Submission submission
    Integer talkRelevance
    Integer talkQuality
    Integer talkLevel
    String talkComment
    Integer speakerQuality
    String speakerComment

    Date dateCreated
    Date lastUpdated

    static constraints = {
        dateCreated nullable: true
        lastUpdated nullable: true
        talkRelevance nullable: true, range: 1..5
        talkQuality nullable: true, range: 1..5
        talkLevel nullable: true, range: 1..5
        talkComment nullable: true
        speakerQuality nullable: true, range: 1..5
        speakerComment nullable: true
    }

    static mapping = {
        talkComment type: 'text'
        speakerComment type: 'text'
    }
}

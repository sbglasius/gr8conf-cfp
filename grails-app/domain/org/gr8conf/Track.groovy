package org.gr8conf

import cacoethes.Conference
class Track {
    Conference conference
    String  name
    String room
    String color
    Integer roomOrder
    Boolean allColumns = false
    boolean breaks

    static constraints = {
        color nullable:true
        room nullable: true
        roomOrder nullable: true
    }

    String toString() {
        name
    }
}

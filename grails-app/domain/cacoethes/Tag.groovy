package cacoethes

import grails.rest.Resource

@Resource(uri="/rest/tags", formats = ["json","xml"], readOnly = true)
class Tag {
    String name
    static constraints = {
    }
    String toString() {
        name.toLowerCase().capitalize()
    }
}

package cacoethes

class MailNotification {
    Submission submission
    Conference conference
    Date dateCreated
    MailTemplate mailTemplate
    static constraints = {
    }
}

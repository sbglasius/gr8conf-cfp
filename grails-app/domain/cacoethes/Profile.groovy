package cacoethes

import cacoethes.auth.User

class Profile {
    String name
    String employer
    String email
    String twitter
    String shortBio
    String bio
    Boolean needTravel
    Boolean needAccommodation
    String travelFrom
    String travelNotes
    Boolean employerSponsor
    String employerContact

    Date dateCreated
    Date lastUpdated

    static hasOne = [profileImage: ProfileImage ]
    static belongsTo = [user: User]
    static hasMany = [featured: Conference]

    static constraints = {
        name blank: false
        email blank: false, email: true
        twitter nullable: true
        shortBio nullable: true
        bio maxSize: 2000
        needTravel nullable: true
        needAccommodation nullable: true
        travelFrom nullable: true
        travelNotes nullable: true, maxSize: 2000
        employerSponsor nullable: true
        employerContact nullable: true, maxSize: 2000
        profileImage nullable: true
        dateCreated nullable: true
        lastUpdated nullable: true
    }

    static transients = ['submissions']

    Set<Submission> getSubmissions() {
        def mysubmissions = Submission.findAllByUser(this.user)

        //noinspection UnnecessaryQualifiedReference
        mysubmissions + Submission.withCriteria {
            additionalSpeakers {
                eq('user', this.user)
            }
        }
    }
}

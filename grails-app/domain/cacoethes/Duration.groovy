package cacoethes

class Duration {
    String name
    int minutes

    static constraints = {
    }

    String toString() {
        "$name ($minutes min.)"
    }
}

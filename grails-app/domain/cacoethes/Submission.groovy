package cacoethes

import cacoethes.auth.User
import org.gr8conf.ScheduledItem
import org.gr8conf.client.ClientRating

class Submission {
    String title
    String summary
    Level level = Level.INTERMEDIATE
    Date dateCreated
    Date lastUpdated
    Profile getProfile() {
        user.profile
    }

    List<Profile> getSpeakers() {
        def profiles = [profile]
        profiles.addAll(additionalSpeakers)
        profiles
    }

    Map<Conference, List<ClientRating>> getRatingsForConferences() {
        ClientRating.findAllBySubmission(this).groupBy { it.conference }
    }
    
    List<ClientRating> getRatingsForConference(Conference conference) {
        ClientRating.findAllByConferenceAndSubmission(conference, this)
    }

    static transients = ['profile', 'speakers']
    static hasMany = [submittedTo       : Conference,
                      acceptedFor       : Conference,
                      rejectedFor       : Conference,
                      tags              : Tag, durations: Duration,
                      additionalSpeakers: Profile,
                      scheduledTimes    : ScheduledItem]
    static belongsTo = [user: User]

    static constraints = {
        title blank: false
        summary blank: false, maxSize: 1000
        tags minSize: 1
        dateCreated nullable: true
        lastUpdated nullable: true
    }

    //Ensure that lists are updated correctly
    void acceptFor(Conference conference) {
        rejectedFor.remove(conference)
        acceptedFor.add(conference)
    }

    //Ensure that lists are updated correctly
    void rejectFor(Conference conference) {
        acceptedFor.remove(conference)
        rejectedFor.add(conference)
    }

    void makeUndecided(Conference conference) {
        acceptedFor.remove(conference)
        rejectedFor.remove(conference)
    }


    boolean isUndecidedFor(Conference conference) {
        !acceptedFor.contains(conference) && !rejectedFor.contains(conference)
    }

    boolean isAcceptedFor(Conference conference) {
        acceptedFor.contains(conference)
    }

    boolean isRejectedFor(Conference conference) {
        rejectedFor.contains(conference)
    }
}

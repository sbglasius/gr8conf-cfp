package cacoethes

class ProfileImage {
    byte[] imageData

    static belongsTo = [profile: Profile]

    static constraints = {
        imageData nullable: false
        profile nullable: false
    }

    static mapping = {
        imageData sqlType: 'blob'
    }

}

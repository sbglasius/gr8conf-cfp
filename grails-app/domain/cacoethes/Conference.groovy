package cacoethes

import org.jadira.usertype.dateandtime.joda.PersistentDateTimeZoneAsString
import org.jadira.usertype.dateandtime.joda.PersistentLocalDate
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.LocalDate

class Conference {
    String name
    String location
    Double latitude, longitude
    LocalDate start
    LocalDate end
    LocalDate cfpDeadline
    DateTimeZone timeZone

    static transients = ['cfpDateTime', 'currentTimeZone']
    static constraints = {
        name blank: false
        location blank: true
        latitude nullable: true, scale: 7
        longitude nullable: true, scale: 7
        start nullable: false
        end nullable: false
        cfpDeadline nullable: false
        timeZone nullable: false
    }

    static mapping = {
        start type: PersistentLocalDate
        end type: PersistentLocalDate
        cfpDeadline type: PersistentLocalDate
        timeZone type: PersistentDateTimeZoneAsString
    }

    public DateTime getCfpDateTime() {
        cfpDeadline.toDateTimeAtStartOfDay(timeZone)
    }

    public String getCurrentTimeZone() {
        timeZone.getShortName(new DateTime(timeZone).millis)
    }

    static namedQueries = {
        activeConferences {
            gt('end', LocalDate.now())
        }
    }

    String toString() {
        "$name ($location)"
    }

    List<Profile> getProfiles() {
        Profile.findAllByUserInList(submissions.user)

    }

    List<Submission> getSubmissions() {
        Submission.list().findAll {
            this in it.acceptedFor
        }
    }
}

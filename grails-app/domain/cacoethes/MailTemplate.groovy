package cacoethes

class MailTemplate {
    String key
    MailTemplateFilter filter = MailTemplateFilter.ALL
    Conference conference
    String from
    String bcc
    String subject
    String body

    static constraints = {
        key nullable: false
        from nullable: false, email: true
        bcc nullable: true, email: true
        filter nullable: false
        conference nullable: false
        subject nullable: false
        body nullable: false, maxSize: 50000
    }

    static mapping = {
        key column: 'template_key'
        from column: "from_address"
        bcc column: "bcc_address"
    }
}

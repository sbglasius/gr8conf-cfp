package org.gr8conf.cfp



class RefreshTwitterSearchJob {
    def twitterSearchService
    static triggers = {
      simple repeatInterval: 60 * 1000L // execute job once in 30 seconds
    }

    def execute() {
        log.debug "Execute search job"
        twitterSearchService.refreshTweets()
    }
}

grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.server.port.http = 8081
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.dependency.resolver = "maven"
grails.project.dependency.resolution = {
    inherits "global"
    log "warn"
    checksums true

    repositories {
        inherits = true
        grailsPlugins()
        grailsHome()
        grailsCentral()
        mavenCentral()
        mavenRepo "http://maven.springframework.org/milestone/"
        mavenRepo "http://dl.bintray.com/pledbrook/plugins/"
        mavenRepo "http://repo.spring.io/milestone/"
        mavenRepo 'http://repo.desirableobjects.co.uk'
        mavenRepo 'https://raw.github.com/fernandezpablo85/scribe-java/mvn-repo'
        mavenLocal()
    }

    def springSocialVersion = "1.1.0.RC1"
    def jacksonVersion = "2.2.2"

    dependencies {
        compile ("org.springframework.social:spring-social-core:$springSocialVersion","org.springframework.social:spring-social-config:$springSocialVersion") {
            exclude "spring-web"
        }
        compile 'org.apache.httpcomponents:httpclient:4.3.3'
        compile "org.springframework.social:spring-social-twitter:$springSocialVersion"
        compile 'org.apache.commons:commons-collections4:4.0'
        // These are for spring-social-google (found in lib)
        compile "com.fasterxml.jackson.core:jackson-core:$jacksonVersion"
        compile "com.fasterxml.jackson.core:jackson-databind:$jacksonVersion"
        compile "com.fasterxml.jackson.core:jackson-annotations:$jacksonVersion"

        compile 'com.google.oauth-client:google-oauth-client:1.18.0-rc'

        compile 'org.codehaus.groovy.modules.http-builder:http-builder:0.7.1'

        runtime 'mysql:mysql-connector-java:5.1.27',
                "com.google.inject:guice:3.0",
                "com.github.groovy-wslite:groovy-wslite:0.8.0"

        compile "org.jadira.usertype:usertype.jodatime:1.9"
    }

    plugins {
        compile ':scaffolding:2.0.3'
//        compile ":heroku:1.0.1", ":cloud-support:1.0.11", {
//            exclude "database-session"
//        }
        compile ":joda-time:1.5"
        compile ':oauth:2.3',
                ':hibernate:3.6.10.13',
                ":fields:1.3",
                ":spring-security-core:2.0-RC2",
                ":spring-security-oauth:2.0.2",
                {
                    excludes "guice", "spock", "spock-grails-support"
                }
        compile ":spring-security-oauth-twitter:0.1"
        compile ":quartz:1.0.1"
        compile ":quartz-monitor:1.0"
        compile ":build-info:1.2.8"
        compile ":i18n-enums:1.0.8"
        compile ":cache-headers:1.1.7"

        runtime ":resources:1.2.8",
                ":twitter-bootstrap:3.0.2",
                ":font-awesome-resources:3.2.1.3",
//                ":cloud-foundry:1.2.3",
                ":database-migration:1.3.8",
                ":markdown:1.1.1",
                ":modernizr:2.6.2",
                ":webxml:1.4.1"

        runtime ":sendgrid:1.1", {
            excludes "groovy-wslite"
        }
        runtime ":cors:1.1.9"

        build ':tomcat:7.0.52.1'
    }
}

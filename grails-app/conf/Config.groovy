import grails.util.Environment

def catalinaBase = System.properties.getProperty('catalina.base')

grails.config.locations = ["file://${userHome}/.grails/${appName}-config.groovy",
                           "file://${catalinaBase}/conf/${appName}-config.groovy"]

println "Expected config locations: ${grails.config.locations}"

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [html         : ['text/html', 'application/xhtml+xml'],
                     xml          : ['text/xml', 'application/xml'],
                     text         : 'text/plain',
                     js           : 'text/javascript',
                     rss          : 'application/rss+xml',
                     atom         : 'application/atom+xml',
                     css          : 'text/css',
                     csv          : 'text/csv',
                     all          : '*/*',
                     json         : ['application/json', 'text/json'],
                     form         : 'application/x-www-form-urlencoded',
                     multipartForm: 'multipart/form-data'
]

// URL Mapping Cache Max Size, defaults to 5000

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = ['org.gr8conf.mapper']
// whether to disable processing of multi part requests
grails.web.disable.multipart = false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// set per-environment serverURL stem for creating absolute links
environments {
    development {
        grails.logging.jul.usebridge = true
        grails.serverURL = "http://localhost:8081/gr8conf-cfp"
        quartz.autoStartup = false
    }
    production {

        grails.logging.jul.usebridge = false
        grails.serverURL = "http://cfp.gr8conf.org"
    }
}

// log4j configuration
log4j = {
    appenders {
        console name: 'stdout', layout: pattern(conversionPattern: '%c{2} %m%n')
    }

    error 'org.codehaus.groovy.grails.web.servlet',  //  controllers
            'org.codehaus.groovy.grails.web.pages', //  GSP
            'org.codehaus.groovy.grails.web.sitemesh', //  layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping', // URL mapping
            'org.codehaus.groovy.grails.commons', // core / classloading
            'org.codehaus.groovy.grails.plugins', // plugins
            'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
            'org.springframework',
            'org.hibernate',
            'net.sf.ehcache.hibernate'

    warn 'org.mortbay.log'

    debug 'grails.plugin.heroku'
    environments {
        development {
            debug 'grails.app.cacoethes.controllers',
                    'grails.app.services.cacoethes',
                    'grails.app.services.org.gr8conf',
                    'grails.app.controllers.cacoethes',
                    'grails.app.controllers.org.gr8conf',
                    'grails.app.domain.org.gr8conf',
                    'grails.app.jo.org.gr8conf',
                    'grails.app.domain.cacoethes'
        }
        production {
            debug 'grails.app.cacoethes.controllers',
                    'grails.app.controllers.cacoethes',
                    'grails.app.controllers.org.gr8conf',
                    'grails.app.domain.org.gr8conf',
                    'grails.app.domain.cacoethes',
             'grails.app.services.cacoethes.api',
                    'grails.app.services.org.gr8conf.api'

        }
    }
}

oauth {
    providers {
        twitter {
            api = org.scribe.builder.api.TwitterApi.SSL
            successUri = '/oauth/success?provider=twitter'
            failureUri = '/oauth/failure'
            //set these in the external config file
            key = '*******'
            secret = '*******'
            callback = "${grails.serverURL}/oauth/twitter/callback"
        }
        facebook {
            api = org.scribe.builder.api.FacebookApi
            successUri = '/oauth/success?provider=facebook'
            failureUri = '/oauth/failure'
            //set these in the external config file
            key = '*******'
            secret = '*******'
            callback = "${grails.serverURL}/oauth/facebook/callback"
        }
        google {
            api = org.scribe.builder.api.GoogleApi
            successUri = '/oauth/success?provider=google'
            failureUri = '/oauth/failure'
            scope = 'https://www.googleapis.com/auth/userinfo.email'
            //set these in the external config file
            key = '*******'
            secret = '*******'
            callback = "${grails.serverURL}/oauth/google/callback"
        }
    }
    debug = true
}

sendgrid {
    username = '*******'
    password = '********'
}

grails.mail.default.from = "admin@nowhere.net"

// Enable database migrations on startup.
if (Environment.current == Environment.PRODUCTION) {
//    grails.plugin.databasemigration.updateOnStart = true
//    grails.plugin.databasemigration.updateOnStartFileNames = ["changelog.groovy"]
}

// Enable dbconsole so we can execute arbitrary SQL against the database.
grails.dbconsole.enabled = true
grails.resources.adhoc.patterns = ["/images/*", "/css/*", "*.js"]

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'cacoethes.auth.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'cacoethes.auth.UserRole'
grails.plugin.springsecurity.authority.className = 'cacoethes.auth.Role'
grails.plugin.springsecurity.rememberMe.persistent = true
grails.plugin.springsecurity.rememberMe.persistentToken.domainClassName = 'cacoethes.auth.PersistentLogin'
//grails.plugin.springsecurity.openid.domainClass = 'cacoethes.auth.OpenId'
//grails.plugin.springsecurity.openid.registration.roleNames = ['ROLE_USER']
grails.plugin.springsecurity.password.algorithm = 'SHA-256'
grails.plugin.springsecurity.password.encodeHashAsBase64 = false
grails.plugin.springsecurity.password.hash.iterations = 1
grails.plugin.springsecurity.password.bcrypt.logrounds = 10
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
        '/dbconsole/**': ['ROLE_ADMIN'],
        '/rest/**'     : ['IS_AUTHENTICATED_ANONYMOUSLY'],
        '/oauth/**'    : ['permitAll'],
        '/quartz/**'   : ['ROLE_ADMIN'],
        '/buildInfo/**': ['ROLE_ADMIN']
]
grails {
    plugin {
        springsecurity {
            oauth {
                active = true

                printStatusMessages = true

                domainClass = 'cachoethes.auth.OauthId'
                userLookup {
                    oAuthIdsPropertyName = 'oauthIds'
                }
            }
        }
    }
}

// Load configuration from JSON provided in the optional GRAILS_APP_CONFIG env variable.
ConfigLoader.addEntries(loadJson(fetchJson()), this)

def fetchJson() { return System.getenv("GRAILS_APP_CONFIG") }

def loadJson(content) { return content ? grails.converters.JSON.parse(content) : [:] }
//Default date formats
jodatime {
    format.org.joda.time.DateTime = "yyyy-MM-dd HH:mm:ss"
    format.org.joda.time.LocalDate = "yyyy-MM-dd"
    format.org.joda.time.LocalTime = "HH:mm:ss"
}

grails.plugins.twitterbootstrap.fixtaglib = true

// Uncomment and edit the following lines to start using Grails encoding & escaping improvements

// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside null
                scriptlet = 'none' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        filteringCodecForContentType {
            //'text/html' = 'html'
        }
    }
}

// Added by the Spring Security OAuth plugin:
grails.plugin.springsecurity.oauth.domainClass = 'cacoethes.auth.OauthId'

twitter {
    consumerKey = 'vGGOh3LDIl6H4G9fWoqJcDF8O'
    consumerSecret = '047u6no0fg4G2CvutQxmXRpHYv7hX1EDH5tOXDBF44zYlY82mt'
}
google {
    consumerKey = '187037446391-ou4lqq9r32tb020q2fpppjbffgs2n5om.apps.googleusercontent.com'
    consumerSecret = '_mmVadvvdYFsP9HNkRmy_O83'
}

oauth {
    consumerKey = '1WAuaxiRscO6Q5IP9Yzt980z2dI'
    consumerSecret = '4yCGnl2XDWVXeXEf-p9BNCA5Te4'
}

grails.resources.adhoc.excludes = ['**/WEB-INF/**', '**/META-INF/**']

//cors.headers = ['Access-Control-Allow-Origin': '*']
//grails.gorm.default.mapping = {
//    "user-type" type: org.jadira.usertype.dateandtime.joda.PersistentDateTime, class: org.joda.time.DateTime
//    "user-type" type: org.jadira.usertype.dateandtime.joda.PersistentLocalDate, class: org.joda.time.LocalDate
//     … define as many other user type mappings as you need
//}

import cacoethes.Conference
import cacoethes.Duration
import cacoethes.Submission
import cacoethes.Tag
import cacoethes.auth.User
import grails.rest.render.json.JsonRenderer
//import org.springframework.social.google.connect.GoogleConnectionFactory
import org.springframework.social.twitter.connect.TwitterConnectionFactory

//import org.springframework.social.connect.support.ConnectionFactoryRegistry
//import org.springframework.social.google.connect.GoogleConnectionFactory
import org.springframework.web.servlet.i18n.SessionLocaleResolver

// Place your Spring DSL code here
beans = {
    localeResolver(SessionLocaleResolver) {
        defaultLocale = new Locale("us", "EN")
        java.util.Locale.setDefault(defaultLocale)
    }
    talkRenderer(JsonRenderer, Submission) {
        includes=['id','title','summary','durations','submittedTo','tags','speakers']
        excludes = ['class','user']
    }

    tagRenderer(JsonRenderer, Tag) {
        excludes = ['class']
    }
    durationRenderer(JsonRenderer, Duration) {
        includes = ['minutes']
        excludes = ['class']
    }
    conferenceRenderer(JsonRenderer, Conference) {
        includes = ['name','location']
        excludes = ['class','cfpDeadline']
    }

    userRenderer(JsonRenderer, User) {
        includes = ['username', 'profile']
        excludes = ['class','cfpDeadline']
    }

    twitterConnectionFactory(TwitterConnectionFactory, grailsApplication.config.twitter.consumerKey, grailsApplication.config.twitter.consumerSecret)
//    googleConnectionFactory(GoogleConnectionFactory, grailsApplication.config.google.consumerKey, grailsApplication.config.google.consumerSecret)
}


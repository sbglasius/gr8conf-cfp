import cacoethes.Conference
import cacoethes.Duration
import cacoethes.Profile
import cacoethes.Submission
import cacoethes.Tag
import cacoethes.auth.Role
import cacoethes.auth.User
import cacoethes.auth.UserRole
import grails.util.Environment
import org.gr8conf.Slot
import org.gr8conf.Track
import org.joda.time.DateTimeZone
import org.joda.time.LocalDate
import org.joda.time.LocalTime

class BootStrap {
    def dataSource
    def init = { servletContext ->
        def roleAdmin = Role.findOrSaveWhere(authority: "ROLE_ADMIN")
        def roleUser = Role.findOrSaveWhere(authority: "ROLE_USER")

        createConference(
                'GR8Conf Europe 2014',
                'IT-University, Copenhagen',
                new LocalDate(2014, 6, 2),
                new LocalDate(2014, 6, 4),
                new LocalDate(2014, 1, 1),
                'Europe/Copenhagen'
        )
        createConference(
                'GR8Conf US 2014',
                'University of St. Thomas, Minneapolis, MN',
                new LocalDate(2014, 7, 28),
                new LocalDate(2014, 7, 29),
                new LocalDate(2014, 2, 1),
                'US/Central'
        )

        Duration.findOrSaveWhere(name: "Quick intro", minutes: 15)
        def duration = Duration.findOrSaveWhere(name: "Regular talk", minutes: 50)
        Duration.findOrSaveWhere(name: "Keynote", minutes: 80)
        Duration.findOrSaveWhere(name: "Long talk", minutes: 80)
        Duration.findOrSaveWhere(name: "Half day workshop", minutes: 3 * 60)
        Duration.findOrSaveWhere(name: "Full day workshop", minutes: 5 * 60)
        ['groovy', 'grails', 'gradle', 'griffon'].each {
            Tag.findOrSaveWhere(name: it)
        }
        if (Environment.current == Environment.DEVELOPMENT && !User.findByUsername("pledbrook")) {
            def users = []
            users << new User(username: "pledbrook", password: "password", enabled: true).save(failOnError: true)
            users << new User(username: "dilbert", password: "password", enabled: true).save(failOnError: true)

            Tag tag = new Tag(name: 'bowling').save(failOnError: true)
            (1..20).each {
                users << new User(username: "thedude" + it, password: "password", enabled: true).save(failOnError: true)
                User user = User.findByUsername('thedude' + it)
                new Profile(
                        user: user,
                        employer: 'Is today a weekday?',
                        name: 'Jeff Lebowski' + it,
                        bio: "I'm the dude, man",
                        email: "thedude${it}@Lebowski.com"
                ).save(failOnError: true)

                Submission submission = new Submission(
                        title: 'Bowling 10' + it,
                        user: user,
                        summary: 'That guy can really roll, man',
                        tags: [tag],
                        durations: [duration]
                )
                submission.submittedTo = Conference.list()
                submission.acceptedFor = Conference.list()

                submission.save(failOnError: true)
            }

            (9..15).each {
                Slot slot = new Slot()

            }


            for (u in users) {
                UserRole.create u, roleUser
            }
        }

        if (!User.findByUsername("admin")) {
            def adminPassword = System.getProperty("admin.password") ?: "changeit"
            def admin = new User(username: "admin", password: adminPassword, enabled: true).save(failOnError: true)

            UserRole.create admin, roleUser
            UserRole.create admin, roleAdmin, true
        }
    }


    private createConference(String name, String location, LocalDate start, LocalDate end, LocalDate cfpDeadline, String timeZoneCode) {
        if (!Conference.findByName(name)) {
            DateTimeZone timeZone = DateTimeZone.forID(timeZoneCode)
            Conference conf = new Conference(name: name, location: location, start: start, end: end, cfpDeadline: cfpDeadline, timeZone: timeZone).save(failOnError: true)
            new Track(
                    name: 'keynotes',
                    conference: conf,
                    allColumns: true,
                    color: '#d4550f'
            ).save()
            new Track(
                    name: 'activities',
                    conference: conf,
                    allColumns: true,
                    color: '#FF0000'
            ).save()
            new Track(
                    name: 'room 1',
                    conference: conf,
                    allColumns: false,
                    color: '#CCCCCC'
            ).save()
            new Track(
                    name: 'room 2',
                    conference: conf,
                    allColumns: false,
                    color: '#00FF00'
            ).save()
            LocalDate date = conf.start
            while (date <= conf.end) {
                (9..15).each {
                    new Slot(conference: conf, date: date, startTime: new LocalTime(it, 0, 0), endTime: new LocalTime(it + 1, 0, 0)).save()
                }
                date = date.plusDays(1)
            }
        }

    }

    def destroy = {
    }
}

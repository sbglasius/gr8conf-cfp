package cacoethes.auth

import grails.plugin.springsecurity.userdetails.GrailsUser
import org.springframework.security.core.context.SecurityContextHolder

import static cacoethes.auth.SpringSecurityOAuthController.SPRING_SECURITY_OAUTH_TOKEN

class SecurityFilters {
    def springSecurityService

    def filters = {
        all(controller:'*', action:'*') {
            before = {
                def grailsUser = SecurityContextHolder.context.authentication?.principal
                def oAuthToken = session[SPRING_SECURITY_OAUTH_TOKEN]

                if (grailsUser instanceof GrailsUser && oAuthToken) {
                    User.withTransaction { status ->
                        def user = User.get(grailsUser.id)
                        if (user) {
                            user.addToOauthIds(provider: oAuthToken.providerName, accessToken: oAuthToken.socialId, user: user)
                            if (!user.save()) {
                                status.setRollbackOnly()
                            }
                        }

                    }

                    session.removeAttribute SPRING_SECURITY_OAUTH_TOKEN
                }
            }
            after = { Map model ->

                if (model && !model.currentUser) {
                    model.currentUser = springSecurityService.currentUser
                }
            }
            afterView = { Exception e ->

            }
        }
    }
}

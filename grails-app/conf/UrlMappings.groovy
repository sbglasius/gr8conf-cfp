class UrlMappings {

    static mappings = {
    //    "/rest/talks"(resources: 'talk')
        "/login/auth" {
            controller = "openId"
            action = "auth"
        }

        "/login/openIdCreateAccount" {
            controller = "openId"
            action = "createAccount"
        }

        "/oauth/success"(controller: "springSecurityOAuth", action: "onSuccess")
        "/oauth/failure"(controller: "springSecurityOAuth", action: "onFailure")

        "/$controller/$action?/$id?"{
            constraints {
                // apply constraints here
            }
        }


        "/admin/emails/accepted/$forYear?"(controller: "profile", action: "acceptedEmails") {
            constraints {
                forYear matches: /\d{4}/
            }
        }

        "/admin/emails/submitted/$forYear?"(controller: "profile", action: "submittedEmails") {
            constraints {
                forYear matches: /\d{4}/
            }
        }

        "/api/talks/$conferenceId"(controller: 'talk', action: 'index')
        "/api/speakers/$conferenceId"(controller: 'speaker', action: 'index')
        "/api/agenda/$conferenceId"(controller: "agendaApi", action: 'index')
        "/api/user/code"(controller: 'clientUser', action: 'createCode')
        "/api/user/get"(controller: 'clientUser', action: 'getUser')
        "/api/favorites/get"(controller: 'data', action: 'loadFavorites')
        "/api/favorites/put"(controller: 'data', action: 'saveFavorite')
        "/api/ratings/get"(controller: 'data', action: 'loadRatings')
        "/api/ratings/put"(controller: 'data', action: 'saveRating')
        "/api/confrating/get"(controller: 'data', action: 'loadConferenceRating')
        "/api/confrating/put"(controller: 'data', action: 'saveConferenceRating')
        "/api2/tweets/r"(controller: 'twitterSearch', action: 'trigger')
        "/api2/tweets"(controller: 'twitterSearch', action: 'latest')
        "/api2/conferences/grouped"(controller: 'data', action: 'conferencesGrouped')
        "/api2/conferences"(controller: 'data', action: 'conferences')
        "/api2/$action/$id/$featured?"(controller: 'data')
        "/"(controller: "submission", action: "index")
        "500"(view: "/error")
    }
}

modules = {
    login {
        resource url: "/css/auth.css"
        resource url: "/css/social-buttons.css"
    }

    gr8conf {
        resource url: '/css/gr8conf.css'
    }

    jquery {
        resource url: '/js/lib/jquery-2.0.3.min.js', disposition: 'head'
    }

    countdown {
        resource url: "http://cdnjs.cloudflare.com/ajax/libs/jquery-countdown/1.6.1/jquery.countdown.min.js",
                 exclude: "minify"
    }


    'bootstrap-tokenfield' {
        resource url: '/bootstrap-tokenfield/tokenfield-typeahead.css'
        resource url: '/bootstrap-tokenfield/bootstrap-tokenfield.css'
        resource url: '/bootstrap-tokenfield/bootstrap-tokenfield.js'
    }

    'typeahead' {
        resource url: '/typeahead.js/typeahead.js'
    }

    'bootstrap-fileupload' {
        resource url: 'bootstrap-fileupload/bootstrap-fileupload.min.css'
        resource url: 'bootstrap-fileupload/bootstrap-fileupload.min.js'
    }

    'jcrop' {
        dependsOn 'jquery'
        resource url: 'jcrop/css/jquery.Jcrop.min.css'
        resource url: 'jcrop/js/jquery.Jcrop.min.js'
        resource url: 'jcrop/js/jquery.color.js'
    }

    'jqplot' {
        dependsOn 'jquery'

        resource url: 'jqplot-1.0.8/excanvas.min.js', wrapper: { s -> "<!--[if lt IE 9]>$s<![endif]-->" }
        resource url: 'jqplot-1.0.8/jquery.jqplot.min.css'
        resource url: 'jqplot-1.0.8/jquery.jqplot.min.js'
    }

    'jqplot-piechart' {
        dependsOn 'jqplot'

        resource url: 'jqplot-1.0.8/plugins/jqplot.pieRenderer.js'

    }

    'bootstrap-select' {
        dependsOn 'bootstrap'

        resource url: 'bootstrap-select/css/bootstrap-select.css'
        resource url: 'bootstrap-select/js/bootstrap-select.js'
    }
}

<%@ page import="cacoethes.auth.OAuthCreateAccountCommand; cacoethes.auth.OAuthLinkAccountCommand" %>
<head>
    <meta name='layout' content='main'/>
    <title>Create or Link Account</title>
</head>

<body>

<div>
    <g:if test='${flash.error}'>
        <div class="alert alert-danger">${flash.error}</div>
    </g:if>

    <div class="alert alert-info"><g:message code="springSecurity.oauth.registration.link.not.exists"
                                             default="No user was found with this account."
                                             args="[session.springSecurityOAuthToken.providerName]"/></div>


    <ul class="nav nav-tabs" id="navi">
        <li><a href="#new-account" data-toggle="tab">
            <g:message code="springSecurity.oauth.registration.create.legend" default="Create a new account"/>
        </a></li>
        <li><a href="#link-account" data-toggle="tab">
            <g:message code="springSecurity.oauth.registration.login.legend" default="Link to an existing account"/>
        </a></li>
        <li><a href="#link-openid" data-toggle="tab">
            <g:message code="springSecurity.oauth.link.openid.legend" default="Link to an existing OpenID account"/>
        </a></li>
    </ul>


    <div class="tab-content">
        <div class="tab-pane active " id="new-account">
            <g:form action="createAccount" method="post" autocomplete="off" class="form-horizontal well well-sm">
                <div class="row">
                    <div class="col-lg-push-2 col-lg-10">
                        <p>We will create an account in our system, and link it to your ${session.springSecurityOAuthToken.providerName} account.</p>
                    </div>
                </div>
                <f:with bean="${createAccountCommand ?: new OAuthCreateAccountCommand()}" span="5">
                    <f:field property="username" span="5"/>
                    <f:field property="password1" span="5"/>
                    <f:field property="password2" span="5"/>
                </f:with>
                <div class="form-group">
                    <div class="col-lg-push-2 col-lg-10">
                        <g:submitButton class="btn btn-default"
                                        name="create" value="${message(code: 'springSecurity.oauth.registration.create.button', default: 'Create')}"/>
                    </div>
                </div>
            </g:form>
        </div>

        <div class="tab-pane" id="link-account">
            <g:form action="linkAccount" method="post" autocomplete="off" class="form-horizontal well well-sm">
                <div class="row">
                    <div class="col-lg-push-2 col-lg-10">
                        <p>If you already have an account with us, we will link that account to your ${session.springSecurityOAuthToken.providerName} account.</p>
                    </div>
                </div>               <f:with bean="${linkAccountCommand ?: new OAuthLinkAccountCommand()}">
                <f:field property="username" span="5"/>
                <f:field property="password" span="5"/>
            </f:with>
                <div class="form-group">
                    <div class="col-lg-push-2 col-lg-10">

                        <g:submitButton class="btn btn-default"
                                        name="link" value="${message(code: 'springSecurity.oauth.registration.login.button', default: 'Login')}"/>
                    </div>
                </div>
            </g:form>
        </div>

        <div class="tab-pane" id="link-openid">
            <form action='${openIdPostUrl}' method='POST' autocomplete='off' name='openIdLoginForm'
                  id="openIdLoginForm" class="form-horizontal well well-sm">
                <f:field bean="${linkAccountCommand ?: new OAuthLinkAccountCommand()}" property="openid_identifier"/>
                <div class="form-group">
                    <div class="col-lg-push-2 col-lg-10">
                        <div class="row">
                            <div class="col-lg-1">
                            <g:submitButton class="btn btn-default"
                                            name="openid" value="${message(code: 'springSecurity.oauth.registration.login.button', default: 'Login')}"/>
                            </div>
                            <div class="col-lg-4">or sign in with:
                                <a href="#" onclick="loginWithGoogle();return false;" class="btn btn-info">Google</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <g:link controller="login" action="auth"><g:message code="springSecurity.oauth.registration.back"
                                                        default="Back to login page"/></g:link>
</div>
<r:script>

    function loginWithGoogle() {
        $('#openIdLoginForm').find('input[type="text"]').val("https://www.google.com/accounts/o8/id");
        $('#openIdLoginForm').find('input[type="submit"]').attr('disabled', 'disabled');
        $('#openIdLoginForm').submit();
    }
    <g:if test="${linkAccountCommand && !linkAccountCommand.openid_identifier}">
        $('#navi a[href="#link-account"]').tab('show')
    </g:if>
    <g:elseif test="${linkAccountCommand && linkAccountCommand.openid_identifier}">
        $('#navi a[href="#link-openid"]').tab('show')
    </g:elseif>
    <g:else>
        $('#navi a[href="#new-account"]').tab('show')
    </g:else>
</r:script>

</body>

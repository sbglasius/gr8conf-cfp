<g:set var="textClasses" value="${classes ?: [] + 'form-control'}"/>
<g:passwordField name="${property}" value="${value}" class="${textClasses.join(' ')}"/>

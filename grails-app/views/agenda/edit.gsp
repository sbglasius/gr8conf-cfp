<!doctype html>
<html>
<head>
    <meta name="layout" content="agenda">
    <title>Edit Agenda</title>
</head>

<body>
<h3>Edit Agenda for ${conference.name} ${activeTab}</h3>


<div>
    <ul class="nav nav-tabs" role="tablist">
        <g:each in="${groupedSlots}" var="group" status="tab">
            <g:set var="slots" value="${group.value}"/>

            <li role="presentation" class="${tab == activeTab ? 'active' : ''}"><a href="#tab-${tab}" aria-controls="tab-${tab}" role="tab" data-toggle="tab">${group.key.toString('MMMM dd')}</a></li>
        </g:each>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <g:each in="${groupedSlots}" var="group" status="tab">
            <g:set var="slots" value="${group.value}"/>
            <div role="tabpanel" class="tab-pane ${tab == activeTab ? 'active' : ''}" id="tab-${tab}">
                <g:form method="post" class="form-horizontal">
                    <g:hiddenField name="activeTab" value="${tab}"/>
                    <g:hiddenField name="id" value="${conference.id}"/>

                    <table>
                        <tr>
                            <th>${group.key.toString('MM-dd')}</th>

                            <g:each in="${tracks}" var="track">
                                <th>
                                    ${track.name}
                                </th>
                            </g:each>
                        </tr>
                        <g:each in="${slots}" var="slot" status="slotNumber">
                            <tr>
                                <td style="padding-left: 30px">
                                    <strong>
                                        ${slot.startTime.toString('hh:mm')} - ${slot.endTime.toString('hh:mm')}
                                    </strong>
                                </td>

                                <g:each in="${tracks}" var="track" status="trackNumber">
                                    <g:set var="scheduledItem" value="${scheduledItems.find { it.track.id == track.id && it.slot.id == slot.id }}"/>
                                    <g:hiddenField name="track[${trackNumber}].slot[${slotNumber}].scheduledItem.id" value="${scheduledItem?.id}"/>
                                    <g:hiddenField name="track[${trackNumber}].slot[${slotNumber}].conference.id" value="${conference?.id}"/>
                                    <g:hiddenField name="track[${trackNumber}].slot[${slotNumber}].slot.id" value="${slot?.id}"/>
                                    <g:hiddenField name="track[${trackNumber}].slot[${slotNumber}].track.id" value="${track?.id}"/>
                                    <td>
                                        <g:if test="${track.breaks}">
                                            <div class="input-group">
                                                <div class="input-group-btn" >
                                                    <g:hiddenField class="track_slot_icon" id="tab${tab}_track${trackNumber}_slot${slotNumber}_icon" name="track[${trackNumber}].slot[${slotNumber}].icon" value="${scheduledItem?.icon}"/>
                                                    <button type="button" id="tab${tab}_track${trackNumber}_slot${slotNumber}_button" class="btn btn-default dropdown-toggle break-icon" data-toggle="dropdown"><i
                                                            class="${scheduledItem?.icon ?: 'icon-sort-down'}"></i></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a class="icon-selector" href="#" data-select="tab${tab}_track${trackNumber}_slot${slotNumber}" data-icon="icon-sort-down"><i class="icon-sort-down"></i> (none)</a></li>
                                                        <li><a class="icon-selector" href="#" data-select="tab${tab}_track${trackNumber}_slot${slotNumber}" data-icon="icon-ticket"><i class="icon-ticket"></i> Registration</a></li>
                                                        <li><a class="icon-selector" href="#" data-select="tab${tab}_track${trackNumber}_slot${slotNumber}" data-icon="icon-food"><i class="icon-food"></i> Lunch break</a></li>
                                                        <li><a class="icon-selector" href="#" data-select="tab${tab}_track${trackNumber}_slot${slotNumber}" data-icon="icon-coffee"><i class="icon-coffee"></i> Coffee break</a></li>
                                                        <li><a class="icon-selector" href="#" data-select="tab${tab}_track${trackNumber}_slot${slotNumber}" data-icon="icon-pause"><i class="icon-pause"></i> Short break</a></li>
                                                    </ul>
                                                </div><!-- /btn-group -->
                                            <g:textField class="form-control" id="tab${tab}_track${trackNumber}_slot${slotNumber}_name" name="track[${trackNumber}].slot[${slotNumber}].name" value="${scheduledItem?.name}"/>
                                            </div>
                                        </g:if>
                                        <g:else>
                                            <g:select
                                                    class="form-control agenda-item"
                                                    name="track[${trackNumber}].slot[${slotNumber}].submission.id"
                                                    from="${acceptedTalks}"
                                                    value="${scheduledItem?.submission?.id}"
                                                    optionValue="${{ talk -> "${talk.title} (${talk.speakers.name.join(' ,')})" }}"
                                                    optionKey="id"
                                                    noSelection="${['': 'None']}"/>
                                        </g:else>
                                    </td>
                                </g:each>
                            </tr>

                        </g:each>
                    </table>

                    <p>Make changes to only one page at the time and then save!</p>
                    <g:actionSubmit class="save btn btn-default" action="save"
                                    value="${message(code: 'default.button.save.label', default: 'Save')}"/>
                </g:form>

            </div>
        </g:each>
    </div>

    <r:script>

        $('body').on('click', '.icon-selector', function (event) {
            var iconsToText = {
               "icon-ticket":"Registration",
                "icon-food":"Lunch break",
                "icon-coffee":"Coffee break",
                "icon-pause":"Short break",
                "icon-sort-down":""
            };
            var element = $(this);
            var select = element.data('select');
            var iconName = element.data('icon');
            var button = $('#' + select + "_button i");
            var name = $('#' + select + "_name");
            var iconInput = $('#' + select + "_icon");
            button.removeAttr('class');
            button.addClass(iconName);
            var text =iconsToText[iconName];
            iconInput.val(iconName);
            name.val(text);
            $('body').trigger('update-icons',[iconInput]);

        }).on('update-icons', function(event,element) {
            console.debug("Update: ",element, element.val());
            var tr = element.closest('tr');
            if(!element.val() || element.val() === '' || element.val() === 'icon-sort-down') {
                tr.css('background-color','');
            } else {
                tr.css('background-color', '#FF0');

            }
        });
        $.each($('.track_slot_icon'), function(i,e) { $('body').trigger('update-icons',[$(e)])});

    </r:script>
</body>
</html>

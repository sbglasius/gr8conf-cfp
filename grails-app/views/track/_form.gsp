<%@ page import="org.gr8conf.Track" %>
<r:require modules="bootstrap-dropdown, bootstrap-tokenfield, typeahead"/>
<f:with bean="${trackInstance}">
    <f:field property="name"/>
    <f:field property="room"/>
    <f:field property="conference"/>
    <f:field property="roomOrder"/>
    <f:field property="allColumns"/>
    <f:field property="breaks"/>
    <div class="form-group ">
        <label for="color" class="col-sm-2 control-label">Color</label>
        <div class="col-sm-10">
            <input type="color" name="color" id="color" value="${trackInstance.color}"/>
        </div>
    </div>
</f:with>


<%@ page import="org.gr8conf.Track" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'track.label', default: 'Track')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<div id="show-track" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <f:display bean="${trackInstance}" property="allColumns">

        <g:formatBoolean boolean="${trackInstance?.allColumns}"/>

    </f:display>


    <f:display bean="${trackInstance}" property="conference">

        <g:link controller="conference" action="show" id="${trackInstance?.conference?.id}">${trackInstance?.conference?.name}</g:link>

    </f:display>



    <f:display bean="${trackInstance}" property="name">

        <g:fieldValue bean="${trackInstance}" field="name"/>

    </f:display>


    <f:display bean="${trackInstance}" property="room">

        <g:fieldValue bean="${trackInstance}" field="room"/>

    </f:display>
    <f:display bean="${trackInstance}" property="breaks">

        <g:formatBoolean boolean="${trackInstance?.breaks}"/>

    </f:display>



    <g:form>
        <class class="row">
            <div class="col-lg-push-2 col-lg-10">
                <g:hiddenField name="id" value="${trackInstance?.id}"/>
                <g:link class="edit btn btn-default" action="edit" id="${trackInstance?.id}"><g:message code="default.button.edit.label" default="Edit"/></g:link>
                <g:actionSubmit class="delete btn btn-warning" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
            </div>
        </class>
    </g:form>
</div>
</body>
</html>

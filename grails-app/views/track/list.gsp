
<%@ page import="org.gr8conf.Track" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'track.label', default: 'Track')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="list-track" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-bordered">
				<thead>
					<tr>
					
					
						<th><g:message code="track.conference.label" default="Conference" /></th>
					
						<g:sortableColumn property="name" title="${message(code: 'track.name.label', default: 'Name')}" />
						<g:sortableColumn property="room" title="${message(code: 'track.room.label', default: 'Roo')}" />
						<g:sortableColumn property="allColumns" title="${message(code: 'track.allColumns.label', default: 'All Columns')}" />
						<g:sortableColumn property="breaks" title="${message(code: 'track.breaks.label', default: 'Breaks')}" />
                        <th>Color</th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${trackInstanceList}" status="i" var="trackInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
					
						<td>${trackInstance.conference.name}</td>
						<td><g:link action="show" id="${trackInstance.id}">${fieldValue(bean: trackInstance, field: "name")}</g:link></td>
						<td>${fieldValue(bean: trackInstance, field: "room")}</td>
						<td>${fieldValue(bean: trackInstance, field: "allColumns")}</td>
						<td>${fieldValue(bean: trackInstance, field: "breaks")}</td>
						<td style="background-color:${fieldValue(bean: trackInstance, field: "color")}"></td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${trackInstanceTotal}" />
			</div>
		</div>
	</body>
</html>

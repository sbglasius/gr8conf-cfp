<head>
    <meta name='layout' content='login'/>
    <title><g:message code="springSecurity.login.title"/></title>
    <r:require modules="login"/>
</head>

<body>
    <div class="header-logo">
        <r:img uri="/images/header.png" class="header-img" alt="GR8Conf + GR8Day Call For Papers"/>
    </div>
<div>
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="text-center">Upcoming conferences</h3>
        </div>

        <div class="panel-body">
            <g:each in="${upcoming.sort { it.start }}" var="conference" status="i">
                <g:if test="${i}">
                    <hr/>
                </g:if>
                <h2 class="text-center">${conference.name}</h2>
                <g:if test="${conference.start < conference.end}">
                    <h3 class="text-primary text-center"><joda:format value="${conference.start}" pattern="MMMM d" locale="${Locale.US}"/> -
                        <g:if test="${conference.start.monthOfYear == conference.end.monthOfYear}">
                            <joda:format value="${conference.end}" pattern="d, yyyy"/>
                        </g:if>
                    <g:else>
                        <joda:format value="${conference.end}" pattern="MMMM d, yyyy"/>
                    </g:else>
                    </h3>
                </g:if>
                <g:else>

                    <h3 class="text-primary text-center"><joda:format value="${conference.start}" pattern="MMMM d" locale="${Locale.US}"/></h3>
                </g:else>
                <h3 class="text-center text-muted">${conference.location}</h3>
                <h5 class="text-center text-muted">Deadline: <joda:format value="${conference.cfpDeadline}" patern="yyyy-MM-dd"/> (<g:countdown id="conf-countdown_${conference.id}" date="${conference.cfpDateTime}"
                                                                                                                                                timeZone="${conference.timeZone}"/>)</h5>
            </g:each>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 text-center">
            <oauth:connect provider="twitter" id="twitter-connect-link" class="btn btn-lg btn-twitter"><i
                    class="icon-twitter"></i> | Login with Twitter</oauth:connect>
        </div>

        %{--
                <div class="col-lg-6 text-center">
                    <oauth:connect provider="google" id="google-connect-link" class="btn btn-lg btn-google-plus"><i
                            class="icon-google-plus"></i> | Login with Google</oauth:connect>
                </div>
        --}%
    </div>


    <div class="row">
        <div class="col-lg-12 text-center ">
            <a href="#" data-toggle="collapse" data-target="#admin" id="adminSection"><small><i class="icon-lock text-muted"></i></small>
            </a>
        </div>
    </div>

    <div class="row">
        <g:if test='${flash.message}'>
            <div class='alert alert-warning'>${flash.message}</div>
        </g:if>
    </div>

    <div class="collapse" id="admin">
        <div class="row">
            <div class="col-lg-12 text-center ">
                -- or log in as the administrator --
            </div>
        </div>

        <form action='${postUrl}' method='POST' id='loginForm' class='form-horizontal' autocomplete='off'>
            <g:if env="production">
                <input type='hidden' class='text_' name='j_username' value='admin'/>
            </g:if>
            <g:if env="development">
                <div class="form-group">
                    <label for='username' class="control-label"><g:message code="springSecurity.login.username.label"/></label>
                    <input type='text' class='form-control' name='j_username' value='admin' id="username"/>
                </div>
            </g:if>
            <div class="form-group">

                <label for='password' class="control-label"><g:message code="springSecurity.login.password.label"/></label>
                <input type='password' class='form-control' name='j_password' id='password'/>
            </div>

            <div class="form-group">&nbsp;</div>

            <div class="form-group text-center ">
                <input type='submit' class="btn btn-danger" value='${message(code: "springSecurity.login.button")}'/>
            </div>
        </form>
    </div>
</div>


<r:script>
    $('#admin').on('shown.bs.collapse', function () {
        $("input[name='j_password']").focus();
    })
</r:script>
</body>

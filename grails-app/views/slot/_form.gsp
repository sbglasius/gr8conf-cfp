<%@ page import="org.gr8conf.Slot" %>

<%@ page import="org.gr8conf.Track" %>
<r:require modules="bootstrap-dropdown, bootstrap-tokenfield, typeahead"/>
<f:with bean="${slotInstance}">
    <f:field property="conference"/>
    <f:field property="date"/>
    <f:field property="startTime"/>
    <f:field property="endTime"/>
</f:with>

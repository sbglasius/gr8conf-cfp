
<%@ page import="org.gr8conf.Slot" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'slot.label', default: 'Slot')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-slot" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-slot" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="slot.conference.label" default="Conference" /></th>
					
						<g:sortableColumn property="date" title="${message(code: 'slot.date.label', default: 'Date')}" />
					
						<g:sortableColumn property="endTime" title="${message(code: 'slot.endTime.label', default: 'End Time')}" />
					
						<g:sortableColumn property="startTime" title="${message(code: 'slot.startTime.label', default: 'Start Time')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${slotInstanceList}" status="i" var="slotInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${slotInstance.id}">${fieldValue(bean: slotInstance, field: "conference")}</g:link></td>
					
						<td>${fieldValue(bean: slotInstance, field: "date")}</td>
					
						<td>${fieldValue(bean: slotInstance, field: "endTime")}</td>
					
						<td>${fieldValue(bean: slotInstance, field: "startTime")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${slotInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>

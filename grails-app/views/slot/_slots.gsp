<div class="form-group">
    <label class="col-sm-2 control-label">Slots</label>

    <div class="col-sm-3">

        <table class="table table-bordered table-condensed table-input">
            <thead>
            <tr>
                <th>Start time</th>
                <th>End time</th>
            </tr>
            </thead>
            <tbody id="slots">

            <g:each in="${slots}" var="slot" status="i">
                <tr>
                    <td>
                        <g:if test="${useId}">
                            <g:hiddenField name="slots[${i}].id" value="${slot.id}"/>
                        </g:if>
                        <g:textField name="slots[${i}].startTime" value="${slot.startTime.format('HH:mm')}" class="form-control text-right"/></td>
                    <td><g:textField name="slots[${i}].endTime" value="${slot.endTime.format('HH:mm')}" class="form-control"/></td>

                </tr>
            </g:each>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2" class="text-right">
                    <button id="addRow" data-id="${slots.size()}" class="btn btn-success"><i class="icon icon-plus-sign"></i></button>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
<r:script>
    $('#addRow').on('click', function (event) {
        event.preventDefault();

        var id = $(this).data('id');
        var row = $('<tr></tr>');
        var cell1 = $('<td></td>').appendTo(row);
        var cell2 = $('<td></td>').appendTo(row);
        var input1 = $('<input name="slots['+id+'].startTime"/>').addClass('form-control text-right').appendTo(cell1);
        $('<input name="slots['+id+'].endTime"/>').addClass('form-control').appendTo(cell2);
        row.appendTo('#slots')
        $(this).data('id',id+1);
        setTimeout(function() {
            input1.focus()
        },100);
    })
</r:script>
<%@ page import="org.gr8conf.Slot" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'slot.label', default: 'Slot')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div id="list-slot" class="content scaffold-list" role="main">
    <h1>Copy Slots</h1>
    <g:if test="${flash.message}">
        <div class="alert alert-warning" role="status">${flash.message}</div>
    </g:if>
    <g:form action="copySlots" method="post" class="form-horizontal">
        <f:with bean="slotsCopyCommand">
            <f:field property="conference"/>
            <f:field property="date"/>
        </f:with>
        <tmpl:slots slots="${slotsCopyCommand.slots}"/>
        <div class="text-right">
            <g:submitButton value="Create" name="submit" class="btn btn-success"/>
        </div>

    </g:form>
</div>
</body>
</html>

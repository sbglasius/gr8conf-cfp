<%@ page import="org.gr8conf.Slot" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'slot.label', default: 'Slot')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div id="list-slot" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="alert alert-warning" role="status">${flash.message}</div>
    </g:if>
    <div>
        <div class="row">
            <div class="col-sm-3">

                <ul class="nav nav-pills nav-stacked" role="tablist">
                    <g:each in="${slotsByConference}" status="i" var="conference">
                        <g:set var="conferenceActive" value="${params.conference == conference.conference.id.toString()}"/>
                        <li role="presentation" class="${conferenceActive ? 'active' : ''}">
                            <a href="#conference_${conference.conference.id}" aria-controls="conference_${conference.conference.id}" role="tab"
                               data-toggle="tab">
                                ${conference.conference.name}
                            </a>
                        </li>
                    </g:each>
                </ul>
            </div>

            <div class="col-sm-9">

                <!-- Tab panes -->
                <div class="tab-content">
                    <g:each in="${slotsByConference}" status="i" var="conference">
                        <g:set var="conferenceActive" value="${params.conference == conference.conference.id.toString()}"/>
                        <div role="tabpanel" class="tab-pane ${conferenceActive ? 'active' : ''}" id="conference_${conference.conference.id}">
                            <div class="row">
                                <div class="col-sm-6">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <g:each in="${conference.days}" status="j" var="day">
                                            <g:activeTab collection="${conference.days}" match="${{ params.date == it.date.format('yyyy-MM-dd') }}" var="active"/>

                                            <li role="presentation" class="${active == j? 'active' : ''}">
                                                <a href="#conference_${conference.conference.id}_${day.date.format('yyyy-MM-dd')}" aria-controls="conference_${i}_${j}" role="tab"
                                                   data-toggle="tab">${day.date.format('d. MMMM - yyyy')}</a>
                                            </li>

                                        </g:each>
                                    </ul>

                                    <div class="tab-content">

                                        <g:each in="${conference.days}" status="j" var="day">
                                            <g:activeTab collection="${conference.days}" match="${{ params.date == it.date.format('yyyy-MM-dd') }}" var="active"/>
                                            <div role="tabpanel" class="tab-pane ${active == j ? 'active' : ''}" id="conference_${conference.conference.id}_${day.date.format('yyyy-MM-dd')}">

                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>Start time</th>
                                                        <th>End time</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <g:each in="${day.slots}" var="slot">

                                                        <tr>
                                                            <td class="text-right">${slot.startTime.format('HH:mm')}</td>
                                                            <td>${slot.endTime.format('HH:mm')}</td>
                                                            <td>
                                                                <g:form action="delete" id="${slot.id}">
                                                                    <g:link action="edit" id="${slot.id}" class="btn btn-info btn-xs"><i class="icon icon-edit"></i></g:link>
                                                                    <button type="submit" class="btn btn-danger btn-xs"><i class="icon icon-trash"></i></button>
                                                                </g:form>
                                                            </td>
                                                        </tr>
                                                    </g:each>
                                                    </tbody>
                                                </table>

                                                <div class="text-right">
                                                    <g:link action="editSlots" params="[date: day.date.format('yyyy-MM-dd'), 'conference.id': conference.conference.id]" class="btn btn-info">
                                                        <i class="icon icon-edit"></i> Edit day
                                                    </g:link>
                                                    <g:link action="copySlots" params="[date: day.date.format('yyyy-MM-dd'), 'conference.id': conference.conference.id]" class="btn btn-success">
                                                        <i class="icon icon-copy"></i> Copy day
                                                    </g:link>
                                                    <g:link action="deleteSlots" params="[date: day.date.format('yyyy-MM-dd'), 'conference.id': conference.conference.id]" class="btn btn-danger">
                                                        <i class="icon icon-trash"></i> Delete day
                                                    </g:link>

                                                </div>
                                            </div>
                                        </g:each>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </g:each>
                </div>
            </div>
        </div>

    </div>
</div>

</body>
</html>


<%@ page import="org.gr8conf.Slot" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'slot.label', default: 'Slot')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>

		<div id="show-slot" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
				<g:if test="${slotInstance?.conference}">
                    <f:display bean="${slotInstance}" property="conference">
					
						<g:link controller="conference" action="show" id="${slotInstance?.conference?.id}">${slotInstance?.conference?.name}</g:link>
					
                    </f:display>
				</g:if>
			
				<g:if test="${slotInstance?.date}">
                    <f:display bean="${slotInstance}" property="date">
					
						<g:fieldValue bean="${slotInstance}" field="date"/>
					
                    </f:display>
				</g:if>
			
				<g:if test="${slotInstance?.endTime}">
                    <f:display bean="${slotInstance}" property="endTime">
					
						<g:fieldValue bean="${slotInstance}" field="endTime"/>
					
                    </f:display>
				</g:if>
			
				<g:if test="${slotInstance?.startTime}">
                    <f:display bean="${slotInstance}" property="startTime">
					
						<g:fieldValue bean="${slotInstance}" field="startTime"/>
					
                    </f:display>
				</g:if>
			
			</ol>
			<g:form>
				<class class="row">
                    <div class="col-lg-push-2 col-lg-10">
					<g:hiddenField name="id" value="${slotInstance?.id}" />
					<g:link class="edit btn btn-default" action="edit" id="${slotInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete btn btn-warning" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                    </div>
				</class>
			</g:form>
		</div>
	</body>
</html>

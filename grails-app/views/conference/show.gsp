<%@ page import="cacoethes.Conference" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'conference.label', default: 'Conference')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<div id="show-conference" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ul class="nav nav-tabs">
        <li class="active"><a href="#info" data-toggle="tab">Conference Info</a></li>
        <li><a href="#ratings" data-toggle="tab">Ratings</a></li>
    </ul>

    <div class="tab-content">
        <div id="info" class="tab-pane active">

            <f:display bean="${conferenceInstance}" property="name">
                <g:fieldValue bean="${conferenceInstance}" field="name"/>
            </f:display>

            <f:display bean="${conferenceInstance}" property="location">
                <g:fieldValue bean="${conferenceInstance}" field="location"/>
            </f:display>
            <f:display bean="${conferenceInstance}" property="latitude">
                <g:fieldValue bean="${conferenceInstance}" field="latitude"/>
            </f:display>
            <f:display bean="${conferenceInstance}" property="longitude">
                <g:fieldValue bean="${conferenceInstance}" field="longitude"/>
            </f:display>

            <f:display bean="${conferenceInstance}" property="start">
                <g:fieldValue bean="${conferenceInstance}" field="start"/>
            </f:display>

            <f:display bean="${conferenceInstance}" property="end">
                <g:fieldValue bean="${conferenceInstance}" field="end"/>
            </f:display>

            <f:display bean="${conferenceInstance}" property="cfpDeadline">
                <g:fieldValue bean="${conferenceInstance}" field="cfpDeadline"/>
            </f:display>

            <f:display bean="${conferenceInstance}" property="timeZone">
                <g:fieldValue bean="${conferenceInstance}" field="timeZone"/>
            </f:display>




            <g:form>
                <class class="row">
                    <div class="col-lg-push-2 col-lg-10">
                        <g:hiddenField name="id" value="${conferenceInstance?.id}"/>
                        <g:link class="edit btn btn-default" action="edit" id="${conferenceInstance?.id}"><g:message code="default.button.edit.label" default="Edit"/></g:link>
                        <g:actionSubmit class="delete btn btn-warning" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                        onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                    </div>
                </class>
            </g:form>

        </div>

        <div id="ratings" class="tab-pane active">
            <div class="row">
                <div class="col-md-6">
                    <p>Pie-charts below are based on ${ratings.totalRatings} ratings</p>
                </div>
            </div>

            <div class="row">
                <h3>Overall</h3>

                <div class="col-md-6">
                    <jqplot:piechart serie="${ratings.overall.overall}"/>
                </div>

                <div class="col-md-6">
                    <h3>Comments</h3>
                    <ul>
                        <g:each in="${ratings.overall.comments}" var="comment">
                            <li>${comment}</li>
                        </g:each>
                    </ul>
                </div>
            </div>
            <hr/>

            <div class="row">
                <h2>Pre conference</h2>

                <div class="col-md-6">
                    <h3>Information level</h3>
                    <jqplot:piechart serie="${ratings.pre.info}"/>
                </div>

                <div class="col-md-6">
                    <h3>Website</h3>
                    <jqplot:piechart serie="${ratings.pre.website}"/>
                </div>

                <div class="col-md-6">
                    <h3>Registration</h3>
                    <jqplot:piechart serie="${ratings.pre.registration}"/>
                </div>

                <div class="col-md-6">
                    <h3>Comments</h3>
                    <ul>
                        <g:each in="${ratings.pre.comments}" var="comment">
                            <li>${comment}</li>
                        </g:each>
                    </ul>
                </div>
            </div>
            <hr/>

            <div class="row">
                <h2>Venue</h2>

                <div class="col-md-6">
                    <h3>Impression</h3>
                    <jqplot:piechart serie="${ratings.venue.impression}"/>
                </div>

                <div class="col-md-6">
                    <h3>Catering</h3>
                    <jqplot:piechart serie="${ratings.venue.catering}"/>
                </div>

                <div class="col-md-6">
                    <h3>Workshop Rooms</h3>
                    <jqplot:piechart serie="${ratings.venue.workshopRooms}"/>
                </div>

                <div class="col-md-6">
                    <h3>Auditoriums</h3>
                    <jqplot:piechart serie="${ratings.venue.auditoriums}"/>
                </div>

                <div class="col-md-6">
                    <h3>Comments</h3>
                    <ul>
                        <g:each in="${ratings.venue.comments}" var="comment">
                            <li>${comment}</li>
                        </g:each>
                    </ul>
                </div>
            </div>
            <hr/>

            <div class="row">
                <h2>Conference</h2>

                <div class="col-md-6">
                    <h3>Topic Relevance</h3>
                    <jqplot:piechart serie="${ratings.conference.topicRelevance}"/>
                </div>

                <div class="col-md-6">
                    <h3>Presentations</h3>
                    <jqplot:piechart serie="${ratings.conference.presentations}"/>
                </div>

                <div class="col-md-6">
                    <h3>Comments</h3>
                    <ul>
                        <g:each in="${ratings.conference.comments}" var="comment">
                            <li>${comment}</li>
                        </g:each>
                    </ul>
                </div>
            </div>
            <hr/>

            <div class="row">
                <h2>Coming back?</h2>

                <div class="col-md-6">
                    <h3>If self decides</h3>
                    <jqplot:piechart serie="${ratings.beBack.self}"/>
                </div>

                <div class="col-md-6">
                    <h3>If boss decides</h3>
                    <jqplot:piechart serie="${ratings.beBack.boss}"/>
                </div>
            </div>
            <div class="col-md-12">
                <h3>Who rated</h3>
                <ul>
                    <g:each in="${ratings.ratedBy}" var="ratedBy">
                        <li>${ratedBy}</li>
                    </g:each>
                </ul>

            </div>

        </div>
    </div>
    <r:script>
        $(function () {
            var lastId = $('.tab-pane.active:last-child').attr('id');
            var firstId = $('.tab-pane.active:first-child').attr('id');
            $('a[href="#' + lastId + '"]').tab('show');
            $('a[href="#' + firstId + '"]').tab('show');
        })

    </r:script>

</body>
</html>

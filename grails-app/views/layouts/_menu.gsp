<%@ page import="cacoethes.Conference" %>
<div class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <g:link uri="/" class="navbar-brand"><g:message code="navigation.title"/></g:link>
    </div>

    <div class="navbar-collapse collapse">
        <sec:ifLoggedIn>
            <ul class="nav navbar-nav">

                <li class="dropdown ${controllerName == 'profile' ? 'active' : ''}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><g:message code="menu.profile.label"/>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <sec:ifAnyGranted roles="ROLE_ADMIN">
                            <li><g:link controller="profile" action="list"><g:message code="menu.list.label"/></g:link></li>
                        </sec:ifAnyGranted>
                        <li>
                            <g:if test="${currentUser?.profile}">
                                <g:link controller="profile" action="show" id="${currentUser?.profile?.id}">
                                    <g:message code="menu.show.label"/></g:link>
                            </g:if>
                            <g:else>
                                <g:link controller="profile" action="create">
                                    <g:message code="menu.create.label"/></g:link>
                            </g:else>
                        </li>

                    </ul>
                </li>
                <g:if test="${currentUser?.profile}">
                    <li class="dropdown  ${controllerName == 'submission' ? 'active' : ''}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><g:message code="menu.submission.label"/><b
                                class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><g:link controller="submission" action="index"><g:message code="menu.list.label"/></g:link></li>
                            <li>
                                <g:link controller="submission" action="create">
                                    <g:message code="menu.create.label"/>
                                </g:link>
                            </li>
                        </ul>
                    </li>
                </g:if>
                <sec:ifAnyGranted roles="ROLE_ADMIN">
                    <li class="dropdown  ${controllerName == 'conference' ? 'active' : ''}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><g:message code="menu.conference.label"/><b
                                class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><g:link controller="conference" action="list"><g:message code="menu.list.label"/></g:link></li>
                            <li>
                                <g:link controller="conference" action="create">
                                    <g:message code="menu.create.label"/>
                                </g:link>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown  ${controllerName == 'track' ? 'active' : ''}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><g:message code="menu.track.label"/><b
                                class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><g:link controller="track" action="list"><g:message code="menu.list.label"/></g:link></li>
                            <li>
                                <g:link controller="track" action="create">
                                    <g:message code="menu.create.label"/>
                                </g:link>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown  ${controllerName == 'slot' ? 'active' : ''}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><g:message code="menu.slot.label"/><b
                                class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><g:link controller="slot" action="list"><g:message code="menu.list.label"/></g:link></li>
                            <li>
                                <g:link controller="slot" action="create">
                                    <g:message code="menu.create.label"/>
                                </g:link>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown  ${controllerName == 'agenda' ? 'active' : ''}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><g:message code="menu.agenda.label"/><b
                                class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <g:each in="${Conference.list()}" var="conference">
                                <li>
                                    <g:link controller="agenda" action="edit" id="${conference.id}">
                                        ${conference.name} <g:message code="menu.show.label"/> / <g:message code="menu.edit.label"/>
                                    </g:link>
                                </li>
                            </g:each>
                        </ul>
                    </li>
                    <li class="dropdown  ${controllerName == 'mailTemplate' ? 'active' : ''}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><g:message code="menu.mailTemplate.label"/><b
                                class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><g:link controller="mailTemplate" action="list"><g:message code="menu.mailTemplate.list.label"/></g:link></li>
                            <li>
                                <g:link controller="mailTemplate" action="create">
                                    <g:message code="menu.mailTemplate.create.label"/>
                                </g:link>
                            </li>
                        </ul>
                    </li>
                </sec:ifAnyGranted>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <g:include controller="profile" action="loggedIn"/>
            </ul>
        </sec:ifLoggedIn>
    </div><!--/.nav-collapse -->

</div>

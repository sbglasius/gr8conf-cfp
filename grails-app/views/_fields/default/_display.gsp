<div class="row">
    <label class="col-sm-2 text-muted">${label}</label>

    <div class="col-sm-10 text-primary">
        ${raw(value)}
    </div>
</div>

<div class="form-group ${invalid ? 'has-error' : ''}">
    <label for="${field}" class="col-sm-${cols ?: '2'} control-label">${label}
        <g:if test="${tooltip != null}">
            <i class="glyphicon glyphicon-question-sign info-tooltip" data-toggle="tooltip" title="<g:message code='${tooltip}'/>">
            </i>
        </g:if>
    </label>

    <div class="col-sm-${span ?: '10'}">
        ${raw(widget)}
        <g:if test="${errors}">
            <g:each in="${errors}" var="error">
                <span class="help-block">${error}</span>
            </g:each>
        </g:if>
    </div>
</div>

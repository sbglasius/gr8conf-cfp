<g:set var="textClasses" value="${classes ?: [] + 'form-control'}"/>
<g:field type="number" step="0.0000001" min="0" name="${property}" value="${value}" class="${textClasses.join(' ')}" placeholder="${tooltip}"/>

<%@ page import="org.springframework.context.MessageSourceResolvable" %>
<g:select name="${property}" from="${type.values().collect { it instanceof org.springframework.context.MessageSourceResolvable ? message(message: it) : it}}" keys="${type.values()*.name()}" value="${value}" class="selectpicker"/>

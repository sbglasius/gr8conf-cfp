<div class="form-group ${invalid ? 'has-error' : ''}">

    <label for="${field}" class="col-sm-${cols ?: '2'} control-label">
        <g:if test="${tooltip != null}">
            <i class="glyphicon glyphicon-question-sign info-tooltip" data-toggle="tooltip" title="<g:message code="${raw(tooltip)}"/>"></i>
        </g:if>
    </label>
    <div class="col-sm-${cols ?: '10'} ">
        <label for="${field}">
            ${raw(widget)} ${raw(label)}
        </label>
        <g:if test="${errors}">
            <g:each in="${errors}" var="error">
                <span class="help-block"><g:message error="${error}"/></span>
            </g:each>
        </g:if>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 text-muted">${label}</label>

    <div class="col-sm-10 text-primary">
        <%-- value comes in as a String for some reason, so we need to do a String compare --%>
        <g:set var="displayValue" value="${(value?.toString()?.trim() == 'True') ? 'default.boolean.yes' : 'default.boolean.no'}"/>
        ${message(code: displayValue)}
    </div>
</div>

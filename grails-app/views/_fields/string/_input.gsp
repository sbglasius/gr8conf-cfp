<g:set var="textClasses" value="${classes ?: [] + 'form-control'}"/>
<g:textField name="${property}" value="${value}" class="${textClasses.join(' ')}" placeholder="${tooltip}"/>

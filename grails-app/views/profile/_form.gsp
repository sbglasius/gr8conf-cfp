<%@ page import="cacoethes.Conference; cacoethes.auth.User; cacoethes.Profile" %>

<f:with bean="${profileInstance}">
    <f:field property="name"/>
    <f:field property="employer"/>
    <f:field property="employerSponsor"/>
    <div id="employerContactSection">
        <f:field property="employerContact"/>
    </div>
    <f:field property="email"/>
    <f:field property="twitter"/>
    <f:field property="bio" tooltip="profile.bio.tooltip"/>
    <f:field property="shortBio" tooltip="profile.shortBio.tooltip"/>
    <f:field property="needTravel" tooltip="profile.needTravel.tooltip"/>
    <f:field property="needAccommodation" tooltip="profile.needAccommodation.tooltip"/>
    <f:field property="travelFrom"/>
    <f:field property="travelNotes" label="Additional Travel Notes" tooltip="profile.travelNotes.tooltip"/>



<sec:ifAllGranted roles="ROLE_ADMIN">
  <f:field property="user">
  <g:select id="user" name="user.id" from="${User.list()}" optionKey="id" optionValue="username" required="" value="${profileInstance?.user?.id}" class="many-to-one"/>
  </f:field>
    <f:field property="featured">
        <g:select name="${property}" from="${cacoethes.Conference.list()}" optionKey="id" optionValue="name" value="${value}" multiple="true"/>
    </f:field>
    </sec:ifAllGranted>
</f:with>

<r:script>
    function toggleEmployerContact() {
        if( $('#employerSponsor').is(':checked')) {
            $("#employerContactSection").show();
        } else {
            $("#employerContactSection").hide();
        }
    }

    $(document).ready(function () {
        $('#employerSponsor').change(toggleEmployerContact);
        toggleEmployerContact();
    });
</r:script>

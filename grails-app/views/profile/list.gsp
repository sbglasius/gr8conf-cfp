
<%@ page import="cacoethes.Profile" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'profile.label', default: 'Profile')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="list-profile" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-bordered">
				<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'profile.name.label', default: 'Name')}" />
						<g:sortableColumn property="employer" title="${message(code: 'profile.employer.label', default: 'Employer')}" />

						%{--<g:sortableColumn property="email" title="${message(code: 'profile.email.label', default: 'Email')}" />--}%
					
						<g:sortableColumn property="bio" title="${message(code: 'profile.bio.label', default: 'Bio')}" />
					
						<th><g:message code="profile.presentations.label" default="# presentations" /></th>
						<th><g:message code="profile.user.label" default="User" /></th>

					</tr>
				</thead>
				<tbody>
				<g:each in="${profileInstanceList}" status="i" var="profileInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:profile value="${profileInstance}"/>
                        </td>
					
						<td><g:company value="${profileInstance}"/></td>
						%{--<td>${fieldValue(bean: profileInstance, field: "email")}</td>--}%

						<td style="width: 40%">${fieldValue(bean: profileInstance, field: "bio")}</td>
					
						<td>${profileInstance.submissions?.size()}</td>
						<td>${fieldValue(bean: profileInstance.user, field: "username")}</td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${profileInstanceTotal}" />
			</div>
		</div>
	</body>
</html>

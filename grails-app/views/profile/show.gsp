<%@ page import="cacoethes.Profile" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'profile.label', default: 'Profile')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<h1><g:message code="default.show.label" args="[entityName]"/></h1>
<tmpl:/common/flash/>
<r:require module="bootstrap-tooltip"/>

<div class="row">
    <div class="form-horizontal col-lg-10">
        <f:with bean="${profileInstance}">
            <f:display property="name">
                ${profileInstance.name}
                <g:if test="${profileInstance.needTravel}">
                    - <i class="icon-plane" data-toggle="tooltip" title="Need travel"></i>
                </g:if>
                <g:if test="${profileInstance.needAccommodation}">
                    - <i class="icon-suitcase" data-toggle="tooltip" title="Need accomodation"></i>
                </g:if>
            </f:display>
            <f:display property="employer"/>
            <f:display property="employerSponsor"/>
            <g:if test="${profileInstance.employerSponsor}">
                <f:display property="employerContact"/>
            </g:if>
            <f:display property="email"/>
            <f:display property="twitter"/>
        </f:with>
    </div>

    <div class="col-lg-2 text-center">
        <h4>Profile image</h4>
        <g:if test="${profileInstance.profileImage}">
            <div class="pad-20">
                <img class="img-circle img-profile" src="${createLink(action: 'img', id: profileInstance.id)}">
            </div>
            <g:link action="setImage" id="${profileInstance.id}" class="btn btn-info">Replace</g:link>

            <g:link action="removeImage" id="${profileInstance.id}" class="btn btn-warning">Remove</g:link>
        </g:if>
        <g:else>
            <div class="pad-20">
                <r:img uri="/images/dont-be-camera-shy.png" class="img-circle img-profile"/>
            </div>
            <g:link action="setImage" id="${profileInstance.id}" class="btn btn-info">Set</g:link>
        </g:else>
    </div>

    <div class="form-horizontal col-lg-10">
        <f:with bean="${profileInstance}">

            <f:display property="bio">
                <markdown:renderHtml><g:fieldValue bean="${profileInstance}" field="bio"/></markdown:renderHtml>
            </f:display>
            <f:display property="shortBio"/>
            <f:display property="travelFrom"/>
            <f:display property="travelNotes"/>
            <f:display property="submissions">
                <g:each in="${profileInstance.submissions}" var="submission">
                    <g:link controller="submission" action="show"
                            id="${submission.id}">${submission.title} (${submission.submittedTo.name.join(', ')})</g:link><br/>
                </g:each>
            </f:display>

            <sec:ifAllGranted roles="ROLE_ADMIN">
                <g:if test="${profileInstance?.user}">

                    <f:display property="user">
                        <g:link controller="user" action="show"
                                id="${profileInstance?.user?.id}">${profileInstance?.user?.username?.encodeAsHTML()}</g:link>
                    </f:display>
                    <f:display property="featured">
                        <g:if test="${value}">
                            <g:join in="${value ?: []}" delimiter="${'<br/>'}"/>
                        </g:if>
                        <g:else>
                            Not featured
                        </g:else>
                    </f:display>
                </g:if>
            </sec:ifAllGranted>
        </f:with>

        <g:form>
            <g:hiddenField name="id" value="${profileInstance?.id}"/>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <g:link class="edit btn btn-default" action="edit" id="${profileInstance?.id}">
                        <g:message code="default.button.edit.label" default="Edit"/>
                    </g:link>
                    <sec:ifAllGranted roles="ROLE_ADMIN">

                        <g:actionSubmit class="delete btn btn-warning" action="delete"
                                        value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                        onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                    </sec:ifAllGranted>
                </div>
            </div>
        </g:form>
    </div>
</div></body>
</html>

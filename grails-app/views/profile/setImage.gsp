<%@ page import="cacoethes.Profile" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="profile.add.image"/></title>
    <style type="text/css">

    /* Apply these styles only when #preview-pane has
       been placed within the Jcrop widget */
    .jcrop-holder #preview-pane {
        display: block;
        position: absolute;
        z-index: 2000;
        top: 10px;
        right: -280px;
        padding: 6px;
        border: 1px rgba(0, 0, 0, .4) solid;
        background-color: white;

        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;

        -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
        box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
    }

    /* The Javascript code will set the aspect ratio of the crop
       area based on the size of the thumbnail preview,
       specified here */
    #preview-pane .preview-container {
        width: 145px;
        height: 145px;
        overflow: hidden;
    }

    </style></head>

<body>
<h1><g:message code="profile.add.image"/></h1>
<tmpl:/common/flash/>
<r:require module="bootstrap-tooltip"/>
<r:require module="bootstrap-fileupload"/>
<r:require module="jcrop"/>

<g:uploadForm action="uploadImage">
    <g:hiddenField name="profile.id" value="${profileInstance?.id}"/>
    <g:hiddenField name="x1"/>
    <g:hiddenField name="x2"/>
    <g:hiddenField name="y1"/>
    <g:hiddenField name="y2"/>
    <g:hiddenField name="width"/>
    <g:hiddenField name="height"/>
    <div class="row">
        <div class="col-sm-8">
            <h4>Loaded image</h4>

            <div class="img-upload-div" id="target">

            </div>

        </div>

        <div class="col-sm-4">
            <h4>Preview</h4>

            <div id="preview-pane">
                <div class="preview-container img-circle">
                    <img/>
                </div>
            </div>
            <h4>Examples: Good</h4>
            <r:img uri="/images/example-good-1.png" class="img-example"/>
            <r:img uri="/images/example-good-2.png" class="img-example"/>
            <h6>Person centered, whole face visible</h6>
            <h4>Examples: Bad</h4>
            <r:img uri="/images/example-bad-1.png" class="img-example"/>
            <r:img uri="/images/example-bad-2.png" class="img-example"/>
            <h6>Too far away, two faces in one picture</h6>
            <r:img uri="/images/example-bad-4.png" class="img-example"/>
            <r:img uri="/images/example-bad-3.png" class="img-example"/>
            <h6>Too close, face not centered.</h6>

        </div>
    </div>


    <div class="form-group">
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <span class="btn btn-file btn-info">
                <span class="fileupload-new"><g:message code="default.fileupload.select"/></span>
                <span class="fileupload-exists">
                    <g:message code="default.fileupload.change" default="Change"/>
                </span><input type="file" name="image" id="image"/></span>
            <g:submitButton class="btn fileupload-exists btn-default" value="${message(code: "profile.upload.button")}" name="upload"/>
            <a href="#" class="btn fileupload-exists btn-danger" data-dismiss="fileupload">
                <g:message code="default.fileupload.remove" default="Remove"/>
            </a>

        </div>
    </div>
</g:uploadForm>
</body>
</html>

<r:script>
    var jcrop_api;
    function readURL(input) {
        var target = $('#target');
        var image;
        var preview = $('#preview-pane'),
                pcnt = $('.preview-container', preview),
                pimg = $('.preview-container img', preview),

                xsize = pcnt.width(),
                ysize = pcnt.height(), boundx, boundy;
        // Grab some information about the preview pane
        if (jcrop_api) {
            jcrop_api.destroy();
            jcrop_api = undefined;
            $('img', target).remove();

        }
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {

                image = $("<img/>")
                        .attr("src", e.target.result)
                        .load(function () {
                            var pic_real_width = this.width;
                            var pic_real_height = this.height;
                            var ratio = pic_real_width / pic_real_height;
                            var scaled_width, scaled_height;
                            if (ratio > 1) {
                                scaled_width = target.width();
                                scaled_height = target.height() / ratio;
                            } else {
                                scaled_height = target.height();
                                scaled_width = target.width * ratio;
                            }
                            image.appendTo(target).Jcrop({
                                boxWidth: scaled_width,
                                boxHeight: scaled_height,
                                aspectRatio: 1,
                                bgColor: 'none',
                                onChange: showCoords,
                                onSelect: showCoords,
                                onRelease: clearCoords
                            }, function () {
                                jcrop_api = this;
                                var bounds = this.getBounds();
                                boundx = bounds[0];
                                boundy = bounds[1];
                                var ratio = boundx / boundy;
                                if (ratio > 1) {
                                    this.setSelect([0, 0, boundx, boundx])
                                } else {
                                    this.setSelect([0, 0, boundy, boundy])
                                }
                            });
                        });

                function showCoords(c) {
                    $('#x1').val(c.x);
                    $('#y1').val(c.y);
                    $('#x2').val(c.x2);
                    $('#y2').val(c.y2);
                    $('#width').val(c.w);
                    $('#height').val(c.h);
                    updatePreview(c);
                }

                function updatePreview(c) {
                    if (parseInt(c.w) > 0) {

                        var rx = xsize / c.w;
                        var ry = ysize / c.h;

                        pimg.css({
                            width: Math.round(rx * boundx) + 'px',
                            height: Math.round(ry * boundy) + 'px',
                            marginLeft: '-' + Math.round(rx * c.x) + 'px',
                            marginTop: '-' + Math.round(ry * c.y) + 'px'
                        });
                    }
                }
                pimg.attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("[name='image']").change(function () {
        readURL(this);
    });




    function clearCoords() {
        $('#x1, #x2, #y1, #y2, #width, #height').val('');
    }

</r:script>
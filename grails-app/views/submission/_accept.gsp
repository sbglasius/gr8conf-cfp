<sec:ifAllGranted roles="ROLE_ADMIN">
    <div>
        <g:each in="${conferences.sort { it.name }}" var="conference">
            <div class="row padded-row">
                <label class="col-sm-6 text-muted">
                    ${conference.name} <g:if test="${submissionInstance.getRatingsForConference(conference)}">
                        (${submissionInstance.getRatingsForConference(conference).size()} ratings)

                    </g:if>
                </label>

                <div class="reload-holder">
                    <tmpl:acceptStatus submission="${submissionInstance}" conference="${conference}"/>
                </div>

            </div>
        </g:each>
    </div>
</sec:ifAllGranted>

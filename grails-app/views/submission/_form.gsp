<%@ page import="cacoethes.Profile; cacoethes.Duration; cacoethes.Conference; cacoethes.Tag" %>
<r:require modules="bootstrap-dropdown, bootstrap-tokenfield, typeahead"/>
<f:with bean="${submissionInstance}">
    <f:field property="title" tooltip="Headline/title of the talk."/>
    <f:field property="summary" tooltip="Talk abstract, up to 1000 characters. Markdown syntax supported."/>
    <f:field property="tags">
        <g:textField name="tags" value="${submissionInstance.tags*.name?.join(',')}" class="form-control" tooltip="Pick from already created tags, or add your own (for others to use)"/>
    </f:field>
    <f:field property="level"/>
    <f:field property="durations" tooltip="Length of your talk - you can pick multiple here">
        <g:select name="durations" class="selectpicker" from="${Duration.list().sort {
            it.minutes
        }}" optionKey="id" multiple="" value="${submissionInstance.durations}"/>
    </f:field>
    <f:field property="submittedTo" tooltip="At what conferences do you want submit this talk to. If your submission is still draft, just leave this empty for now.">
        <g:select name="submittedTo" from="${Conference.list()}" optionKey="id" optionValue="name" value="${submissionInstance.submittedTo}" multiple="true"/>
    </f:field>
    <r:script>
        $('[name="tags"]').tokenfield({
          typeahead: {
            name: 'tags',
            local: [${raw(Tag.list()*.name?.collect { "'${it.encodeAsJavaScript()}'" }?.join(','))}]
          },
          allowDuplicates: true
        });
    </r:script>
</f:with>

<sec:ifAllGranted roles="ROLE_ADMIN">
    <f:with bean="${submissionInstance}">
        <f:field property="acceptedFor">
            <g:select name="acceptedFor" from="${Conference.list()}" optionKey="id" optionValue="name" value="${submissionInstance.acceptedFor}" multiple="true"/>
        </f:field>
        <f:field property="user">
            <g:set var="currProfile" value="${submissionInstance?.user?.profile}"/>
            <g:select name="user" from="${Profile.list().sort { it.name } }" optionKey="userId" optionValue="name" value="${submissionInstance?.user?.id}"/>
        </f:field>
        <f:field property="additionalSpeakers">
            <g:select name="additionalSpeakers" from="${Profile.list().sort { it.name } }" optionKey="id" optionValue="name" multiple="true" value="${submissionInstance.additionalSpeakers}"/>
        </f:field>
    </f:with>
</sec:ifAllGranted>
      

<div class="col-sm-6">
    <div class="btn-group">
        <g:link action="changeStatus" params="[id: submission.id, conferenceId: conference.id, status:'accept']"
                class="acceptReject btn btn-default ${submission.isAcceptedFor(conference) ? 'active' : ''} ${submission.tags ? '' : 'disabled'}">
            <g:message code="button.accept.label"/>
        </g:link>
        <g:link action="changeStatus" params="[id: submission.id, conferenceId: conference.id, status:'undecided']"
                class="acceptReject btn btn-default ${submission.isUndecidedFor(conference) ? 'active' : ''} ${submission.tags ? '' : 'disabled'}">
            <g:message code="button.undecided.label"/>
        </g:link>
        <g:link action="changeStatus" params="[id: submission.id, conferenceId: conference.id, status: 'reject']"
                class="acceptReject btn btn-default ${submission.isRejectedFor(conference) ? 'active' : ''} ${submission.tags ? '' : 'disabled'}">
            <g:message code="button.reject.label"/>
        </g:link>
    </div>
</div>

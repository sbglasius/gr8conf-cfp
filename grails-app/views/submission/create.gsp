<%@ page import="cacoethes.Submission" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'submission.label', default: 'Submission')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="create-submission" class="content scaffold-create" role="main">
    <h1><g:message code="default.create.label" args="[entityName]"/></h1>
    <tmpl:/common/flash/>
    <g:form action="save" class="form-horizontal">
        <g:render template="form"/>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">

                <g:submitButton name="create" class="save btn btn-default"
                                value="${message(code: 'default.button.create.label', default: 'Create')}"/>
            </div>
        </div>
    </g:form>
</div>
</body>
</html>

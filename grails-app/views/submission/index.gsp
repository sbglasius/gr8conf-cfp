<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'submission.label', default: 'Submission')}"/>
    <sec:ifAllGranted roles="ROLE_ADMIN">
        <g:set var="pageTitle" value="All Talk Submissions"/>
    </sec:ifAllGranted>
    <sec:ifNotGranted roles="ROLE_ADMIN">
        <g:set var="pageTitle" value="Your Talk Submissions"/>
    </sec:ifNotGranted>
    <title>${pageTitle}</title>
</head>

<body>

<div id="list-submission" class="content scaffold-list" role="main">
    <h1>${pageTitle}</h1>

    <sec:ifNotGranted roles="ROLE_ADMIN">
        <div class="alert alert-info">
            If you need travel and/or accommodation expenses paid, please mark this in
            <g:link class="btn btn-info" controller="profile" action="show" id="${currentUser.profile?.id}">your profile</g:link>.
        </div>
        <g:unless test="${currentUser.profile.profileImage}">
            <div class="alert alert-warning">
                Please take a moment to <g:link class="btn btn-success" controller="profile" action="setImage" id="${currentUser.profile?.id}">add an image</g:link> to your profile
                .
            </div>

        </g:unless>
    </sec:ifNotGranted>
    <g:if test="${flash.message}">
        <div class="alert alert-success" role="status">${flash.message}</div>
    </g:if>
    <sec:ifAllGranted roles="ROLE_ADMIN">
        <h3>Total submissions: ${submissionInstanceList.size()}<g:if test="${command.hasParams}">(Filtered)</g:if></h3>
        <h5><a href="#" data-toggle="collapse" data-target="#filters">Filters</a></h5>

        <div id="filters" class="${command.hasParams ? '' : 'collapse'}">
            <div class="row">
                <div class="col-lg-2">Conferences:</div>

                <div class="col-lg-10">
                    <g:filterLinks collection="${conferences}" name="conference" button="true" selected="${command.conference}" params="${command.params}"/>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-2">Accepted for:</div>

                <div class="col-lg-10">
                    <g:filterLinks collection="${conferences}" name="acceptedFor" button="true" selected="${command.acceptedFor}" params="${command.params}"/>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-2">Rejected for:</div>

                <div class="col-lg-10">
                    <g:filterLinks collection="${conferences}" name="rejectedFor" button="true" selected="${command.rejectedFor}" params="${command.params}"/>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-2">Tags:</div>

                <div class="col-lg-10">
                    <g:filterLinks collection="${allTags}" name="tag" button="true" selected="${command.tag}" params="${command.params}"/>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-2">Durations:</div>

                <div class="col-lg-10">
                    <g:filterLinks collection="${allDurations}" name="duration" button="true" selected="${command.duration}" params="${command.params}"/>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-2">Past conferences:</div>

                <div class="col-lg-10">
                    <g:link action="index" params="${command.params}" class="btn btn-xs btn-${command.hidePastConferences ? 'info' : 'primary'}">
                        <g:if test="${command.hidePastConferences}">
                            Show
                        </g:if>
                        <g:else>
                            Hide
                        </g:else>

                    </g:link>
                </div>

            </div>

            <div class="row padded-row ${command.hasParams ? '' : 'hide'}">
                <div class="col-lg-2"><g:link action="index" class="btn btn-xs btn-default">Clear filters</g:link></div>
            </div>
        </div>
    </sec:ifAllGranted>

    <div class="table-responsive">
        <table class="table table-bordered table-striped" id="submissions">
            <thead>
            <tr>

                <th class="col-sm-3"><g:message code='submission.title.label' default='Title'/></th>
                <sec:ifNotGranted roles="ROLE_ADMIN">
                    <th class="col-lg-2">Submitted to</th>
                    <th class="col-lg-2">Accepted for</th>
                </sec:ifNotGranted>
                <sec:ifAllGranted roles="ROLE_ADMIN">
                    <th class="col-sm-2"><g:message code="submission.user.label" default="User"/></th>
                    <th class="col-sm-4"><g:message code="submission.acceptance.label"
                                                    default="Submission and Acceptance"/></th>
                </sec:ifAllGranted>

            </tr>
            </thead>
            <tbody>
            <g:each in="${submissionInstanceList}" status="i" var="submissionInstance">
                <tr>

                    <td><g:link action="show"
                                id="${submissionInstance.id}">${fieldValue(bean: submissionInstance, field: "title")}</g:link>
                        <sec:ifAllGranted roles="ROLE_ADMIN">
                            <br/>

                            <div class="text-muted small">Durations:
                            <g:filterLinks collection="${submissionInstance.durations}" name="duration"/>
                            </div>
                            <div class="text-muted small">Tags:
                            <g:filterLinks collection="${submissionInstance.tags}" name="tag"/>
                            <g:if test="${!submissionInstance.tags}">
                                <g:link action="edit" id="${submissionInstance.id}"><i class="glyphicon-warning-sign" style="color: red"></i> <strong style="color: red">Fix missing tags</strong></g:link>
                            </g:if>
                            </div>
                            <div class="text-muted small">Level: ${submissionInstance.level}</div>

                        </sec:ifAllGranted>

                    </td>

                    <sec:ifNotGranted roles="ROLE_ADMIN">
                        <td>
                            <g:join in="${submissionInstance.submittedTo*.name}" delimiter=", "/></td>
                        <td>
                            <g:if test="${submissionInstance.acceptedFor}">
                                <g:join in="${submissionInstance.acceptedFor*.name}" delimiter=", "/>
                            </g:if>
                            <g:else>
                                <g:message code="submission.accepted.for.none" default="To be decided"/>
                            </g:else>

                        </td>
                    </sec:ifNotGranted>
                    <sec:ifAllGranted roles="ROLE_ADMIN">
                        <g:set var="currProfile" value="${submissionInstance?.profile}"/>
                        <td>
                            <g:profile value="${currProfile}"/>
                            <br/>
                            <span class="text-muted small">
                                Employer : <g:company value="${currProfile}"/></span>
                            <br/>
                            <span class="text-muted small">From: ${currProfile?.travelFrom}</span>
                        </td>
                        <td class="container">
                            <g:if test="${command.conference}">
                                <g:set var="confs" value="${[command.conference]}"/>
                            </g:if>
                            <g:else>
                                <g:set var="confs" value="${submissionInstance.submittedTo.sort { it.name }}"/>
                            </g:else>
                            <g:set var="visibleConfs" value="${confs.findAll { it.start > year }}"/>
                            <tmpl:accept conferences="${visibleConfs}" submissionInstance="${submissionInstance}"/>
                        </td>
                    </sec:ifAllGranted>

                </tr>
            </g:each>
            </tbody>
        </table>
    </div>

    <div class="row text-right">
        <g:link class="btn btn-default" action="create">Add presentation</g:link>
    </div>

</div>
<sec:ifAllGranted roles="ROLE_ADMIN">
    <r:script>
        $('#submissions').on('click', '.acceptReject', function (event) {
            event.preventDefault();

            var parent = $(this).closest('.reload-holder');
            var href = $(this).attr('href');
            window.setTimeout(function () {
                parent.fadeTo(100, 0.2, function () {
                    parent.load(href, function () {
                        parent.fadeTo(200, 1.0)
                    });
                })
            }, 150);

        })
    </r:script>
</sec:ifAllGranted>
</body>
</html>

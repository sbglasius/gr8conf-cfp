<%@ page import="cacoethes.Submission" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'submission.label', default: 'Submission')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div id="show-submission" class="content scaffold-show" role="main">
    <h1>Title: ${submissionInstance.title}</h1>
    <tmpl:/common/flash/>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#info" data-toggle="tab">Content</a></li>
        <sec:ifAllGranted roles="ROLE_ADMIN">
            <li><a href="#admin" data-toggle="tab">Organizer</a></li>
        </sec:ifAllGranted>
        <g:each in="${ratings}" var="ratingGroup">
            <li><a href="#rating${ratingGroup.key.id}" data-toggle="tab">Ratings from: ${ratingGroup.key.name}</a></li>
        </g:each>
    </ul>

    <div class="tab-content">
        <div id="info" class="tab-pane active">
            <div class="form-horizontal">
                <f:with bean="${submissionInstance}">
                    <f:display property="summary">
                        <markdown:renderHtml><g:fieldValue bean="${submissionInstance}" field="summary"/></markdown:renderHtml>
                    </f:display>
                    <f:display property="tags">
                        <g:join in="${submissionInstance.tags.sort { it.name }*.name}" delimiter=", "/>
                    </f:display>
                    <f:display property="level"/>
                    <f:display property="durations">
                        <g:join in="${submissionInstance.durations.sort { it.minutes }}" delimiter=", "/>
                    </f:display>
                    <f:display property="submittedTo">
                        <g:join in="${submissionInstance.submittedTo.sort { it.start }*.name}" delimiter=", "/>
                    </f:display>
                    <f:display property="acceptedFor">
                        <g:if test="${submissionInstance.acceptedFor}">
                            <g:join in="${submissionInstance.acceptedFor.sort { it.start }*.name}" delimiter=", "/>
                        </g:if>
                        <g:else>
                            <g:message code="submission.accepted.for.none" default="Not determined yet"/>
                        </g:else>
                    </f:display>
                </f:with>
                <g:form>
                    <div class="row padded-row">
                        <div class="col-sm-offset-2 col-sm-10">
                            <app:canEditSubmission id="${submissionInstance?.id}">
                                <g:hiddenField name="id" value="${submissionInstance?.id}"/>
                                <g:link class="edit btn btn-info" action="edit" id="${submissionInstance?.id}"><g:message
                                        code="default.button.edit.label" default="Edit"/></g:link>
                                <g:actionSubmit class="delete btn btn-danger" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </app:canEditSubmission>

                        </div>
                    </div>
                </g:form>

            </div>
        </div>
        <sec:ifAllGranted roles="ROLE_ADMIN">
            <div id="admin" class="tab-pane">
                <g:if test="${submissionInstance?.user}">
                    <f:with bean="${submissionInstance}">
                        <f:display property="user.profile" label="submission.profiles.label">
                            <g:profile value="${submissionInstance.user.profile}"/>
                            <g:each in="${submissionInstance.additionalSpeakers}" var="profile">
                                <br/><g:profile value="${profile}"/>
                            </g:each>
                        </f:display>
                    </f:with>
                </g:if>
                <div class="padded-section reload-holder">
                    <tmpl:accept conferences="${submissionInstance.submittedTo}" submissionInstance="${submissionInstance}"/>
                </div>

            </div>
        </sec:ifAllGranted>
        <g:each in="${ratings}" var="ratingGroup">
            <div id="rating${ratingGroup.key.id}" class="tab-pane active">
                <div class="row">
                    <div class="col-md-6">
                        <p>Pie-charts below are based on ${ratingGroup.value.totalRatings} ratings</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <h3>Talk Relevance</h3>
                        <jqplot:piechart serie="${ratingGroup.value.talkRelevance}"/>
                    </div>

                    <div class="col-md-6">
                        <h3>Talk Quality</h3>
                        <jqplot:piechart serie="${ratingGroup.value.talkQuality}"/>
                    </div>

                    <div class="col-md-6">
                        <h3>Talk Level</h3>
                        <jqplot:piechart serie="${ratingGroup.value.talkLevel}"/>
                    </div>

                    <div class="col-md-6">
                        <h3>Talk comments</h3>
                        <ul>
                            <g:each in="${ratingGroup.value.talkComments}" var="comment">
                                <li>${comment}</li>
                            </g:each>
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <h3>Speaker quality</h3>
                        <jqplot:piechart serie="${ratingGroup.value.speakerQuality}"/>

                    </div>

                    <div class="col-md-6">
                        <h3>Speaker comments</h3>
                        <ul>
                            <g:each in="${ratingGroup.value.speakerComments}" var="comment">
                                <li>${comment}</li>
                            </g:each>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <sec:ifAllGranted roles="ROLE_ADMIN">
                        <div class="col-md-12">
                            <h3>Who rated</h3>
                            <ul>
                            <g:each in="${ratingGroup.value.ratedBy}" var="ratedBy">
                                    <li>${ratedBy}</li>
                            </g:each>
                            </ul>

                        </div>
                    </sec:ifAllGranted>

                </div>
            </div>

        </g:each>
    </div>

    <sec:ifAllGranted roles="ROLE_ADMIN">
        <r:script>
            $('.reload-holder').on('click', '.acceptReject', function (event) {
                event.preventDefault();
                var parent = $(this).closest('.reload-holder');
                parent.fadeTo(100, 0.1).load($(this).attr('href'), function () {
                    parent.fadeTo(200, 1.0)
                });

            })
        </r:script>
    </sec:ifAllGranted>
    <r:script>
        $(function () {
            var lastId = $('.tab-pane.active:last-child').attr('id');
            var firstId = $('.tab-pane.active:first-child').attr('id');
            $('a[href="#' + lastId + '"]').tab('show');
            $('a[href="#' + firstId + '"]').tab('show');
        })

    </r:script>
</div>

</body>
</html>

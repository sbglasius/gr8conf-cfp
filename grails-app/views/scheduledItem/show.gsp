
<%@ page import="org.gr8conf.ScheduledItem" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'scheduledItem.label', default: 'ScheduledItem')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>

		<div id="show-scheduledItem" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
				<g:if test="${scheduledItemInstance?.submission}">
                    <f:display bean="${scheduledItemInstance}" property="submission">
					
						<g:link controller="submission" action="show" id="${scheduledItemInstance?.submission?.id}">${scheduledItemInstance?.submission?.encodeAsHTML()}</g:link>
					
                    </f:display>
				</g:if>
			
				<g:if test="${scheduledItemInstance?.name}">
                    <f:display bean="${scheduledItemInstance}" property="name">
					
						<g:fieldValue bean="${scheduledItemInstance}" field="name"/>
					
                    </f:display>
				</g:if>
			
				<g:if test="${scheduledItemInstance?.conference}">
                    <f:display bean="${scheduledItemInstance}" property="conference">
					
						<g:link controller="conference" action="show" id="${scheduledItemInstance?.conference?.id}">${scheduledItemInstance?.conference?.encodeAsHTML()}</g:link>
					
                    </f:display>
				</g:if>
			
				<g:if test="${scheduledItemInstance?.slot}">
                    <f:display bean="${scheduledItemInstance}" property="slot">
					
						<g:link controller="slot" action="show" id="${scheduledItemInstance?.slot?.id}">${scheduledItemInstance?.slot?.encodeAsHTML()}</g:link>
					
                    </f:display>
				</g:if>
			
				<g:if test="${scheduledItemInstance?.track}">
                    <f:display bean="${scheduledItemInstance}" property="track">
					
						<g:link controller="track" action="show" id="${scheduledItemInstance?.track?.id}">${scheduledItemInstance?.track?.encodeAsHTML()}</g:link>
					
                    </f:display>
				</g:if>
			
			</ol>
			<g:form>
				<class class="row">
                    <div class="col-lg-push-2 col-lg-10">
					<g:hiddenField name="id" value="${scheduledItemInstance?.id}" />
					<g:link class="edit btn btn-default" action="edit" id="${scheduledItemInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete btn btn-warning" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                    </div>
				</class>
			</g:form>
		</div>
	</body>
</html>

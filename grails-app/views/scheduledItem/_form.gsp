<f:with bean="${scheduledItemInstance}">
    <g:set var="conferenceId" value="${scheduledItemInstance.conferenceId?:params.conferenceId}"/>
    <f:field property="name"/>
    <f:field property="track"/>
    <f:field property="slot"/>
    <g:hiddenField name="conference" value="${conferenceId}"/>
</f:with>

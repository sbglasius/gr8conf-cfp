
<%@ page import="org.gr8conf.ScheduledItem" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'scheduledItem.label', default: 'ScheduledItem')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="list-scheduledItem" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-bordered">
				<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'scheduledItem.name.label', default: 'Name')}" />
					
						<th><g:message code="scheduledItem.conference.label" default="Conference" /></th>
					
						<th><g:message code="scheduledItem.slot.label" default="Slot" /></th>
					
						<th><g:message code="scheduledItem.track.label" default="Track" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${scheduledItemInstanceList}" status="i" var="scheduledItemInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${scheduledItemInstance.id}">${fieldValue(bean: scheduledItemInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: scheduledItemInstance, field: "conference")}</td>
					
						<td>${fieldValue(bean: scheduledItemInstance, field: "slot")}</td>
					
						<td>${fieldValue(bean: scheduledItemInstance, field: "track")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${scheduledItemInstanceTotal}" />
			</div>
		</div>
	</body>
</html>

<g:set var="textClasses" value="${classes ?: [] + 'form-control'}"/>
<g:select name="${property}" 
    value="${value?.id}" 
    class="${textClasses.join(' ')}" 
    from="${slots}" 
    placeholder="${tooltip}"
    optionKey="id"
/>

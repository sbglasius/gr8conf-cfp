<g:set var="textClasses" value="${classes ?: [] + 'form-control'}"/>
<g:select name="${property}" 
    value="${value?.id}" 
    class="${textClasses.join(' ')}" 
    from="${tracks}" 
    placeholder="${tooltip}"
    optionKey="id"
/>

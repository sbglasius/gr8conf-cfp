
<%@ page import="cacoethes.MailTemplate" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'mailTemplate.label', default: 'MailTemplate')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>

		<div id="show-mailTemplate" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
				<g:if test="${mailTemplateInstance?.key}">
                    <f:display bean="${mailTemplateInstance}" property="key">
					
						<g:fieldValue bean="${mailTemplateInstance}" field="key"/>
					
                    </f:display>
				</g:if>
			
				<g:if test="${mailTemplateInstance?.from}">
                    <f:display bean="${mailTemplateInstance}" property="from">
					
						<g:fieldValue bean="${mailTemplateInstance}" field="from"/>
					
                    </f:display>
				</g:if>
			
				<g:if test="${mailTemplateInstance?.bcc}">
                    <f:display bean="${mailTemplateInstance}" property="bcc">
					
						<g:fieldValue bean="${mailTemplateInstance}" field="bcc"/>
					
                    </f:display>
				</g:if>
			
				<g:if test="${mailTemplateInstance?.filter}">
                    <f:display bean="${mailTemplateInstance}" property="filter">
					
						<g:fieldValue bean="${mailTemplateInstance}" field="filter"/>
					
                    </f:display>
				</g:if>
			
				<g:if test="${mailTemplateInstance?.conference}">
                    <f:display bean="${mailTemplateInstance}" property="conference">
					
						<g:link controller="conference" action="show" id="${mailTemplateInstance?.conference?.id}">${mailTemplateInstance?.conference?.encodeAsHTML()}</g:link>
					
                    </f:display>
				</g:if>
			
				<g:if test="${mailTemplateInstance?.subject}">
                    <f:display bean="${mailTemplateInstance}" property="subject">
					
						<g:fieldValue bean="${mailTemplateInstance}" field="subject"/>
					
                    </f:display>
				</g:if>
			
				<g:if test="${mailTemplateInstance?.body}">
                    <f:display bean="${mailTemplateInstance}" property="body">
					
						<g:fieldValue bean="${mailTemplateInstance}" field="body"/>
					
                    </f:display>
				</g:if>
			
			</ol>
			<g:form>
				<class class="row">
                    <div class="col-lg-push-2 col-lg-10">
					<g:hiddenField name="id" value="${mailTemplateInstance?.id}" />
					<g:link class="edit btn btn-default" action="edit" id="${mailTemplateInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete btn btn-warning" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                    </div>
				</class>
			</g:form>
		</div>
	</body>
</html>

<%@ page import="cacoethes.Conference" %>
<r:require modules="bootstrap-select"/>
<f:with bean="mailTemplateInstance">
    <f:field property="key"/>
    <f:field property="from"/>
    <f:field property="bcc"/>
    <f:field property="subject"/>
    <f:field property="body"/>
    <f:field property="filter"/>
    <f:field property="conference">
        <g:select name="${property}" from="${Conference.list()}" value="${value}" optionKey="id" optionValue="name" class="selectpicker"/>
    </f:field>
</f:with>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-9">
    <p>The data for the email is an iteration over profiles, picking submissions for the selected filter and conference. </>
    <p>The following properties are available:
    </p>
    </div>
    <div class="col-md-2"></div>
    <div class="col-md-3">
        <ul>
            <li>profile.name</li>
            <li>profile.employer</li>
            <li>profile.email</li>
            <li>profile.twitter</li>
            <li>profile.bio</li>
            <li>profile.needTravel</li>
            <li>profile.needAccomodation</li>
            <li>profile.profileImage <small>use only for exists check</small></li>
        </ul>
    </div>

    <div class="col-md-3">
        <ul>
            <li>submissions <em>(Matching filter)</em>, <small>each having</small>
                <ul>
                    <li>title</li>
                    <li>level</li>
                    <li>summary</li>
                    <li>tags <small>List&lt;Tag&gt; tag.name</small></li>
                </ul>
            </li>
        </ul>
    </div>

    <div class="col-md-3">
        <ul>
            <li>conference.name</li>
            <li>conference.location</li>
            <li>conference.start <small>LocalDate</small></li>
            <li>conference.end <small>LocalDate</small></li>
        </ul>
    </div>
</div>

</div>

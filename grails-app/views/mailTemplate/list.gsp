
<%@ page import="cacoethes.MailTemplate" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'mailTemplate.label', default: 'MailTemplate')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="list-mailTemplate" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table class="table table-bordered">
				<thead>
					<tr>
					
						<g:sortableColumn property="key" title="${message(code: 'mailTemplate.key.label', default: 'Key')}" />
					
						<g:sortableColumn property="from" title="${message(code: 'mailTemplate.from.label', default: 'From')}" />
					
						<g:sortableColumn property="bcc" title="${message(code: 'mailTemplate.bcc.label', default: 'Bcc')}" />
					
						<g:sortableColumn property="filter" title="${message(code: 'mailTemplate.filter.label', default: 'Filter')}" />
					
						<th><g:message code="mailTemplate.conference.label" default="Conference" /></th>
					
						<g:sortableColumn property="subject" title="${message(code: 'mailTemplate.subject.label', default: 'Subject')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${mailTemplateInstanceList}" status="i" var="mailTemplateInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${mailTemplateInstance.id}">${fieldValue(bean: mailTemplateInstance, field: "key")}</g:link></td>
					
						<td>${fieldValue(bean: mailTemplateInstance, field: "from")}</td>
					
						<td>${fieldValue(bean: mailTemplateInstance, field: "bcc")}</td>
					
						<td>${fieldValue(bean: mailTemplateInstance, field: "filter")}</td>
					
						<td>${fieldValue(bean: mailTemplateInstance, field: "conference")}</td>
					
						<td>${fieldValue(bean: mailTemplateInstance, field: "subject")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${mailTemplateInstanceTotal}" />
			</div>
		</div>
	</body>
</html>

package org.gr8conf

import groovyx.net.http.HTTPBuilder
import org.apache.commons.collections4.queue.CircularFifoQueue

import static groovyx.net.http.ContentType.URLENC
import static groovyx.net.http.Method.GET
import static groovyx.net.http.Method.POST

class TwitterSearchService {
    static MAX_TWEETS_IN_BUFFER = 25

    CircularFifoQueue tweetBuffer = new CircularFifoQueue(MAX_TWEETS_IN_BUFFER)
    private String accessToken
    public Map searchMetadata
    public List<Map> getRecentTweets() {
        def list = (tweetBuffer as List<Map> ?: []).sort { it.id }.collect()
        Collections.shuffle(list)
        return list
    }

    public Map getLatestSearchResult() {
        [search_metadata: searchMetadata, statuses: recentTweets]
    }
    public refreshTweets() {
        if(!accessToken) {
            accessToken = requestAccessToken()
        }
        def filter = 'gr8conf OR #gr8conf OR @gr8conf'

        def query = [q: filter, count: MAX_TWEETS_IN_BUFFER]
        if(searchMetadata?.max_id) {
            query.since_id = searchMetadata.max_id
        }

        def latestTweets = getLatestTweets(accessToken, query)
        searchMetadata = latestTweets.search_metadata
        log.debug("Refresh Tweets: new tweets arrived: ${latestTweets.statuses.size()}")
        latestTweets.statuses.each {
            tweetBuffer.add(it)
        }
    }



    private String requestAccessToken() {
        def http = new HTTPBuilder('https://api.twitter.com/')
        def credentials = "vGGOh3LDIl6H4G9fWoqJcDF8O:047u6no0fg4G2CvutQxmXRpHYv7hX1EDH5tOXDBF44zYlY82mt"

        def authorizationString = "Basic ${credentials.bytes.encodeBase64().toString()}"


        def postBody = [grant_type: 'client_credentials'] // will be url-encoded


        def tokens = []
        http.request(POST) {
            uri.path = '/oauth2/token'
            headers.'Authorization' = authorizationString
            requestContentType = URLENC

            body = postBody

            response.success = { resp, map ->
                log.debug "Request token: POST response status: ${resp.statusLine}"
                tokens = map

            }
            response.error = { resp ->
                log.error "Request token: POST response ${resp}"
            }
        }

        return tokens.access_token
    }

    private Map getLatestTweets(String accessToken, Map query) {
        def http = new HTTPBuilder('https://api.twitter.com/')

        def authorizationString = "bearer ${accessToken}"


        def tweets = [:]
        http.request(GET) {
            uri.path = '/1.1/search/tweets.json'
            uri.query = query
            headers.'Authorization' = authorizationString

            response.success = { resp, map ->
                log.debug "getLatestTweets: POST response status: ${resp.statusLine}"
                tweets = map
            }
            response.error = { resp ->
                log.error "getLatestTweets: POST response ${resp}"
            }
            response.'401' = { resp, json ->
                log.error "getLatestTweets: POST response ${resp.statusLine}"
                log.error json
                requestAccessToken()
            }

        }
        return tweets
    }



}

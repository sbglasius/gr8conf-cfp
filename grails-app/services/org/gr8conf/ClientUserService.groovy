package org.gr8conf

import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import org.apache.commons.collections4.map.PassiveExpiringMap
import org.apache.commons.collections4.queue.CircularFifoQueue
import org.gr8conf.api.ClientCode
import org.gr8conf.client.ClientGithubProfile
import org.gr8conf.client.ClientGoogleProfile
import org.gr8conf.client.ClientProfile
import org.gr8conf.client.ClientTwitterProfile
import org.springframework.social.connect.Connection
import org.springframework.social.oauth1.OAuthToken
import org.springframework.social.twitter.api.Twitter

import javax.servlet.http.HttpServletResponse
import java.util.concurrent.TimeUnit

class ClientUserService {
    def twitterConnectionFactory
    def grailsApplication

    // codes for oauth.io
    CircularFifoQueue<String> stateTokens = new CircularFifoQueue<>(20)
    // received tokens from oauth.io, as they can not be requested twice. Needed for twitter when email is unknown.
    PassiveExpiringMap<String, Map<String, String>> activeTokens = Collections.synchronizedMap(new PassiveExpiringMap(30, TimeUnit.MINUTES))

    String getStateToken() {
        def uuid = UUID.randomUUID().toString().replace(/-/, '')

        stateTokens.add(uuid)
        log.debug("Current tokens: $stateTokens")
        return uuid
    }

    boolean validateStateToken(String code) {
        stateTokens.contains(code)
    }

    Map userLogin(ClientCode clientCode) {
        log.debug("Client user login: $clientCode")
        try {

            Map<String, String> token = activeTokens.get(clientCode.tokenKey) ?: getToken(clientCode)
            if (!validateStateToken(token.state)) {
                [status: HttpServletResponse.SC_BAD_REQUEST, message: "The expected code for token generation was not found. Try again"]
            }

            switch (clientCode.provider) {
                case 'twitter':
                    return handleTwitterLogin(token, clientCode)
                case 'google':
                    return handleGoogleLogin(token)
                case 'github':
                    return handleGitHubLogin(token)
                    break;
            }
        } catch (e) {
            log.error(e)
            [status: HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message: e.message]
        }
    }

    private Map handleGoogleLogin(Map<String, String> token) {
        def profile = getGoogleInfo(token.access_token)
        def profileImageUrl = getValidProfileImageUrl(profile.picture as String)
        def clientGoogleProfile = ClientGoogleProfile.findByGoogleId(profile.id)
        if (clientGoogleProfile) {
            clientGoogleProfile.oauthToken = token.access_token
            clientGoogleProfile.save(flush: true)
            clientGoogleProfile.profileImageUrl = profileImageUrl
            def clientProfile = clientGoogleProfile.clientProfile

            return buildGoogleResponse(clientProfile)
        }
        clientGoogleProfile = new ClientGoogleProfile(googleId: profile.id, oauthToken: token.access_token, screenName: profile.email, profileImageUrl: profileImageUrl)

        def clientProfile = ClientProfile.findByEmail(profile.email) ?: new ClientProfile(profile)
        clientProfile.screenName = profile.given_name
        clientProfile.verified = profile.verified_email.asBoolean()

        clientProfile.googleProfile = clientGoogleProfile

        if (clientProfile.save(failOnError: true)) {
            return buildGoogleResponse(clientProfile)
        } else {
            log.warn(clientProfile.errors)
            return [status: HttpServletResponse.SC_BAD_REQUEST, message: "Could not save clientProfile", errors: clientProfile.errors]
        }
    }

    private LinkedHashMap<String, Object> buildGoogleResponse(ClientProfile clientProfile) {
        [
                status         : HttpServletResponse.SC_OK,
                uuid           : clientProfile.uuid,
                name           : clientProfile.name,
                email          : clientProfile.email,
                screenName     : clientProfile.googleProfile.screenName,
                profileImageUrl: clientProfile.googleProfile.profileImageUrl,
                provider       : 'Google'
        ]
    }

    private Map handleGitHubLogin(Map<String, String> token) {
        def profile = getGithubInfo(token.access_token)
        def profileImageUrl = getValidProfileImageUrl(profile.avatar_url)
        def clientGithubProfile = ClientGithubProfile.findByGithubId(profile.id)
        if (clientGithubProfile) {
            clientGithubProfile.oauthToken = token.access_token

            clientGithubProfile.profileImageUrl = profileImageUrl
            clientGithubProfile.save(flush: true)
            def clientProfile = clientGithubProfile.clientProfile

            return buildGitHubResponse(clientProfile)
        }
        clientGithubProfile = new ClientGithubProfile(githubId: profile.id, oauthToken: token.access_token, screenName: profile.login, profileImageUrl: profileImageUrl)

        def clientProfile = ClientProfile.findByEmail(profile.email)
        if (!clientProfile) {
            clientProfile = new ClientProfile(profile)
            clientProfile.screenName = profile.login
        }
        clientProfile.verified = true

        clientProfile.githubProfile = clientGithubProfile

        if (clientProfile.save()) {
            return buildGitHubResponse(clientProfile)
        } else {
            log.warn(clientProfile.errors)
            return [status: HttpServletResponse.SC_BAD_REQUEST, message: "Could not save clientProfile", errors: clientProfile.errors]
        }
    }

    private LinkedHashMap<String, Object> buildGitHubResponse(ClientProfile clientProfile) {
        [
                status         : HttpServletResponse.SC_OK,
                uuid           : clientProfile.uuid,
                name           : clientProfile.name,
                email          : clientProfile.email,
                screenName     : clientProfile.githubProfile.screenName,
                profileImageUrl: clientProfile.githubProfile.profileImageUrl,
                provider       : 'GitHub'
        ]
    }

    private Map handleTwitterLogin(Map<String, String> token, ClientCode clientCode) {
        // Lookup user on twitter
        OAuthToken accessToken = new OAuthToken(token.oauth_token, token.oauth_token_secret)
        Connection<Twitter> connection = twitterConnectionFactory.createConnection(accessToken)


        def profile = connection.api.userOperations().userProfile
        def profileImageUrl = getValidProfileImageUrl(profile.profileImageUrl)
        // Check if the user is already present
        def clientTwitterProfile = ClientTwitterProfile.findByScreenName(profile.screenName)
        if (clientTwitterProfile) {
            // Update the oauth token on the twitter profile
            updateTwitterTokens(token, clientTwitterProfile)
            clientTwitterProfile.profileImageUrl = profileImageUrl
            clientTwitterProfile.save(flush: true)
            def clientProfile = clientTwitterProfile.clientProfile
            return buildTwitterResponse(clientProfile)
        }
        // If user is not present, check if we have name and email
        if (!clientCode.name || !clientCode.email) {
            return [status: HttpServletResponse.SC_PRECONDITION_FAILED, name: profile.name]
        }
        // Finally create a ClientProfile and attach the ClientTwitterProfile
        clientTwitterProfile = new ClientTwitterProfile(profile.properties)
        updateTwitterTokens(token, clientTwitterProfile)

        def clientProfile = ClientProfile.findByEmail(clientCode.email) ?: new ClientProfile(clientCode.properties)
        clientProfile.screenName = clientTwitterProfile.screenName
        clientProfile.twitterProfile = clientTwitterProfile

        if (clientProfile.save(flush: true)) {
            return buildTwitterResponse(clientProfile)
        } else {
            log.warn(clientProfile.errors)
            return [status: HttpServletResponse.SC_BAD_REQUEST, message: "Could not save clientProfile", errors: clientProfile.errors]
        }
    }

    private LinkedHashMap<String, Object> buildTwitterResponse(ClientProfile clientProfile) {
        [
                status         : HttpServletResponse.SC_OK,
                uuid           : clientProfile.uuid,
                name           : clientProfile.name,
                email          : clientProfile.email,
                screenName     : clientProfile.twitterProfile.screenName,
                profileImageUrl: clientProfile.twitterProfile.profileImageUrl,
                provider       : 'Twitter']
    }

    private void updateTwitterTokens(Map<String, String> token, ClientTwitterProfile clientTwitterProfile) {
        clientTwitterProfile.oauthToken = token.oauth_token
        clientTwitterProfile.oauthTokenSecret = token.oauth_token_secret
    }

    Map<String, String> getToken(ClientCode clientCode) {
        // Get access token from oauth.io based on code.
        if (activeTokens.get(clientCode.code)) {
            return activeTokens.get(clientCode.code)
        }
        def restClient = new HTTPBuilder('https://oauth.io/auth/')
        def consumerKey = grailsApplication.config.oauth.consumerKey
        def consumerSecret = grailsApplication.config.oauth.consumerSecret
        def map = [code: clientCode.code, key: consumerKey, secret: consumerSecret]
        Map<String, String> resp = restClient.post(path: 'access_token', body: map)

        activeTokens.put(clientCode.code, resp)
        resp
    }

    Map<String, String> getGoogleInfo(String token) {
        def http = new HTTPBuilder('https://content.googleapis.com')
        Map<String, String> reply
        http.request(Method.GET, ContentType.JSON) {
            uri.path = '/userinfo/v2/me'

            headers.'User-Agent' = 'Mozilla/5.0 Ubuntu/8.10 Firefox/3.0.4'
            headers.'Authorization' = "Bearer $token"
            headers.'X-JavaScript-User-Agent' = 'something'
//            headers.'x-origin' = 'http://glasius.dk'
            // response handler for a success response code:
            response.success = { resp, json ->
                log.debug resp.statusLine

                // parse the JSON response object:
                reply = json
            }

            // handler for any failure status code:
            response.failure = { resp ->
                log.error "Unexpected error: ${resp.statusLine.statusCode} : ${resp.statusLine.reasonPhrase}"
            }
        }
        return reply

    }

    Map<String, String> getGithubInfo(String token) {
        def http = new HTTPBuilder('https://api.github.com')

        Map<String, String> reply
        http.request(Method.GET, ContentType.JSON) {
            uri.path = '/user'
//            uri.query = [ access_token: token ]
            headers.'User-Agent' = 'Mozilla/5.0 Ubuntu/8.10 Firefox/3.0.4'
            headers.'X-JavaScript-User-Agent' = 'something'
            headers.'x-origin' = 'http://glasius.dk'
            headers.'Authorization' = "token $token"
            // response handler for a success response code:
            response.success = { resp, json ->
                log.debug resp.statusLine

                // parse the JSON response object:
                reply = json
            }

            // handler for any failure status code:
            response.failure = { resp ->
                log.error "Unexpected error: ${resp.statusLine.statusCode} : ${resp.statusLine.reasonPhrase}"
            }
        }
        return reply

    }

    URL getValidProfileImageUrl(String imageUrl) {
        try {
            return imageUrl.toURL()
        } catch (e) {
            log.warn("This url is not valid: $imageUrl")
            return null
        }
    }
}

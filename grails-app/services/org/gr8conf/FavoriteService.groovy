package org.gr8conf

import grails.transaction.Transactional
import org.gr8conf.api.ClientDataCommand
import org.gr8conf.api.SaveFavoriteCommand
import org.gr8conf.client.ClientFavorite

@Transactional(readOnly = true)
class FavoriteService {
    @Transactional(readOnly = false)
    List<Long> saveFavorite(SaveFavoriteCommand command) {
        def favorite = ClientFavorite.findByConferenceAndClientProfileAndSubmission(command.conference, command.clientProfile, command.talk)
        if (command.status && !favorite) {
            log.debug "Create $command"
            new ClientFavorite(command.properties).save(flush: true, failOnError: true)
        } else {
            log.debug "Remove $command"
            favorite?.delete(flush: true)
        }

        getCurrentFavorites(command);
    }

    List<Long> getCurrentFavorites(ClientDataCommand command) {
        ClientFavorite.findAllByConferenceAndClientProfile(command.conference, command.clientProfile)*.submission*.id
    }
}

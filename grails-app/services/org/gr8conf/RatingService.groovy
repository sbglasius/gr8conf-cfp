package org.gr8conf

import cacoethes.Conference
import cacoethes.Submission
import org.gr8conf.api.ClientDataCommand
import org.gr8conf.api.SaveConferenceRatingCommand
import org.gr8conf.api.SaveRatingCommand
import org.gr8conf.client.ClientConferenceRating
import org.gr8conf.client.ClientRating

class RatingService {
    List<Map> saveRating(SaveRatingCommand command) {
        def rating = ClientRating.findByConferenceAndClientProfileAndSubmission(command.conference, command.clientProfile, command.submission)
        if (!rating) {
            log.debug "Create $command"
            new ClientRating(command.properties).save(flush: true, failOnError: true)
        } else {
            log.debug "Update $command"
            rating.properties = command.properties
            rating.save(flush: true, failOnError: true)
        }

        getCurrentRatings(command);
    }

    List<Map> getCurrentRatings(ClientDataCommand command) {
        ClientRating.findAllByConferenceAndClientProfile(command.conference, command.clientProfile).collect {
            [
                    id            : it.submission.id,
                    talkRelevance : it.talkRelevance,
                    talkQuality   : it.talkQuality,
                    talkLevel     : it.talkLevel,
                    talkComment   : it.talkComment,
                    speakerQuality: it.speakerQuality,
                    speakerComment: it.speakerComment
            ]
        }
    }


    ClientConferenceRating saveConferenceRating(SaveConferenceRatingCommand command) {
        def rating = getConferenceRating(command)
        if (!rating) {
            log.debug "Create $command"
            rating = new ClientConferenceRating(command.properties).save(flush: true, failOnError: true)
        } else {
            log.debug "Update $command"
            rating.properties = command.properties
            rating.save(flush: true, failOnError: true)
        }
        return rating
    }


    ClientConferenceRating getConferenceRating(ClientDataCommand command) {
        ClientConferenceRating.findByConferenceAndClientProfile(command.conference, command.clientProfile)
    }


    static TALK_TRANSLATIONS = [
            relevanceLevels: [1: "Irrelevant to me", 2: "Not really that relevant, but nice to know.",
                              3: "It sparked my interest...", 4: "I need to pursuade my boss to use it", 5: "Just what I was looking for!"],
            qualityLevels  : [1: "Poor", 2: "Needs more work.", 3: "This was ok. Average I would say.", 4: "This was pretty good.",
                              5: "One word: Cool!"],
            talkLevels     : [1: "This was just an overview, right?", 2: "A bit more depth would have been nice", 3: "About right",
                              4: "I could follow it - unless I blinked", 5: "Way over my head..."],
            speakerLevels  : [1: "Unprepared", 2: "A little more preparation would help", 3: "This was ok. Average I would say.",
                              4: "Good job indeed", 5: "Skills - you got skills!"]
    ]

    static CONF_TRANSLATIONS = [
            badToExcellentLevels: [1: 'Bad', 2: 'Below Average', 3: 'Average', 4: 'Above Average', 5: 'Excellent'],
            comingBackLevels    : [1: 'No', 2: 'Perhaps if the prices is better', 3: 'I hope', 4: 'Yes!', 5: 'I would not miss it!']

    ]

    Map<Conference, Map> getRatingsForSubmission(Submission submission) {
        def groupedRatings = ClientRating.findAllBySubmission(submission).groupBy { it.conference }

        groupedRatings.collectEntries { conference, ratings ->
            [conference,
             [
                     talkRelevance  : mapLevels(ratings*.talkRelevance, TALK_TRANSLATIONS.relevanceLevels),
                     talkQuality    : mapLevels(ratings*.talkQuality, TALK_TRANSLATIONS.qualityLevels),
                     talkLevel      : mapLevels(ratings*.talkLevel, TALK_TRANSLATIONS.talkLevels),
                     talkComments   : ratings*.talkComment.findAll { it },
                     speakerQuality : mapLevels(ratings*.speakerQuality, TALK_TRANSLATIONS.speakerLevels),
                     speakerComments: ratings*.speakerComment.findAll { it },
                     totalRatings   : ratings.size(),
                     ratedBy        : ratings*.clientProfile.collect {
                         "${it.name} (${it.email})"
                     }

             ]

            ]
        }
    }

    Map getRatingsForConference(Conference conference) {
        def ratings = ClientConferenceRating.findAllByConference(conference)
        [
                overall: [
                        overall: mapLevels(ratings*.overall, CONF_TRANSLATIONS.badToExcellentLevels),
                        comments: buildComments(ratings, "overallComments")
                ],
                pre: [
                        info: mapLevels(ratings*.preInfo, CONF_TRANSLATIONS.badToExcellentLevels),
                        website: mapLevels(ratings*.preWebsite, CONF_TRANSLATIONS.badToExcellentLevels),
                        registration: mapLevels(ratings*.preRegistration, CONF_TRANSLATIONS.badToExcellentLevels),
                        comments: buildComments(ratings, "preComments")
                ],
                venue: [
                        impression: mapLevels(ratings*.venueImpression, CONF_TRANSLATIONS.badToExcellentLevels),
                        catering: mapLevels(ratings*.venueCatering, CONF_TRANSLATIONS.badToExcellentLevels),
                        workshopRooms: mapLevels(ratings*.venueWorkshopRooms, CONF_TRANSLATIONS.badToExcellentLevels),
                        auditoriums: mapLevels(ratings*.venueAuditoriums, CONF_TRANSLATIONS.badToExcellentLevels),
                        comments: buildComments(ratings, "venueComments")

                ],
                conference: [
                        topicRelevance: mapLevels(ratings*.confTopicRelevance, CONF_TRANSLATIONS.badToExcellentLevels),
                        presentations: mapLevels(ratings*.confPresentations, CONF_TRANSLATIONS.badToExcellentLevels),
                        comments: buildComments(ratings, "confComments")
                ],
                beBack: [
                        self: mapLevels(ratings*.beBackSelf, CONF_TRANSLATIONS.comingBackLevels),
                        boss: mapLevels(ratings*.beBackSelf, CONF_TRANSLATIONS.comingBackLevels),
                ],
                totalRatings   : ratings.size(),
                ratedBy        : ratings*.clientProfile.collect {
                    "${it.name} (${it.email})"
                }
        ]

    }

    private List<GString> buildComments(List<ClientConferenceRating> ratings, String field) {
        ratings.findAll { it[field] }.collect { "$it.clientProfile.name: ${it[field]}" }
    }

    List<List> mapLevels(List<Integer> scores, Map<Integer, String> translations) {
        def groupedScores = scores.groupBy { it }.collectEntries { score, values ->
            [score, values?.size() ?: 0]
        }
        translations.collect { score, translation ->
            def size = groupedScores[score] ?: 0
            ["$translation (${size})", size]
        }


    }
}

package org.gr8conf
import cacoethes.Profile
import cacoethes.Submission
import com.naleid.grails.MarkdownService
import org.codehaus.groovy.grails.web.mapping.LinkGenerator
import org.gr8conf.transfer.Slot
import org.gr8conf.transfer.Speaker
import org.gr8conf.transfer.Talk

class TalkTranslaterService {
    LinkGenerator grailsLinkGenerator
    MarkdownService markdownService

    List<Talk> translateToTalkList(List<Submission> submissions, Long conferenceId) {
        Map<Long, Speaker> speakers = [:]
        submissions.collect { submission ->
            Talk talk = translate(submission, conferenceId)
            talk.speaker = speakers[submission.profile.id]?:translate(submission.profile)
            speakers[submission.profile.id] = talk.speaker
            talk
        }
    }

    List<Speaker> translateToSpeakerList(List<Submission> submissions, Long conferenceId) {

        Map<Long, Speaker> speakers = [:]
        submissions.each { submission ->
            Talk talk = translate(submission, conferenceId)
            Speaker speaker = speakers[submission.profile.id]?:translate(submission.profile)
            speakers[submission.profile.id] = speaker
            speaker.talks << talk
        }
        return speakers.values() as List
    }

    Talk translate(Submission submission, Long conferenceId) {
        ScheduledItem scheduledItem = submission.scheduledTimes.find { it.conference.id == conferenceId }
        return new Talk(
            id: submission.id,
            title: submission.title,
            summary: markdownService.markdown(submission.summary),
            tags: submission.tags*.name,
            slot: new Slot(
                startTime: scheduledItem?.slot?.startTime,
                endTime: scheduledItem?.slot?.endTime,
                date: scheduledItem?.slot?.date
            )
        )
    }

    Speaker translate(Profile profile) {
        return new Speaker(
                id: profile.id,
                name: profile.name,
                employer: profile.employer,
                twitter: profile.twitter,
                bio: markdownService.markdown(profile.bio),
                shortBio: profile.shortBio,
                talks: [],
                image: profile.profileImage ? grailsLinkGenerator.link(controller: 'profile', action: 'img', id: profile.id, absolute: true) : grailsLinkGenerator.resource(dir: 'images', file: 'camera-shy.png', absolute: true)
        )
    }
}

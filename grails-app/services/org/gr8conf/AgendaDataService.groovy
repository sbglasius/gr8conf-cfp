package org.gr8conf

import cacoethes.Conference
import cacoethes.Profile
import cacoethes.Submission
import grails.transaction.Transactional
import org.gr8conf.client.ClientFavorite
import org.gr8conf.client.ClientProfile

@Transactional(readOnly = true)
class AgendaDataService {
    def markdownService
    def grailsLinkGenerator

    Map<String, Date> getStatus(Conference conference, String uuid) {
        def clientProfile = ClientProfile.findByUuid(uuid)
        def submissions = Submission.list() // Also those not scheduled for the confernce, if someone is removed from the list.
        def latestSubmissionUpdate = submissions.collect { it.lastUpdated ?: it.dateCreated }.max()
        def scheduledItems = ScheduledItem.findAllByConference(conference)
        def latestScheduledItemsUpdate = scheduledItems.collect { it.lastUpdated ?: it.dateCreated }.max()
        def latestFavorite = ClientFavorite.findAllByConferenceAndClientProfile(conference, clientProfile)
        def latestFavoriteUpdate = latestFavorite.collect { it.lastUpdated ?: it.dateCreated }.max()

        return [talks: latestSubmissionUpdate, speakers: latestSubmissionUpdate, agenda: latestScheduledItemsUpdate, favorites: latestFavoriteUpdate]
    }

    List<Map> getSpeakers(Conference conference, boolean featured = false) {
        List<Submission> submissions = getSubmissions(conference)

        def profiles = findSpeakers(submissions)
        if (featured) {
            profiles = profiles.findAll { conference in it.featured }
        }

        profiles.collect {
            mapProfileToSpeaker(conference, it)
        }.sort { it.name }
    }

    List<Map> getTalks(Conference conference) {
        List<Submission> submissions = getSubmissions(conference)

        def talks = submissions.collect {
            mapSubmissionToTalk(conference, it)
        }.sort { it.title }
        talks
    }

    List<Map> getConferences() {
        List<Conference> conferences = Conference.list(sort: 'start')

        def conferenceList = conferences.collect {
            def numberOfSessions = it.submissions?.size() ?: 0

            def conference = [
                    id              : it.id,
                    name            : it.name,
                    location        : it.location,
                    start           : it.start,
                    end             : it.end,
                    timeZone        : it.timeZone,
                    numberOfSessions: numberOfSessions
            ]
            if (it.latitude && it.longitude) {
                conference.latitude = it.latitude
                conference.longitude = it.longitude
            }

            return conference

        }
        return conferenceList
    }

    List<Map> buildAgenda(Conference conference) {
        List<ScheduledItem> scheduledItems = ScheduledItem.findAllByConference(conference)
        def groupedByDay = scheduledItems.groupBy {
            it.slot.date
        }

        def data = groupedByDay.collect { day, scheduledForDay ->
            def tracks = getTracksForDay(scheduledForDay)
            def start = tracks*.start.flatten().min()
            def end = tracks*.end.flatten().max()
            [
                    day   : day,
                    start : start,
                    end   : end,
                    tracks: tracks,
            ]
        }
        data
    }

    List<Map> getTags(Conference conference) {
        List<Submission> submissions = getSubmissions(conference)
        mapTagsToTalks(submissions)
    }

    private ArrayList<Profile> findSpeakers(List<Submission> submissions) {
        submissions*.speakers.flatten().unique()
    }

    private List<LinkedHashMap<String, Serializable>> mapTagsToTalks(List<Submission> submissions) {
        def tags = submissions.collect { submission ->
            submission.tags.collect {
                [tag: it, talk: submission.id]
            }
        }.flatten().groupBy { it.tag }.collect {
            [
                    tag  : it.key.name,
                    talks: it.value*.talk.unique()
            ]
        }.sort { it.tag }
        tags
    }

    private List getTracksForDay(List<ScheduledItem> scheduledItems) {
        def allTracks = scheduledItems.groupBy { it.track }

        def trackData = allTracks.collect { track, items ->
            def t =
                    [
                            id        : track.id,
                            start     : items*.slot*.startTime.min(),
                            end       : items*.slot*.endTime.max(),
                            name      : track.name,
                            room      : track.room ?: '',
                            order     : track.roomOrder ?: 0,
                            color     : track.color,
                            allColumns: track.allColumns,
                            breaks    : track.breaks
                    ]
            if (!track.breaks) {
                t.slots = items.collect { mapAgendaItemToSlot(it) }
            } else {
                t.slots = items.collect { mapAgendaItemsToBreakSlot(it) }
            }
            return t
        }.sort { it.order }
        return trackData

    }

    private LinkedHashMap<String, Object> mapProfileToSpeaker(Conference conference, Profile it) {
        def acceptedTalks = it.submissions.findAll {
            conference in it.acceptedFor
        }
        [
                id      : it.id,
                name    : it.name,
                company : it.employer,
                twitter : it.twitter,
                image   : it.profileImage ? grailsLinkGenerator.link(controller: 'profile', action: 'img', id: it.id, absolute: true) : grailsLinkGenerator.resource(dir: 'images', file: 'camera-shy.png', absolute: true),
                bio     : markdownService.markdown(it.bio),
                shortBio: it.shortBio,
                featured: conference in it.featured,
                talks   : acceptedTalks.collect { [id: it.id, title: it.title, level: it.level?.name()] }
        ]
    }

    private Map mapSubmissionToTalk(Conference conference, Submission submission) {
        return [
                id      : submission.id,
                title   : submission.title,
                summary : markdownService.markdown(submission.summary),
                speakers: submission.speakers.collect { [id: it.id, name: it.name] },
                tags    : submission.tags.collect { it.name },
                level   : submission.level?.name(),
                slot    : mapAgendaItemToSlot(submission.scheduledTimes.find { it.conference == conference }),
        ]
    }

    private Map mapAgendaItemToSlot(ScheduledItem scheduledItem) {
        if (!scheduledItem) return [:]
        [
                start   : scheduledItem.slot.startTime,
                end     : scheduledItem.slot.endTime,
                duration: scheduledItem.slot.durationInMinutes,
                talk    : [id: scheduledItem.submission.id, title: scheduledItem.submission.title, level: scheduledItem.submission.level?.name()],
                speakers: scheduledItem.submission.speakers.collect {
                    [id: it.id, name: it.name]
                }
        ]
    }

    private Map mapAgendaItemsToBreakSlot(ScheduledItem scheduledItem) {
        [
                allColumns: scheduledItem.track.allColumns,
                start     : scheduledItem.slot.startTime,
                end       : scheduledItem.slot.endTime,
                duration  : scheduledItem.slot.durationInMinutes,
                name      : scheduledItem.name,
                icon      : scheduledItem.icon
        ]
    }

    private List<Submission> getSubmissions(conference) {
        def submissions = Submission.withCriteria {
            acceptedFor {
                eq('id', conference.id)
            }
        }
        submissions
    }
}

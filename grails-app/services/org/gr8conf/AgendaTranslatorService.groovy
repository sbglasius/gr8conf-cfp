package org.gr8conf

import org.gr8conf.transfer.Agenda
import org.gr8conf.transfer.AgendaDay
import org.gr8conf.transfer.AgendaItem
import org.gr8conf.transfer.Slot as SlotTransfer
import org.gr8conf.transfer.Track as TrackTransfer
import org.joda.time.LocalDate

class AgendaTranslatorService {
    TalkTranslaterService talkTranslaterService
    Agenda toAgenda(List<Track> tracks, List<Slot> slots, List<ScheduledItem> items) {
        List<SlotTransfer> slotTransfers = translateSlots(slots)
        List<LocalDate> days = slotTransfers*.date.unique()
        List<AgendaDay> agendaDays = days.collect { LocalDate date ->
            AgendaDay agendaDay = new AgendaDay(date: date)
            agendaDay.slots = slotTransfers.findAll { it.date == date }
            agendaDay.tracks = translateTracks(tracks, agendaDay.slots, items)
            agendaDay.startTime = slots.min { it.startTime }.startTime
            agendaDay.endTime = slots.max { it.endTime }.endTime
            agendaDay
        }
        agendaDays.each {
            it.tracks = it.tracks.findAll { it.agendaItems }
        }

        return new Agenda(agendaDays: agendaDays)
    }

    List<TrackTransfer> translateTracks(List<Track> tracks, List<SlotTransfer> slots, List<ScheduledItem> items) {
        tracks.collect { Track track ->
            TrackTransfer trackTransfer =  new TrackTransfer(name: track.name, allColumns:track.allColumns, color:track.color)
            List<ScheduledItem> trackItems = items.findAll { it.track.id == track.id }
            trackTransfer.agendaItems = slots.collect { SlotTransfer slot ->
                ScheduledItem item = trackItems.find { it.slot.date == slot.date && it.slot.startTime == slot.startTime }
                AgendaItem agendaItem
                
                if (!item) {
                    return null
                    //agendaItem = new AgendaItem(title: 'Empty')
                } else if (item.submission) {
                    agendaItem = talkTranslaterService.translate(item.submission, item.conference.id)
                    agendaItem.speaker = talkTranslaterService.translate(item.submission.profile)
                } else {
                    agendaItem = new AgendaItem(title: item.name)
                }
                if (item && item.track.allColumns) {
                    slot.allColumns = true
                    slot.item = agendaItem
                }
                //This will have to be done better if we have different slots per track
                agendaItem.slot = slot
                agendaItem
            } - null
            trackTransfer
        }
    }

    List<SlotTransfer> translateSlots(List<Slot> slots) {
        slots.collect { 
            new SlotTransfer(
                startTime: it.startTime,
                endTime: it.endTime,
                date: it.date
            )
        }.sort { a, b -> a.date <=> b.date ?: a.startTime <=> b.startTime }
    }
}

package org.gr8conf

import grails.transaction.Transactional


@Transactional
class AgendaService {

    def saveAgenda(AgendaCommand agenda) {
        def nonEmptyTracks = agenda.track.findAll()
        log.debug "Saving $nonEmptyTracks tracks"

        nonEmptyTracks.each { trackCommand ->
            def nonEmptySlots = trackCommand?.slot?.findAll()
            nonEmptySlots.each { slotCommand ->

                if (slotCommand.scheduledItem && slotCommand.empty) {
                    slotCommand.scheduledItem.delete()
                } else if (slotCommand.scheduledItem && slotCommand.changed) {
                    slotCommand.scheduledItem.with {
                        name = slotCommand.name
                        icon = slotCommand.icon
                        submission = slotCommand.submission
                        save(failOnError: true)
                    }
                } else if (!slotCommand.scheduledItem && !slotCommand.empty) {
                    new ScheduledItem(slotCommand.properties).save(failOnError: true)
                }
            }
        }

    }
}

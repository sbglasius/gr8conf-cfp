package org.gr8conf

import cacoethes.Conference
import cacoethes.Submission
class SubmissionService {
    
    List<Submission> findSubmissionsForConference(Long conferenceId) {
        Conference conference = Conference.get(conferenceId)
        Submission.executeQuery(
            '''select submission from Submission submission where :conference in elements(submission.acceptedFor)''',
            [conference:conference]
        )
    }
}

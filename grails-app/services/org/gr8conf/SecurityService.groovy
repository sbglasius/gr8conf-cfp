package org.gr8conf

import cacoethes.auth.User
import grails.plugin.springsecurity.SpringSecurityUtils

class SecurityService {
    
    def springSecurityService

    boolean canAccessUserData(User user) {
        return admin || user == currentUser
    }

    boolean isAdmin() {
        SpringSecurityUtils.ifAllGranted("ROLE_ADMIN")
    }

    //Convenience so we only have to include a single security service
    User getCurrentUser() {
        springSecurityService.currentUser
    }

}

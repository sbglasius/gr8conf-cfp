package cacoethes

import cacoethes.auth.User

import gr8conf.SecurityService
import gr8conf.test.UserBuilder

import grails.plugins.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor

import org.joda.time.LocalDate

import spock.lang.Specification

/**
 * SubmissionControllerSpec
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(SubmissionController)
@Mock([Submission, User, Conference, Tag])
class SubmissionControllerSpec extends Specification {

    SecurityService securityService
    User user
    Conference conference
    void setup() {
        securityService = Mock()
        user = new UserBuilder().build()
        conference = new Conference(start:new LocalDate(2014, 1, 1)).save(validate: false)
        user.springSecurityService = Mock(SpringSecurityService)
        controller.securityService = securityService
    }

    def populateValidParams(params) {
        params.title = 'my awesome talk'
        params.summary = 'totally awesome'
        params.tags = "groovy,grails"
        params.submittedTo = "$conference.id"
    }

    def testIndex() {
        when:
        controller.index()

        then:
        assert "/submission/list" == response.redirectedUrl
    }

    def testList() {

        when:
        def model = controller.list()

        then:
        2 * securityService.isAdmin() >> false
        3 * securityService.getCurrentUser() >> user
        assert model.submissionInstanceList.size() == 0
        assert model.submissionInstanceTotal == 0
    }

    def testCreate() {
       when:
       def model = controller.create()

       then:
       assert model.submissionInstance != null
    }

    def testSave() {
        when:
        controller.save()

        then:
        assert model.submissionInstance != null
        assert view == '/submission/create'

        when:
        response.reset()

        populateValidParams(params)
        controller.save()

        then:
        1 * securityService.getCurrentUser() >> user
        assert response.redirectedUrl == '/submission/list'
        assert controller.flash.message != null
        assert Submission.count() == 1
    }

    def testShow() {
        when:
        controller.show()

        then:
        assert flash.message != null
        assert response.redirectedUrl == '/submission/list'


        when:
        def submission = createValidSubmission()

        then:
        assert submission.save(failOnError: true) != null

        when:
        response.reset()
        params.id = submission.id
        def model = controller.show()

        then:
        assert model.submissionInstance == submission
    }

    def testEdit() {
        when:
        controller.edit()

        then:
        assert flash.message != null
        assert response.redirectedUrl == '/submission/list'


        when:
        def submission = createValidSubmission()

        then:
        assert submission.save() != null

        when:
        response.reset()
        params.id = submission.id
        def model = controller.edit()

        then:
        1 * securityService.canAccessUserData(user) >> true
        assert model.submissionInstance == submission
    }

    def testUpdate() {
        when:
        controller.update()

        then:
        assert flash.message != null
        assert response.redirectedUrl == '/submission/list'

        when:
        response.reset()

        and:
        def submission = createValidSubmission()

        then:
        assert submission.save() != null

        when:
        params.id = submission.id
        params.title = ''

        controller.update()

        then:
        1 * securityService.canAccessUserData(user) >> true
        assert view == "/submission/edit"
        assert model.submissionInstance != null

        when:
        submission.clearErrors()

        populateValidParams(params)
        controller.update()

        then:
        1 * securityService.canAccessUserData(user) >> true
        assert response.redirectedUrl == "/submission/show/$submission.id"
        assert flash.message != null

        when:
        //test outdated version number
        response.reset()
        submission.clearErrors()

        populateValidParams(params)
        params.id = submission.id
        params.version = -1
        controller.update()

        then:
        1 * securityService.canAccessUserData(user) >> true
        assert view == "/submission/edit"
        assert model.submissionInstance != null
        assert model.submissionInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    def testDelete() {
        when:
        controller.delete()

        then:
        assert flash.message != null
        assert response.redirectedUrl == '/submission/list'

        when:
        response.reset()

        and:
        def submission = createValidSubmission()

        then:

        assert submission.save() != null
        assert Submission.count() == 1

        when:
        params.id = submission.id
        controller.delete()

        then:
        assert Submission.count() == 0
        assert Submission.get(submission.id) == null
        assert response.redirectedUrl == '/submission/list'
    }

    private createValidSubmission() {
        def submission = new Submission(title: "Coooool talk", summary: "awesome talk", user: user)
        submission.addToTags(name: "Groovy")
        submission.addToSubmittedTo(conference)

        assert submission.validate()

        return submission
    }
}

package cacoethes

import spock.lang.Specification

import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainUnitTestMixin} for usage instructions
 */
@TestFor(Profile)
class ProfileSpec extends Specification{

    void testSomething() {
        expect:
        1 == 1
    }
}

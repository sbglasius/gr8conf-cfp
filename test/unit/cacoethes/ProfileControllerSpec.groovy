package cacoethes

import cacoethes.auth.User

import gr8conf.SecurityService
import gr8conf.test.UserBuilder
import grails.plugins.springsecurity.SpringSecurityService 

import grails.test.mixin.*

import org.junit.*

import spock.lang.Specification

@TestFor(ProfileController)
@Mock([Profile, User])
class ProfileControllerSpec extends Specification {

    SecurityService securityService
    User user
    Profile profile
    void setup() {
        securityService = Mock()
        user = new UserBuilder().build()
        user.springSecurityService = Mock(SpringSecurityService)
        profile = user.profile
        controller.securityService = securityService
    }

    def testIndex() {
        when:
        controller.index()

        then:
        "/profile/list" == response.redirectedUrl
    }

    def testList() {

        when:
        def model = controller.list()

        then:
        model.profileInstanceList.size() == 1
        model.profileInstanceTotal == 1
    }

    def testCreate() {
       when:
       user.profile = null
       def model = controller.create()

       then:
       1 * securityService.isAdmin() >> false
       1 * securityService.getCurrentUser() >> user
       model.profileInstance != null
    }

    def testSave() {

        when:
        controller.save()

        then:
        2 * securityService.getCurrentUser() >> user
        assert model.profileInstance != null
        assert view == '/profile/create'

        when:
        response.reset()
        params.name = 'Fred'
        params.email = 'fred@flinstone.com'
        params.bio = 'yabbadabbadoo'
        controller.save()

        then:
        2 * securityService.getCurrentUser() >> user
        assert response.redirectedUrl == '/'
        assert controller.flash.message != null
        assert Profile.count() == 2
    }

    def testShow() {
        when:
        controller.show()

        then:
        assert flash.message != null
        assert response.redirectedUrl == '/profile/list'


        when:
        response.reset()
        params.id = profile.id
        def model = controller.show()

        then:
        securityService.isAdmin() >> false
        securityService.canAccessUserData(user) >> true
        assert model.profileInstance == profile
    }

    def testEdit() {
        when:
        controller.edit()

        then:
        securityService.isAdmin() >> false
        assert flash.message != null
        assert response.redirectedUrl == '/profile/list'


        when:
        response.reset()
        assert profile.id
        params.id = profile.id
        def model = controller.edit()

        then:
        1 * securityService.canAccessUserData(user) >> true
        assert model.profileInstance == profile
    }

    def testUpdate() {

        when:
        response.reset()
        request.method = 'POST'
        controller.update()

        then:
        assert flash.message != null
        assert response.redirectedUrl == '/profile/list'

        when:
        response.reset()
        params.name = ''
        params.id = profile.id
        controller.update()

        then:
        1 * securityService.canAccessUserData(user) >> true
        assert view == "/profile/edit"
        assert model.profileInstance != null

        when:
        response.reset()
        profile.clearErrors()
        params.name = 'Neil Gaiman'
        params.email = 'sandman@gaiman.com'
        params.bio = 'author'

        controller.update()

        then:
        1 * securityService.canAccessUserData(user) >> true
        assert response.redirectedUrl == "/profile/show/$profile.id"
        assert flash.message != null
    }

    def testDelete() {

        when:
        response.reset()
        request.method = 'POST'
        controller.delete()

        then:
        assert flash.message != null
        assert response.redirectedUrl == '/profile/list'

        when:
        response.reset()
        then:
        assert Profile.count() == 1

        when:
        params.id = profile.id
        controller.delete()

        then:
        securityService.isAdmin() >> false
        assert Profile.count() == 0
        assert Profile.get(profile.id) == null
        assert response.redirectedUrl == '/profile/list'
    }

}

package cacoethes



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(TalkController)
class TalkControllerSpec {

    void testSomething() {
        fail "Implement me"
    }
}

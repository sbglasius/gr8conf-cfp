package gr8conf.test

import cacoethes.Profile
import cacoethes.auth.User

class UserBuilder {
    User user = new User(
        username:'fred',
        password:'yabbadabbadoo'
    )

    Profile profile = new Profile(
        name:'Fred Flintstone',
        email:'fred@flintstone.com',
        bio: 'yabbadabbadoo'
    )

    User build() {
        user.profile = profile
        profile.user = user
        return user.save()
    }

    UserBuilder withUsername(String username) {
        user.username = username
    }

    UserBuilder withPassword(String password) {
        profile.password = password
    }

    UserBuilder withName(String name) {
        profile.name = name
    }

    UserBuilder withEmail(String email) {
        profile.email = email
    }
}

package org.gr8conf.client


import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(ClientProfile)
class ClientProfileSpec {

    void testSomething() {
        fail "Implement me"
    }
}

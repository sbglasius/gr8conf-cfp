package org.gr8conf


import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ClientUserService)
class ClientUserServiceSpec {

    void testSomething() {
        fail "Implement me"
    }
}

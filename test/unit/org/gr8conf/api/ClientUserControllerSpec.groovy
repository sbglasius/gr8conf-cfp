package org.gr8conf.api



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(ClientUserController)
class ClientUserControllerSpec {

    void testSomething() {
       fail "Implement me"
    }
}
